/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INTCONST = 258,
     DOUBLECONST = 259,
     IF_KEYWORD = 260,
     ELSE_KEYWORD = 261,
     WHILE_KEYWORD = 262,
     FOR_KEYWORD = 263,
     FUNCTION_KEYWORD = 264,
     RETURN_KEYWORD = 265,
     BREAK_KEYWORD = 266,
     CONTINUE_KEYWORD = 267,
     AND_KEYWORD = 268,
     NOT_KEYWORD = 269,
     OR_KEYWORD = 270,
     LOCAL_KEYWORD = 271,
     GLOBAL_KEYWORD = 272,
     TRUE_KEYWORD = 273,
     FALSE_KEYWORD = 274,
     NIL_KEYWORD = 275,
     MINUS_OPERATOR = 276,
     MUL_OPERATOR = 277,
     DIV_OPERATOR = 278,
     MOD_OPERATOR = 279,
     EQUAL_OPERATOR = 280,
     NEQ_OPERATOR = 281,
     ASSIGN_OPERATOR = 282,
     PLUS_OPERATOR = 283,
     PLUSPLUS_OPERATOR = 284,
     MINUSMINUS_OPERATOR = 285,
     GRE_OPERATOR = 286,
     SMA_OPERATOR = 287,
     GREQ_OPERATOR = 288,
     SMAQ_OPERATOR = 289,
     LAG = 290,
     DAG = 291,
     LBRA = 292,
     DBRA = 293,
     FULLST = 294,
     LPAR = 295,
     DPAR = 296,
     QUESTION = 297,
     COMA = 298,
     DFULLST = 299,
     D_FULLSTP = 300,
     DOLLAR = 301,
     ID = 302,
     STRING = 303,
     ERRORR = 304,
     UMINUS = 305
   };
#endif
#define INTCONST 258
#define DOUBLECONST 259
#define IF_KEYWORD 260
#define ELSE_KEYWORD 261
#define WHILE_KEYWORD 262
#define FOR_KEYWORD 263
#define FUNCTION_KEYWORD 264
#define RETURN_KEYWORD 265
#define BREAK_KEYWORD 266
#define CONTINUE_KEYWORD 267
#define AND_KEYWORD 268
#define NOT_KEYWORD 269
#define OR_KEYWORD 270
#define LOCAL_KEYWORD 271
#define GLOBAL_KEYWORD 272
#define TRUE_KEYWORD 273
#define FALSE_KEYWORD 274
#define NIL_KEYWORD 275
#define MINUS_OPERATOR 276
#define MUL_OPERATOR 277
#define DIV_OPERATOR 278
#define MOD_OPERATOR 279
#define EQUAL_OPERATOR 280
#define NEQ_OPERATOR 281
#define ASSIGN_OPERATOR 282
#define PLUS_OPERATOR 283
#define PLUSPLUS_OPERATOR 284
#define MINUSMINUS_OPERATOR 285
#define GRE_OPERATOR 286
#define SMA_OPERATOR 287
#define GREQ_OPERATOR 288
#define SMAQ_OPERATOR 289
#define LAG 290
#define DAG 291
#define LBRA 292
#define DBRA 293
#define FULLST 294
#define LPAR 295
#define DPAR 296
#define QUESTION 297
#define COMA 298
#define DFULLST 299
#define D_FULLSTP 300
#define DOLLAR 301
#define ID 302
#define STRING 303
#define ERRORR 304
#define UMINUS 305




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 61 "parser.y"
typedef union Types{
	struct expr *exprV;
	char *stringV;
	int intV;
	double doubleV;
	struct forhelp *forV;
	
} YYSTYPE;
/* Line 1285 of yacc.c.  */
#line 146 "parser.tab.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



