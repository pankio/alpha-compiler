#ifndef _BINARY_H_
#define _BINARY_H_





/*enum vmopcode{

			assign_v =0	 , add_v 	 	  , sub_v 		  ,
			mul_v        , divX_v 	 	  , mod_v 		  ,
			jeq_v    	 , jne_v 		  ,
			jle_v        , jge_v 	 	  , jlt_v      	  ,
			jgt_v    	 , jump_v    	  , call_v        ,
			pusharg_v    , funcenter_v    ,
			funcexit_v	 , newtable_v	  ,
			tablegetelem_v, tablesetelem_v, uminus_v      ,
			and_v 	 	  , or_v  		  ,	not_v        ,
			getretval_v, nop_v    
			
		

};


enum vmarg_t{


	label_a    = 0,
	global_a   = 1,
	formal_a   = 2,
	local_a    = 3,
	number_a   = 4,
	string_a   = 5,
	bool_a     = 6,
	nil_a      = 7,
	userfunc_a = 8,
	libfunc_a  = 9,
	retval_a   = 10 

};

struct vmarg{

	enum vmarg_t type;
	unsigned val;
	struct symbol *sym;


};

struct instruction{

	enum vmopcode opcode;
	struct vmarg result;
	struct vmarg arg1;
	struct vmarg arg2;
	unsigned srcLine;


};

struct userfunc{

	unsigned address;
	unsigned localSize;
	char * id; 

};
*/

double  *numConst;
char ** stringConst;
struct userfunc ** userFunc;
char **nameLibFunc;

struct instruction *instrs;


void write_binary(double *, char **, struct userfunc **, char **, struct instruction *, unsigned, unsigned, unsigned, unsigned, int, int);

void read_binary();

void write_userfuncs(FILE *bfile, struct userfunc **userFuncs, unsigned totalUserFuncs);
void write_libfuncs(FILE *bfile, char **nameLibFuncs, unsigned totalNamedLibfuncs);
void write_strings(FILE *bfile, char **stringConsts, unsigned totalStringConsts);
void write_numbers(FILE *bfile, double *numConsts, unsigned totalNumConsts);
void write_instructions(FILE *bfile, struct instruction *instr, int quadcnt);
void read_userfuncs(FILE *bfile);
void read_libfuncs(FILE *bfile);
void read_strings(FILE *bfile);
void read_numbers(FILE *bfile);
void read_instructions(FILE *bfile);



#endif
