/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INTCONST = 258,
     DOUBLECONST = 259,
     IF_KEYWORD = 260,
     ELSE_KEYWORD = 261,
     WHILE_KEYWORD = 262,
     FOR_KEYWORD = 263,
     FUNCTION_KEYWORD = 264,
     RETURN_KEYWORD = 265,
     BREAK_KEYWORD = 266,
     CONTINUE_KEYWORD = 267,
     AND_KEYWORD = 268,
     NOT_KEYWORD = 269,
     OR_KEYWORD = 270,
     LOCAL_KEYWORD = 271,
     GLOBAL_KEYWORD = 272,
     TRUE_KEYWORD = 273,
     FALSE_KEYWORD = 274,
     NIL_KEYWORD = 275,
     MINUS_OPERATOR = 276,
     MUL_OPERATOR = 277,
     DIV_OPERATOR = 278,
     MOD_OPERATOR = 279,
     EQUAL_OPERATOR = 280,
     NEQ_OPERATOR = 281,
     ASSIGN_OPERATOR = 282,
     PLUS_OPERATOR = 283,
     PLUSPLUS_OPERATOR = 284,
     MINUSMINUS_OPERATOR = 285,
     GRE_OPERATOR = 286,
     SMA_OPERATOR = 287,
     GREQ_OPERATOR = 288,
     SMAQ_OPERATOR = 289,
     LAG = 290,
     DAG = 291,
     LBRA = 292,
     DBRA = 293,
     FULLST = 294,
     LPAR = 295,
     DPAR = 296,
     QUESTION = 297,
     COMA = 298,
     DFULLST = 299,
     D_FULLSTP = 300,
     DOLLAR = 301,
     ID = 302,
     STRING = 303,
     ERRORR = 304,
     UMINUS = 305
   };
#endif
#define INTCONST 258
#define DOUBLECONST 259
#define IF_KEYWORD 260
#define ELSE_KEYWORD 261
#define WHILE_KEYWORD 262
#define FOR_KEYWORD 263
#define FUNCTION_KEYWORD 264
#define RETURN_KEYWORD 265
#define BREAK_KEYWORD 266
#define CONTINUE_KEYWORD 267
#define AND_KEYWORD 268
#define NOT_KEYWORD 269
#define OR_KEYWORD 270
#define LOCAL_KEYWORD 271
#define GLOBAL_KEYWORD 272
#define TRUE_KEYWORD 273
#define FALSE_KEYWORD 274
#define NIL_KEYWORD 275
#define MINUS_OPERATOR 276
#define MUL_OPERATOR 277
#define DIV_OPERATOR 278
#define MOD_OPERATOR 279
#define EQUAL_OPERATOR 280
#define NEQ_OPERATOR 281
#define ASSIGN_OPERATOR 282
#define PLUS_OPERATOR 283
#define PLUSPLUS_OPERATOR 284
#define MINUSMINUS_OPERATOR 285
#define GRE_OPERATOR 286
#define SMA_OPERATOR 287
#define GREQ_OPERATOR 288
#define SMAQ_OPERATOR 289
#define LAG 290
#define DAG 291
#define LBRA 292
#define DBRA 293
#define FULLST 294
#define LPAR 295
#define DPAR 296
#define QUESTION 297
#define COMA 298
#define DFULLST 299
#define D_FULLSTP 300
#define DOLLAR 301
#define ID 302
#define STRING 303
#define ERRORR 304
#define UMINUS 305




/* Copy the first part of user declarations.  */
#line 1 "parser.y"

	#include<stdio.h>
	#include<stdlib.h>
	#include<string.h>
	#include<time.h>
	#include<math.h>
	#include"scanner.h"
	#include"inter.h"
	#include"symboltable.h"
	#include"inCode.h"
	#include"tarCode.h"
	#include"binary.h"

	int yyerror (char *);
	int alpha_yylex (void *);

	extern int yylineno;
	extern char* yytext;
	extern FILE* yyin;
	
	extern unsigned totalNumConsts;
	extern unsigned totalStringConsts;
	extern unsigned totalNamedLibfuncs;
	extern unsigned totalUserFuncs;
	extern int curr_incode;
	
	int loopcounter = 0, breakcntblock=0, otinanai = 1;
	int scope = 0, i, Max;
	int scopeDead,num_progVars = 0;
	struct alpha_token_t *alpha;
	int argsNum = -1;
	char **args;
	int prfundef = 0;	//gia mia metavliti
	int scopeHide = 0;
	int anonymous = 0;
	char *func_name;
	struct expr *elist_l = NULL;
	struct expr *indexed_l = NULL;
	int error = 0;



	struct funcdef_scope *loopcntstack;
	struct funcdef_scope *b;
	struct funcdef_scope *c;

	struct funcdef_scope *insertf(struct funcdef_scope *, int fscope);
	struct funcdef_scope *delete(struct funcdef_scope *);	
	enum scopespace_t currscopespace(void);	
	unsigned int programVarOffset = -1;	
	unsigned int functionLocalOffset = -1;	
	unsigned int formalArgOffset = -1;	/*Gia na doume an einai programvar,formalarg i metabliti mesa se function*/
	unsigned int scopeSpaceCounter = 1;
	struct forhelp{
		int enter;
		int test;
	};
	


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 61 "parser.y"
typedef union Types{
	struct expr *exprV;
	char *stringV;
	int intV;
	double doubleV;
	struct forhelp *forV;
	
} YYSTYPE;
/* Line 191 of yacc.c.  */
#line 245 "parser.tab.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */
#line 257 "parser.tab.c"

#if ! defined (yyoverflow) || YYERROR_VERBOSE

# ifndef YYFREE
#  define YYFREE free
# endif
# ifndef YYMALLOC
#  define YYMALLOC malloc
# endif

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   define YYSTACK_ALLOC alloca
#  endif
# else
#  if defined (alloca) || defined (_ALLOCA_H)
#   define YYSTACK_ALLOC alloca
#  else
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  79
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   661

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  51
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  49
/* YYNRULES -- Number of rules. */
#define YYNRULES  113
/* YYNRULES -- Number of states. */
#define YYNSTATES  197

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   305

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     8,     9,    12,    14,    17,    19,
      21,    23,    25,    28,    31,    33,    35,    37,    39,    43,
      47,    51,    55,    59,    63,    67,    71,    75,    79,    83,
      87,    91,    93,    97,   100,   103,   106,   109,   112,   115,
     117,   121,   123,   125,   127,   131,   133,   135,   138,   141,
     143,   147,   152,   156,   161,   166,   171,   177,   183,   188,
     195,   197,   198,   201,   204,   205,   209,   211,   213,   215,
     218,   221,   222,   228,   229,   234,   235,   236,   237,   238,
     249,   251,   252,   254,   256,   258,   260,   262,   264,   266,
     269,   270,   273,   274,   277,   282,   287,   289,   293,   295,
     299,   300,   301,   302,   303,   307,   315,   323,   327,   329,
     330,   332,   334,   336
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const yysigned_char yyrhs[] =
{
      52,     0,    -1,    53,    -1,    54,    53,    -1,    -1,     1,
      54,    -1,    49,    -1,    55,    42,    -1,    83,    -1,    86,
      -1,    95,    -1,    96,    -1,    11,    42,    -1,    12,    42,
      -1,    71,    -1,    73,    -1,    42,    -1,    57,    -1,    55,
      28,    55,    -1,    55,    21,    55,    -1,    55,    22,    55,
      -1,    55,    23,    55,    -1,    55,    24,    55,    -1,    55,
      31,    55,    -1,    55,    33,    55,    -1,    55,    32,    55,
      -1,    55,    34,    55,    -1,    55,    25,    55,    -1,    55,
      26,    55,    -1,    55,    15,    55,    -1,    55,    13,    55,
      -1,    56,    -1,    40,    55,    41,    -1,    21,    55,    -1,
      14,    55,    -1,    29,    59,    -1,    59,    29,    -1,    30,
      59,    -1,    59,    30,    -1,    58,    -1,    59,    27,    55,
      -1,    59,    -1,    61,    -1,    65,    -1,    40,    73,    41,
      -1,    79,    -1,    47,    -1,    98,    47,    -1,    99,    47,
      -1,    60,    -1,    59,    39,    47,    -1,    59,    37,    55,
      38,    -1,    61,    39,    47,    -1,    61,    37,    55,    38,
      -1,    61,    40,    62,    41,    -1,    47,    40,    62,    41,
      -1,    99,    47,    40,    62,    41,    -1,    98,    47,    40,
      62,    41,    -1,    60,    40,    62,    41,    -1,    40,    73,
      41,    40,    62,    41,    -1,    63,    -1,    -1,    55,    64,
      -1,    43,    63,    -1,    -1,    37,    66,    38,    -1,    67,
      -1,    62,    -1,    68,    -1,    70,    69,    -1,    43,    68,
      -1,    -1,    35,    55,    44,    55,    36,    -1,    -1,    35,
      72,    53,    36,    -1,    -1,    -1,    -1,    -1,     9,    78,
      74,    40,    75,    80,    76,    41,    77,    71,    -1,    47,
      -1,    -1,     3,    -1,     4,    -1,    48,    -1,    20,    -1,
      18,    -1,    19,    -1,    81,    -1,    47,    82,    -1,    -1,
      43,    81,    -1,    -1,    84,    54,    -1,     5,    40,    55,
      41,    -1,    84,    54,    85,    54,    -1,     6,    -1,    87,
      88,    93,    -1,     7,    -1,    40,    55,    41,    -1,    -1,
      -1,    -1,    -1,    91,    54,    92,    -1,     8,    40,    62,
      42,    90,    55,    42,    -1,    94,    89,    62,    41,    89,
      93,    89,    -1,    10,    97,    42,    -1,    55,    -1,    -1,
      16,    -1,    46,    -1,    17,    -1,    45,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   103,   103,   106,   107,   113,   114,   115,   116,   117,
     118,   119,   120,   130,   140,   141,   142,   146,   152,   158,
     164,   170,   176,   182,   188,   194,   200,   206,   212,   218,
     224,   230,   235,   236,   242,   248,   256,   272,   280,   296,
     301,   323,   327,   330,   333,   337,   342,   393,   426,   454,
     459,   495,   505,   517,   529,   553,   575,   597,   614,   627,
     635,   643,   646,   656,   657,   660,   663,   676,   691,   703,
     713,   714,   717,   720,   720,   724,   735,   739,   763,   724,
     793,   794,   798,   802,   806,   810,   814,   818,   824,   827,
     836,   839,   842,   847,   850,   856,   866,   873,   902,   907,
     913,   916,   920,   923,   926,   932,   941,   970,   986,   987,
     990,   991,   994,   995
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "INTCONST", "DOUBLECONST", "IF_KEYWORD",
  "ELSE_KEYWORD", "WHILE_KEYWORD", "FOR_KEYWORD", "FUNCTION_KEYWORD",
  "RETURN_KEYWORD", "BREAK_KEYWORD", "CONTINUE_KEYWORD", "AND_KEYWORD",
  "NOT_KEYWORD", "OR_KEYWORD", "LOCAL_KEYWORD", "GLOBAL_KEYWORD",
  "TRUE_KEYWORD", "FALSE_KEYWORD", "NIL_KEYWORD", "MINUS_OPERATOR",
  "MUL_OPERATOR", "DIV_OPERATOR", "MOD_OPERATOR", "EQUAL_OPERATOR",
  "NEQ_OPERATOR", "ASSIGN_OPERATOR", "PLUS_OPERATOR", "PLUSPLUS_OPERATOR",
  "MINUSMINUS_OPERATOR", "GRE_OPERATOR", "SMA_OPERATOR", "GREQ_OPERATOR",
  "SMAQ_OPERATOR", "LAG", "DAG", "LBRA", "DBRA", "FULLST", "LPAR", "DPAR",
  "QUESTION", "COMA", "DFULLST", "D_FULLSTP", "DOLLAR", "ID", "STRING",
  "ERRORR", "UMINUS", "$accept", "program", "stmts", "stmt", "expr",
  "term", "assignexpr", "primary", "lvalue", "member", "call", "elist",
  "exprs", "exprs2", "objectdef", "objectdef2", "indexed", "indexed1",
  "indexed2", "indexedelem", "block", "@1", "funcdef", "@2", "@3", "@4",
  "@5", "id", "const", "idlist", "idlist1", "idlist2", "ifstmt",
  "ifprefix", "elseprefix", "whilestmt", "whilestart", "whilecontinue",
  "N", "M", "loopstart", "loopend", "loopstmt", "forprefix", "forstmt",
  "returnstmt", "returnstmt2", "loc", "glob", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    51,    52,    53,    53,    54,    54,    54,    54,    54,
      54,    54,    54,    54,    54,    54,    54,    55,    55,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      55,    55,    56,    56,    56,    56,    56,    56,    56,    56,
      57,    58,    58,    58,    58,    58,    59,    59,    59,    59,
      60,    60,    60,    60,    61,    61,    61,    61,    61,    61,
      62,    62,    63,    64,    64,    65,    66,    66,    67,    68,
      69,    69,    70,    72,    71,    74,    75,    76,    77,    73,
      78,    78,    79,    79,    79,    79,    79,    79,    80,    81,
      81,    82,    82,    83,    84,    83,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    97,
      98,    98,    99,    99
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     2,     0,     2,     1,     2,     1,     1,
       1,     1,     2,     2,     1,     1,     1,     1,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     1,     3,     2,     2,     2,     2,     2,     2,     1,
       3,     1,     1,     1,     3,     1,     1,     2,     2,     1,
       3,     4,     3,     4,     4,     4,     5,     5,     4,     6,
       1,     0,     2,     2,     0,     3,     1,     1,     1,     2,
       2,     0,     5,     0,     4,     0,     0,     0,     0,    10,
       1,     0,     1,     1,     1,     1,     1,     1,     1,     2,
       0,     2,     0,     2,     4,     4,     1,     3,     1,     3,
       0,     0,     0,     0,     3,     7,     7,     3,     1,     0,
       1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
       0,     0,    82,    83,     0,    98,     0,    81,   109,     0,
       0,     0,   110,   112,    86,    87,    85,     0,     0,     0,
      73,    61,     0,    16,   113,   111,    46,    84,     6,     0,
       2,     0,     0,    31,    17,    39,    41,    49,    42,    43,
      14,    15,    45,     8,     0,     9,     0,   100,    10,    11,
       0,     0,     5,     0,    61,    80,    75,   108,     0,    12,
      13,    34,    33,     0,    35,     0,    37,     0,     0,    64,
      67,    60,     0,    66,    68,    71,     0,     0,    61,     1,
       3,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     7,     0,    36,    38,     0,     0,
      61,     0,     0,    61,    93,     0,   102,    61,    47,    48,
       0,     0,     0,   107,     0,     0,     0,     0,    62,    65,
       0,    69,    32,    44,     0,    30,    29,    19,    20,    21,
      22,    27,    28,    18,    23,    25,    24,    26,    40,     0,
      50,     0,     0,    52,     0,    96,     0,     0,     0,    97,
       0,    61,    61,    94,   101,    76,     0,    74,     0,    63,
      70,    61,    55,    51,    58,    53,    54,    95,    99,   103,
     100,     0,     0,     0,    90,     0,     0,   104,   102,    57,
      56,     0,    92,    77,    88,    72,    59,   100,   105,    90,
      89,     0,   106,    91,    78,     0,    79
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,    29,    30,    31,    69,    33,    34,    35,    36,    37,
      38,    70,    71,   118,    39,    72,    73,    74,   121,    75,
      40,    67,    41,   112,   174,   191,   195,    56,    42,   183,
     184,   190,    43,    44,   146,    45,    46,   106,   107,   173,
     148,   177,   149,    47,    48,    49,    58,    50,    51
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -167
static const short int yypact[] =
{
     167,   265,  -167,  -167,   -26,  -167,   -25,   -27,   375,   -18,
     -15,   375,  -167,  -167,  -167,  -167,  -167,   375,    -4,    -4,
    -167,   305,   340,  -167,  -167,  -167,   -10,  -167,  -167,    35,
    -167,   115,   425,  -167,  -167,  -167,   -11,     6,   -30,  -167,
    -167,  -167,  -167,  -167,   265,  -167,    12,  -167,  -167,  -167,
      -7,     3,  -167,   375,   375,  -167,  -167,   585,    13,  -167,
    -167,  -167,  -167,    47,   -14,   -30,   -14,   216,   375,   411,
    -167,  -167,    27,  -167,  -167,    14,   469,    32,   375,  -167,
    -167,   375,   375,   375,   375,   375,   375,   375,   375,   375,
     375,   375,   375,   375,  -167,   375,  -167,  -167,   375,    28,
     375,   375,    29,   375,    68,   375,  -167,   375,    37,    39,
     483,    36,    40,  -167,    53,    60,   385,   375,  -167,  -167,
      62,  -167,  -167,    59,    63,    38,   599,    10,  -167,  -167,
    -167,   613,   613,    10,   627,   627,   627,   627,   585,   519,
    -167,    65,   541,  -167,    66,  -167,   265,   505,   265,  -167,
      67,   375,   375,  -167,  -167,  -167,    59,  -167,   375,  -167,
    -167,   375,  -167,  -167,  -167,  -167,  -167,  -167,  -167,  -167,
    -167,    69,    70,   375,    74,   563,    72,  -167,  -167,  -167,
    -167,   447,    57,  -167,  -167,  -167,  -167,  -167,  -167,    74,
    -167,    73,  -167,  -167,  -167,    82,  -167
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -167,  -167,   -28,     1,     0,  -167,  -167,  -167,    19,  -167,
      30,   -49,    -8,  -167,  -167,  -167,  -167,     8,  -167,  -167,
     -65,  -167,   -16,  -167,  -167,  -167,  -167,  -167,  -167,  -167,
     -52,  -167,  -167,  -167,  -167,  -167,  -167,  -167,  -166,  -167,
    -167,  -167,   -40,  -167,  -167,  -167,  -167,  -167,  -167
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -5
static const short int yytable[] =
{
      32,    32,    52,    80,   178,   111,    77,   101,    57,   102,
     103,    61,    12,    13,    53,    54,    95,    62,    96,    97,
      55,   192,    76,    98,    59,    99,    98,    60,    99,   124,
      78,    32,    84,    85,    86,    79,    63,    64,    66,   115,
     108,    24,    25,    26,    32,   104,   100,   114,    65,    65,
     109,   141,   105,   110,   144,   113,     7,   120,   150,    83,
      84,    85,    86,    87,    88,   119,    89,    32,   116,    90,
      91,    92,    93,   123,   145,   140,   143,   151,   154,   152,
     155,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   156,   138,   157,    68,   139,   161,
     189,   142,   171,   172,   162,   147,   164,   166,   170,   159,
     179,   180,   176,   186,   194,    -4,     1,    20,     2,     3,
       4,   182,     5,     6,     7,     8,     9,    10,   160,    11,
     196,    12,    13,    14,    15,    16,    17,   193,   187,     0,
       0,     0,     0,     0,    18,    19,    32,   167,    32,   169,
      20,    -4,    21,     0,     0,    22,     0,    23,   175,     0,
      24,    25,    26,    27,    28,     0,     0,    -4,     1,     0,
       2,     3,     4,   181,     5,     6,     7,     8,     9,    10,
       0,    11,     0,    12,    13,    14,    15,    16,    17,     0,
       0,     0,     0,     0,     0,     0,    18,    19,     0,     0,
       0,     0,    20,     0,    21,     0,     0,    22,     0,    23,
       0,     0,    24,    25,    26,    27,    28,     1,     0,     2,
       3,     4,     0,     5,     6,     7,     8,     9,    10,     0,
      11,     0,    12,    13,    14,    15,    16,    17,     0,     0,
       0,     0,     0,     0,     0,    18,    19,     0,     0,     0,
       0,    20,    -4,    21,     0,     0,    22,     0,    23,     0,
       0,    24,    25,    26,    27,    28,     1,     0,     2,     3,
       4,     0,     5,     6,     7,     8,     9,    10,     0,    11,
       0,    12,    13,    14,    15,    16,    17,     0,     0,     0,
       0,     0,     0,     0,    18,    19,     0,     0,     0,     0,
      20,     0,    21,     0,     0,    22,     0,    23,     2,     3,
      24,    25,    26,    27,    28,     0,     0,     0,     0,    11,
       0,    12,    13,    14,    15,    16,    17,     0,     0,     0,
       0,     0,     0,     0,    18,    19,     0,     0,     0,     0,
      68,     0,    21,     2,     3,    22,     0,     0,     0,     7,
      24,    25,    26,    27,    11,     0,    12,    13,    14,    15,
      16,    17,     0,     0,     0,     0,     0,     0,     0,    18,
      19,     0,     0,     0,     0,     0,     0,    21,     2,     3,
      22,     0,     0,     0,     0,    24,    25,    26,    27,    11,
       0,    12,    13,    14,    15,    16,    17,     0,    81,     0,
      82,     0,     0,     0,    18,    19,    83,    84,    85,    86,
      87,    88,    21,    89,     0,    22,    90,    91,    92,    93,
      24,    25,    26,    27,    81,     0,    82,     0,     0,   158,
       0,     0,    83,    84,    85,    86,    87,    88,    81,    89,
      82,     0,    90,    91,    92,    93,    83,    84,    85,    86,
      87,    88,     0,    89,   117,     0,    90,    91,    92,    93,
      81,     0,    82,     0,     0,     0,     0,    94,    83,    84,
      85,    86,    87,    88,     0,    89,     0,     0,    90,    91,
      92,    93,    81,     0,    82,     0,     0,     0,     0,   188,
      83,    84,    85,    86,    87,    88,    81,    89,    82,     0,
      90,    91,    92,    93,    83,    84,    85,    86,    87,    88,
     122,    89,     0,     0,    90,    91,    92,    93,    81,     0,
      82,     0,     0,     0,   153,     0,    83,    84,    85,    86,
      87,    88,    81,    89,    82,     0,    90,    91,    92,    93,
      83,    84,    85,    86,    87,    88,   168,    89,     0,     0,
      90,    91,    92,    93,    81,     0,    82,   163,     0,     0,
       0,     0,    83,    84,    85,    86,    87,    88,     0,    89,
       0,     0,    90,    91,    92,    93,    81,     0,    82,   165,
       0,     0,     0,     0,    83,    84,    85,    86,    87,    88,
       0,    89,     0,     0,    90,    91,    92,    93,    81,   185,
      82,     0,     0,     0,     0,     0,    83,    84,    85,    86,
      87,    88,    81,    89,     0,     0,    90,    91,    92,    93,
      83,    84,    85,    86,    87,    88,     0,    89,     0,     0,
      90,    91,    92,    93,    83,    84,    85,    86,    -5,    -5,
       0,    89,     0,     0,    90,    91,    92,    93,    83,    84,
      85,    86,     0,     0,     0,    89,     0,     0,    -5,    -5,
      -5,    -5
};

static const short int yycheck[] =
{
       0,     1,     1,    31,   170,    54,    22,    37,     8,    39,
      40,    11,    16,    17,    40,    40,    27,    17,    29,    30,
      47,   187,    22,    37,    42,    39,    37,    42,    39,    78,
      40,    31,    22,    23,    24,     0,    40,    18,    19,    67,
      47,    45,    46,    47,    44,    44,    40,    63,    18,    19,
      47,   100,    40,    53,   103,    42,     9,    43,   107,    21,
      22,    23,    24,    25,    26,    38,    28,    67,    68,    31,
      32,    33,    34,    41,     6,    47,    47,    40,    42,    40,
      40,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    41,    95,    36,    35,    98,    40,
      43,   101,   151,   152,    41,   105,    41,    41,    41,   117,
      41,    41,   161,    41,    41,     0,     1,    35,     3,     4,
       5,    47,     7,     8,     9,    10,    11,    12,   120,    14,
     195,    16,    17,    18,    19,    20,    21,   189,   178,    -1,
      -1,    -1,    -1,    -1,    29,    30,   146,   146,   148,   148,
      35,    36,    37,    -1,    -1,    40,    -1,    42,   158,    -1,
      45,    46,    47,    48,    49,    -1,    -1,     0,     1,    -1,
       3,     4,     5,   173,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    16,    17,    18,    19,    20,    21,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    29,    30,    -1,    -1,
      -1,    -1,    35,    -1,    37,    -1,    -1,    40,    -1,    42,
      -1,    -1,    45,    46,    47,    48,    49,     1,    -1,     3,
       4,     5,    -1,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    16,    17,    18,    19,    20,    21,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    29,    30,    -1,    -1,    -1,
      -1,    35,    36,    37,    -1,    -1,    40,    -1,    42,    -1,
      -1,    45,    46,    47,    48,    49,     1,    -1,     3,     4,
       5,    -1,     7,     8,     9,    10,    11,    12,    -1,    14,
      -1,    16,    17,    18,    19,    20,    21,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    29,    30,    -1,    -1,    -1,    -1,
      35,    -1,    37,    -1,    -1,    40,    -1,    42,     3,     4,
      45,    46,    47,    48,    49,    -1,    -1,    -1,    -1,    14,
      -1,    16,    17,    18,    19,    20,    21,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    29,    30,    -1,    -1,    -1,    -1,
      35,    -1,    37,     3,     4,    40,    -1,    -1,    -1,     9,
      45,    46,    47,    48,    14,    -1,    16,    17,    18,    19,
      20,    21,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    29,
      30,    -1,    -1,    -1,    -1,    -1,    -1,    37,     3,     4,
      40,    -1,    -1,    -1,    -1,    45,    46,    47,    48,    14,
      -1,    16,    17,    18,    19,    20,    21,    -1,    13,    -1,
      15,    -1,    -1,    -1,    29,    30,    21,    22,    23,    24,
      25,    26,    37,    28,    -1,    40,    31,    32,    33,    34,
      45,    46,    47,    48,    13,    -1,    15,    -1,    -1,    44,
      -1,    -1,    21,    22,    23,    24,    25,    26,    13,    28,
      15,    -1,    31,    32,    33,    34,    21,    22,    23,    24,
      25,    26,    -1,    28,    43,    -1,    31,    32,    33,    34,
      13,    -1,    15,    -1,    -1,    -1,    -1,    42,    21,    22,
      23,    24,    25,    26,    -1,    28,    -1,    -1,    31,    32,
      33,    34,    13,    -1,    15,    -1,    -1,    -1,    -1,    42,
      21,    22,    23,    24,    25,    26,    13,    28,    15,    -1,
      31,    32,    33,    34,    21,    22,    23,    24,    25,    26,
      41,    28,    -1,    -1,    31,    32,    33,    34,    13,    -1,
      15,    -1,    -1,    -1,    41,    -1,    21,    22,    23,    24,
      25,    26,    13,    28,    15,    -1,    31,    32,    33,    34,
      21,    22,    23,    24,    25,    26,    41,    28,    -1,    -1,
      31,    32,    33,    34,    13,    -1,    15,    38,    -1,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    26,    -1,    28,
      -1,    -1,    31,    32,    33,    34,    13,    -1,    15,    38,
      -1,    -1,    -1,    -1,    21,    22,    23,    24,    25,    26,
      -1,    28,    -1,    -1,    31,    32,    33,    34,    13,    36,
      15,    -1,    -1,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    26,    13,    28,    -1,    -1,    31,    32,    33,    34,
      21,    22,    23,    24,    25,    26,    -1,    28,    -1,    -1,
      31,    32,    33,    34,    21,    22,    23,    24,    25,    26,
      -1,    28,    -1,    -1,    31,    32,    33,    34,    21,    22,
      23,    24,    -1,    -1,    -1,    28,    -1,    -1,    31,    32,
      33,    34
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,     1,     3,     4,     5,     7,     8,     9,    10,    11,
      12,    14,    16,    17,    18,    19,    20,    21,    29,    30,
      35,    37,    40,    42,    45,    46,    47,    48,    49,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    65,
      71,    73,    79,    83,    84,    86,    87,    94,    95,    96,
      98,    99,    54,    40,    40,    47,    78,    55,    97,    42,
      42,    55,    55,    40,    59,    61,    59,    72,    35,    55,
      62,    63,    66,    67,    68,    70,    55,    73,    40,     0,
      53,    13,    15,    21,    22,    23,    24,    25,    26,    28,
      31,    32,    33,    34,    42,    27,    29,    30,    37,    39,
      40,    37,    39,    40,    54,    40,    88,    89,    47,    47,
      55,    62,    74,    42,    73,    53,    55,    43,    64,    38,
      43,    69,    41,    41,    62,    55,    55,    55,    55,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      47,    62,    55,    47,    62,     6,    85,    55,    91,    93,
      62,    40,    40,    41,    42,    40,    41,    36,    44,    63,
      68,    40,    41,    38,    41,    38,    41,    54,    41,    54,
      41,    62,    62,    90,    75,    55,    62,    92,    89,    41,
      41,    55,    47,    80,    81,    36,    41,    93,    42,    43,
      82,    76,    89,    81,    41,    77,    71
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)		\
   ((Current).first_line   = (Rhs)[1].first_line,	\
    (Current).first_column = (Rhs)[1].first_column,	\
    (Current).last_line    = (Rhs)[N].last_line,	\
    (Current).last_column  = (Rhs)[N].last_column)
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

# define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
# define YYDSYMPRINTF(Title, Token, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if defined (YYMAXDEPTH) && YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yytype, yyvaluep)
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  register short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;


  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 103 "parser.y"
    {printf("programm->stmts\n");emit(nop, NULL, NULL, NULL, 0, yylineno);;}
    break;

  case 3:
#line 106 "parser.y"
    {printf("stmts->stmt stmts\n");;}
    break;

  case 4:
#line 107 "parser.y"
    {printf("NO rule stmts\n");;}
    break;

  case 5:
#line 113 "parser.y"
    {yyerrok;error = 1;;}
    break;

  case 6:
#line 114 "parser.y"
    {yyerror("parse error invalid character ");error = 1;;}
    break;

  case 7:
#line 115 "parser.y"
    {resettemp();printf("stmt->expr ;\n");;}
    break;

  case 8:
#line 116 "parser.y"
    {resettemp();printf("stmt->ifstmt\n");;}
    break;

  case 9:
#line 117 "parser.y"
    {resettemp();printf("stmt->whilestmt\n");;}
    break;

  case 10:
#line 118 "parser.y"
    {resettemp();printf("stmt->forstmt\n");;}
    break;

  case 11:
#line 119 "parser.y"
    {printf("stmt->returnstmt\n");;}
    break;

  case 12:
#line 120 "parser.y"
    {	
								if(loopcounter >0 && otinanai !=0){
									printf("error: illegal break statement (not in a loop) at line %d\n", yylineno);
									error = 1;}
								else{
									b=insertf(b, nextquadlabel()); emit(jump, NULL,NULL,NULL, 0, yylineno);
									b->foffset = breakcntblock;
								}
								printf("stmt->break ;\n");
							;}
    break;

  case 13:
#line 130 "parser.y"
    {
								if(loopcounter >0 && otinanai !=0){
									printf("error: illegal continue statement (not in a loop) at line %d\n", yylineno);
									error = 1;}
								else{
									c=insertf(c, nextquadlabel()); emit(jump, NULL,NULL,NULL, 0, yylineno);
									c->foffset = breakcntblock;
								}
								printf("stmt->continue ;\n");
							;}
    break;

  case 14:
#line 140 "parser.y"
    {resettemp();printf("stmt->block\n");;}
    break;

  case 15:
#line 141 "parser.y"
    {resettemp();printf("stmt->funcdef\n");;}
    break;

  case 16:
#line 142 "parser.y"
    {printf("stmt->;\n");;}
    break;

  case 17:
#line 146 "parser.y"
    {if(yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = yyvsp[0].exprV;
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->assirnexpr\n");
							;}
    break;

  case 18:
#line 152 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = operations_expr(add, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr + expr\n");
							;}
    break;

  case 19:
#line 158 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = operations_expr(sub, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr - expr\n");
							;}
    break;

  case 20:
#line 164 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = operations_expr(mul, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr * expr\n");
							;}
    break;

  case 21:
#line 170 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = operations_expr(divX, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr / expr\n");
							;}
    break;

  case 22:
#line 176 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = operations_expr(mod, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr % expr\n");
							;}
    break;

  case 23:
#line 182 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = bool_op_expr(if_greater, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr > expr\n");
							;}
    break;

  case 24:
#line 188 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = bool_op_expr(if_greatereq, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr >= expr\n");
							;}
    break;

  case 25:
#line 194 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = bool_op_expr(if_less, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;	
								printf("expr->expr < expr\n");
							;}
    break;

  case 26:
#line 200 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = bool_op_expr(if_lesseq, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr <= expr\n");
							;}
    break;

  case 27:
#line 206 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = bool_op_expr(if_eq, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr == expr\n");
							;}
    break;

  case 28:
#line 212 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = bool_op_expr(if_noteq, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr != expr\n");
							;}
    break;

  case 29:
#line 218 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = andor_expr(or, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr or expr\n");
							;}
    break;

  case 30:
#line 224 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {
							 	yyval.exprV = andor_expr(and, yyvsp[-2].exprV, yyvsp[0].exprV);
							 }
							 else yyval.exprV->sym = NULL;
								printf("expr->expr and expr\n");
							;}
    break;

  case 31:
#line 230 "parser.y"
    {	yyval.exprV = yyvsp[0].exprV;
								printf("expr->term\n");	
							;}
    break;

  case 32:
#line 235 "parser.y"
    {yyval.exprV = yyvsp[-1].exprV; printf("term->( expr )\n");;}
    break;

  case 33:
#line 236 "parser.y"
    {
									if(yyvsp[0].exprV->sym != NULL){
										yyval.exprV = expruminus(uminus, yyvsp[0].exprV);
									}
									printf("term->- expr\n");
								;}
    break;

  case 34:
#line 242 "parser.y"
    {if ( yyvsp[0].exprV->sym != NULL){
	   								yyval.exprV = not_expr(not,yyvsp[0].exprV);
								 }
								 else yyval.exprV->sym = NULL;
									printf("term->not expr\n");
								;}
    break;

  case 35:
#line 248 "parser.y"
    {	struct expr *e = newexpr_intnum(1);
									if(yyvsp[0].exprV->sym != NULL){
										yyval.exprV = sspp_lvalue(add, yyvsp[0].exprV, e);
										free(e);
								 	}
									else yyval.exprV->sym = NULL;
									printf("term->++ lvalue\n");
								;}
    break;

  case 36:
#line 256 "parser.y"
    {	struct expr *e;
									yyval.exprV = newexpr(var_e);
									yyval.exprV->sym = newtemp(scope);

									if(yyvsp[-1].exprV->type == tableitem_e) {
										e = emit_iftableitem(yyvsp[-1].exprV);
										emit(assign, e, yyvsp[-1].exprV, NULL, 0, yylineno);
										yyval.exprV = sspp_lvalue(add, yyvsp[-1].exprV, newexpr_intnum(1));
										emit(tablesetelem,yyvsp[-1].exprV, yyvsp[-1].exprV->index, e, 0, yylineno);
									}
									else {
										emit(assign, newexpr_intnum(1), NULL, yyval.exprV, 0, yylineno);
										yyval.exprV = sspp_lvalue(add, yyvsp[-1].exprV, yyval.exprV);
									}
									printf("term->lvalue ++\n");
								;}
    break;

  case 37:
#line 272 "parser.y"
    {	struct expr *e = newexpr_intnum(1);
									if(yyvsp[0].exprV->sym != NULL){
										yyval.exprV = sspp_lvalue(sub, yyvsp[0].exprV, e);
										free(e);
								 	}
									else yyval.exprV->sym = NULL;
									printf("term->--lvalue\n");
								;}
    break;

  case 38:
#line 280 "parser.y"
    {	struct expr *e;
									yyval.exprV = newexpr(var_e);
									yyval.exprV->sym = newtemp(scope);

									if(yyvsp[-1].exprV->type == tableitem_e) {
										e = emit_iftableitem(yyvsp[-1].exprV);
										emit(assign, e, yyvsp[-1].exprV, NULL, 0, yylineno);
										yyval.exprV = sspp_lvalue(add, yyvsp[-1].exprV, newexpr_intnum(1));
										emit(tablesetelem, yyvsp[-1].exprV,yyvsp[-1].exprV->index, e, 0, yylineno);
									}
									else {		
										emit(assign, newexpr_intnum(1), NULL, yyval.exprV, 0, yylineno);
										yyval.exprV = sspp_lvalue(sub, yyvsp[-1].exprV, yyval.exprV);
									}
									printf("term->lvalue --\n");
								;}
    break;

  case 39:
#line 296 "parser.y"
    {	yyval.exprV = yyvsp[0].exprV;
									printf("term->primary\n");
								;}
    break;

  case 40:
#line 301 "parser.y"
    {if(yyvsp[-2].exprV->sym != NULL && yyvsp[0].exprV->sym != NULL) {	
								if(yyvsp[-2].exprV->type == tableitem_e) {
									emit(tablesetelem, yyvsp[-2].exprV->index, yyvsp[0].exprV, yyvsp[-2].exprV, 0, yylineno);
									yyval.exprV = emit_iftableitem(yyvsp[-2].exprV);
									yyval.exprV->type = assignexpr_e;
								}
								else {
									emit(assign, yyvsp[0].exprV, NULL, yyvsp[-2].exprV, 0, yylineno);
									yyval.exprV = newexpr(assignexpr_e);
									yyval.exprV->sym = newtemp(scope);
									emit(assign, yyvsp[-2].exprV, NULL, yyval.exprV, 0, yylineno);
									yyval.exprV = yyvsp[-2].exprV;
								}
								
							 }
							 else
							 	yyval.exprV->sym = NULL;

							printf("assignexpr->lvalue = expr\n");
							;}
    break;

  case 41:
#line 323 "parser.y"
    {	yyval.exprV = emit_iftableitem(yyvsp[0].exprV);
							
							printf("primary->lvalue\n");
						;}
    break;

  case 42:
#line 327 "parser.y"
    {	yyval.exprV = yyvsp[0].exprV;
							printf("primary->call\n");
						;}
    break;

  case 43:
#line 330 "parser.y"
    {	yyval.exprV = yyvsp[0].exprV;
							printf("primary->objectdef\n");
						;}
    break;

  case 44:
#line 333 "parser.y"
    {	
							yyval.exprV = yyvsp[-1].exprV;
							printf("primary->( funcdef )\n");
						;}
    break;

  case 45:
#line 337 "parser.y"
    {	yyval.exprV = const_expr(assign, yyvsp[0].exprV);
							printf("primary->const\n");
						;}
    break;

  case 46:
#line 342 "parser.y"
    {	struct expr *e;
						struct symbol *sym;
						int i;	
						printf("lvalue->ID\n");
						
						
						if((lookup_total(yyvsp[0].stringV) == 0) /*&& (lookup_one_level($1, scope) == 0)*/){
							if(currscopespace() == 0){
								programVarOffset++;
								insert(scope,1,create_v(yyvsp[0].stringV,scope,yylineno,currscopespace(),programVarOffset),NULL, NULL);	
								num_progVars++;
								sym = lookup_retSym(yyvsp[0].stringV, scope);
								e = newexpr_var(sym);
								yyval.exprV = e;	
							}else if(currscopespace() == 1){//na balo kati
								fs->foffset++;
								functionLocalOffset = fs->foffset;
								insert(scope,1,create_v(yyvsp[0].stringV,scope,yylineno,currscopespace(),fs->foffset),NULL, NULL);}
								sym = lookup_retSym(yyvsp[0].stringV, scope);
								e = newexpr_var(sym);
								yyval.exprV = e;	
						}
						else{
						
							if(scope-1>=0 && prfundef>=1){
								for(i=fs->fscope;i<=scope;i++){
									sym = lookup_retSym(yyvsp[0].stringV, i);
									if(sym!=NULL){
										break;
									}
								}
							}else{
								for(i=0;i<=scope;i++){
									sym = lookup_retSym(yyvsp[0].stringV, i);
									if(sym!=NULL){
										break;
									}
								}
							}
							if(sym == NULL) {
								printf("error:Redefine variable at line:%d\n",yylineno);
								error = 1;
								yyval.exprV = NULL;
							}
							else {
								e = newexpr_var(sym);
								yyval.exprV = e;	
							}
						}
					
					;}
    break;

  case 47:
#line 393 "parser.y"
    {struct expr *e;
						struct symbol *sym;	
					printf("lvalue->loc ID\n");
					if(scope == 0){
						
						printf("parse error:illegal statement local at line %d\n",yylineno);
						error = 1;
					}
					else{
						if(lookup_one_level(yyvsp[0].stringV, scope) == 0){
							if(currscopespace() == 0){
								programVarOffset++;
								insert(scope,1,create_v(yyvsp[0].stringV,scope,yylineno,currscopespace(),programVarOffset),NULL, NULL);
								num_progVars++;
								sym = lookup_retSym(yyvsp[0].stringV, scope);
							}else if(currscopespace() == 1){//na balo kati
								fs->foffset++;
								functionLocalOffset = fs->foffset;
								insert(scope,1,create_v(yyvsp[0].stringV,scope,yylineno,currscopespace(),fs->foffset),NULL, NULL);}
								sym = lookup_retSym(yyvsp[0].stringV, scope);
						}
						sym = lookup_retSym(yyvsp[0].stringV, scope);
						if(sym == NULL) {
							printf("error:Redefine variable at line:%d\n",yylineno);
							error = 1;
							yyval.exprV = NULL;
						}
						else {
							e = newexpr_var(sym);
							yyval.exprV = e;	
						}
					}
					;}
    break;

  case 48:
#line 426 "parser.y"
    {struct expr *e;
						struct symbol *sym;						
						printf("lvalue->glob ID\n");
						if(lookup_total(yyvsp[0].stringV) == 0){
							if(scope == 0){
								programVarOffset++;
								insert(scope,1,create_v(yyvsp[0].stringV,scope,yylineno,currscopespace(),programVarOffset),NULL, NULL);
								num_progVars++;
								sym = lookup_retSym(yyvsp[0].stringV, 0);
							}
							else{
								printf("parse error:illegal statement global at line %d\n", yylineno);
								error = 1;
							}
								
						}
						sym = lookup_retSym(yyvsp[0].stringV, 0);
						if(scope == 0 && sym == NULL){
							printf("error:Redefine variable at line:%d\n",yylineno);
							error = 1;
							yyval.exprV = NULL;
						}
						else{
							e = newexpr_var(sym);
							yyval.exprV = e;
						}
						
					;}
    break;

  case 49:
#line 454 "parser.y"
    {	yyval.exprV=yyvsp[0].exprV;
						printf("lvalue->member\n");
					;}
    break;

  case 50:
#line 459 "parser.y"
    {	
							if(yyvsp[-2].exprV->sym!=NULL){
							
							/*	struct symbol * sym = newtemp(scope);
								struct expr * expres= newexpr_var(sym);
							
								struct expr* table = newexpr(tableitem_e);
								table->sym = $1->sym;
								//if($1->type==tableitem_e)
								//emit(tablegetelem, table, newexpr_conststring($3), expres, 0, yylineno);
								//$1 = emit_iftableitem($1);
								$$ = expres;
								$$->type = tableitem_e;
								$$-> sym = $1->sym;
								$$ -> index = newexpr_conststring($3); */
								
								yyvsp[-2].exprV = emit_iftableitem(yyvsp[-2].exprV);
								yyval.exprV = newexpr(tableitem_e);
								yyval.exprV -> sym = yyvsp[-2].exprV->sym;
								
								char *str = (char *) malloc(sizeof(char) * strlen(yyvsp[0].stringV)+3);
								int i;
								
								str[0] = '\"';
								for(i=1;i<strlen(yyvsp[0].stringV)+1; i++){
									str[i] = yyvsp[0].stringV[i-1];
								}
								int size = strlen(str);
								str[size] = '\"';
								str[size+1] = '\0';
								
								yyval.exprV->index = newexpr_conststring(str);
							}
							
							printf("member->lvalue . ID\n");
						;}
    break;

  case 51:
#line 495 "parser.y"
    {
							if(yyvsp[-3].exprV->sym !=NULL && yyvsp[-1].exprV->sym !=NULL){
							
								yyvsp[-3].exprV = emit_iftableitem(yyvsp[-3].exprV);
								yyval.exprV = newexpr(tableitem_e);
								yyval.exprV-> sym = yyvsp[-3].exprV->sym;
								yyval.exprV->index = yyvsp[-1].exprV;
							}
							printf("member->lvalue [ expr ]\n");
						;}
    break;

  case 52:
#line 505 "parser.y"
    {	
							if(yyvsp[-2].exprV->sym!=NULL){
							

								
								yyvsp[-2].exprV = emit_iftableitem(yyvsp[-2].exprV);
								yyval.exprV = newexpr(tableitem_e);
								yyval.exprV -> sym = yyvsp[-2].exprV->sym;
								yyval.exprV->index = newexpr_conststring(yyvsp[0].stringV);
							}
							printf("member->call . ID\n");
						;}
    break;

  case 53:
#line 517 "parser.y"
    {	
							if(yyvsp[-3].exprV->sym !=NULL && yyvsp[-1].exprV->sym !=NULL){
							
								yyvsp[-3].exprV = emit_iftableitem(yyvsp[-3].exprV);
								yyval.exprV = newexpr(tableitem_e);
								yyval.exprV-> sym = yyvsp[-3].exprV->sym;
								yyval.exprV->index = yyvsp[-1].exprV;
							}
							printf("member->call [ expr ]\n");
						;}
    break;

  case 54:
#line 529 "parser.y"
    {	
									int i, find = 0;
									struct expr *ex;
									struct symbol *sym;
									
									if(yyvsp[-3].exprV->sym!=NULL){
										for(i=0; i<=Max; i++) {
											if(lookup_one_level(yyvsp[-3].exprV->sym->u.v->name, i)){
												sym = lookup_retSym(yyvsp[-3].exprV->sym->u.v->name, i);
												ex = newexpr_var(sym);
												yyval.exprV = newexpr_call(ex, elist_l);
												elist_l = NULL;
												find = 1;
												break;
											} 
										}
									}
									if(find == 0) {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call->call ( elist )\n");
								;}
    break;

  case 55:
#line 553 "parser.y"
    {
									int i, find = 0;
									struct symbol *sym;
									struct expr *ex;
									
									for(i=0; i<=Max; i++) {
										if(lookup_one_level(yyvsp[-3].stringV, i)){
											sym = lookup_retSym(yyvsp[-3].stringV, i);
											ex = expr_func(sym);
											yyval.exprV = newexpr_call(ex, elist_l);
											elist_l = NULL;
											find = 1;
											break;
										} 
									}
									if(find == 0) {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call->ID ( elist )\n");
								;}
    break;

  case 56:
#line 575 "parser.y"
    {
									int i, find = 0;
									struct symbol *sym;
									struct expr *ex;
									
									for(i=0; i<=Max; i++) {
										if(lookup_one_level(yyvsp[-3].stringV, i)){
											sym = lookup_retSym(yyvsp[-3].stringV, i);
											ex = expr_func(sym);
											yyval.exprV = newexpr_call(ex, elist_l);
											elist_l = NULL;
											find = 1;
											break;
										} 
									}
									if(find == 0) {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call-> glob ID ( elist )\n");
								;}
    break;

  case 57:
#line 597 "parser.y"
    {
									struct symbol *sym;
									struct expr *ex;
									
									if(lookup_one_level(yyvsp[-3].stringV, scope)){
										sym = lookup_retSym(yyvsp[-3].stringV, i);
										ex = expr_func(sym);
										yyval.exprV = newexpr_call(ex, elist_l);
										elist_l = NULL;								
									}
									else {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call->loc ID ( elist )\n");		
								;}
    break;

  case 58:
#line 614 "parser.y"
    {
									struct symbol *sym;
									if(yyvsp[-3].exprV->sym != NULL) {
										yyval.exprV = newexpr_call(yyvsp[-3].exprV, elist_l);
										elist_l = NULL;
									}	
									else {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}
									printf("call->member ( elist )\n");
								;}
    break;

  case 59:
#line 627 "parser.y"
    {	
									
									yyval.exprV = newexpr_call(yyvsp[-4].exprV, elist_l);
									elist_l = NULL;
									printf("call->( funcdef ) ( elist )\n");
								;}
    break;

  case 60:
#line 635 "parser.y"
    {	/*if(elist_l == NULL){
						elist_l = exprcpy(elist_l, $1);
					}
					else{	
						$1->next = elist_l;
						elist_l = exprcpy(elist_l, $1);
					}*/
					printf("elist->exprs\n");;}
    break;

  case 61:
#line 643 "parser.y"
    {printf("NO rule elist\n");;}
    break;

  case 62:
#line 646 "parser.y"
    {	if(elist_l == NULL){
							elist_l = exprcpy(elist_l, yyvsp[-1].exprV);
						}
						else{     
							yyvsp[-1].exprV->next = elist_l;
							elist_l = exprcpy(elist_l, yyvsp[-1].exprV);
						}
						printf("exprs->expr exprs2\n");;}
    break;

  case 63:
#line 656 "parser.y"
    {yyval.exprV = yyvsp[0].exprV; printf("exprs2->, exprs\n");;}
    break;

  case 64:
#line 657 "parser.y"
    {elist_l = NULL; printf("NO rule exprs2\n");;}
    break;

  case 65:
#line 660 "parser.y"
    {yyval.exprV = yyvsp[-1].exprV; printf("objectdef->[ objectdef2 ]\n");;}
    break;

  case 66:
#line 663 "parser.y"
    {
	
						struct expr *t = newexpr(newtable_e);
						struct expr *tmp;
						int i = 0;
						
						t->sym = newtemp(scope);
						emit(tablecreate, NULL, NULL, t, 0, yylineno);
						for(tmp = indexed_l; tmp != NULL; tmp = tmp->next)
							emit(tablesetelem, tmp, tmp->index, t, 0, yylineno);
						yyval.exprV = t;
						printf("objectdef2->indexed\n");
					;}
    break;

  case 67:
#line 676 "parser.y"
    {	
			
						struct expr *t = newexpr(newtable_e);
						struct expr *tmp;
						int i = 0;
						
						t->sym = newtemp(scope);
						emit(tablecreate, NULL, NULL, t, 0, yylineno);
						for(tmp = elist_l; tmp != NULL; tmp = tmp->next)
							emit(tablesetelem, newexpr_intnum(i++), tmp, t, 0, yylineno);
						yyval.exprV = t;
						printf("objectdef2->elist\n");
					;}
    break;

  case 68:
#line 691 "parser.y"
    {	/*if(indexed_l == NULL){
							indexed_l = exprcpy(indexed_l, $1);
						}
						else{
							$1->next = indexed_l;
							//indexed_l = $1;							
							indexed_l = exprcpy(indexed_l, $1);
							//indexed_l->next = NULL;
						}*/
						printf("indexed->indexed1\n");;}
    break;

  case 69:
#line 703 "parser.y"
    {	if(indexed_l == NULL){
								indexed_l = exprcpy(indexed_l, yyvsp[-1].exprV);
							}
							else{
								yyvsp[-1].exprV->next = indexed_l;
								indexed_l = exprcpy(indexed_l, yyvsp[-1].exprV);
						}
							printf("indexed1->indexedelem indexed2\n");;}
    break;

  case 70:
#line 713 "parser.y"
    {yyval.exprV = yyvsp[0].exprV; printf("indexed2->, indexed1\n");;}
    break;

  case 71:
#line 714 "parser.y"
    {indexed_l = NULL; printf("NO rule indexed2\n");;}
    break;

  case 72:
#line 717 "parser.y"
    {yyvsp[-3].exprV ->index = exprcpy(yyvsp[-3].exprV->index,yyvsp[-1].exprV);yyval.exprV=yyvsp[-3].exprV;printf("indexedelem->{ expr : expr }\n");;}
    break;

  case 73:
#line 720 "parser.y"
    {scope++;Max=scope;;}
    break;

  case 74:
#line 720 "parser.y"
    {printf("block->{ stmt }\n");
							scope--;hide(scope+1);;}
    break;

  case 75:
#line 724 "parser.y"
    {
				loopcounter ++;
				otinanai = 1;	
				fs = insertf(fs,scope);
				if(fs->next!=NULL){
					for(i=fs->fscope;i>fs->next->fscope;i--) {
						//printf("hide scope:%d\n",i);
						hide(i);
					}
				}
			;}
    break;

  case 76:
#line 735 "parser.y"
    {
				
			scopeSpaceCounter++;
			
			argsNum=-1;args = (char**)malloc(sizeof(char*));scope++;Max=scope;;}
    break;

  case 77:
#line 739 "parser.y"
    {
			if(anonymous==0) {
				if(lookup_total(((char *)yyvsp[-4].exprV)) == 0){
				insert(scope-1,2,NULL,create_f(((char *)yyvsp[-4].exprV),args,scope,yylineno,argsNum+1), NULL);}
				else {printf("error:Redefine function at line:%d\n",yylineno);error = 1;}
			} 
			else{while(lookup_total(func_name = create_funcName(func_name))){/*printf("error:Redefine function at line:%d\n",yylineno);*/}
				insert(scope-1,2,NULL,create_f(func_name,args,scope,yylineno,argsNum+1), NULL);
				
			}
			struct expr *e;
			struct symbol *sym;
			if(anonymous == 0)//an den einai  anonimi
				sym = lookup_retSym((char *)yyvsp[-4].exprV, scope-1);
			else
				sym = lookup_retSym(func_name, scope-1);
				
			anonymous = 0;
				
			e = expr_func(sym);
			yyvsp[-4].exprV = e;
			emit(funcstart,yyvsp[-4].exprV,NULL,NULL,0,yylineno);
			//$$ = $2;
			;}
    break;

  case 78:
#line 763 "parser.y"
    {prfundef++;scopeSpaceCounter++;formalArgOffset = -1;
				/*loopcntstack = insertf(loopcntstack, loopcounter);loopcounter = 0;*/
			;}
    break;

  case 79:
#line 765 "parser.y"
    {		
											loopcounter --;
											/*loopcounter = loopcntstack->fscope;
											loopcntstack = delete(loopcntstack);*/
											scopeSpaceCounter = scopeSpaceCounter - 2;
											printf("funcdef->function id ( idlist ) block\n");
											scopeDead = scope+1;
											printf("dead scope:%d\n",scopeDead);
											dead(scopeDead);
											scopeDead--;
											if(scopeDead != 0) {
												//printf("dead scope:%d\n",scopeDead);
												dead(scopeDead);
											}
											scope--;
											prfundef--;
											if(fs->next != NULL){
												for(i=fs->fscope;i>fs->next->fscope;i--){	
													//printf("unhide scope:%d\n",i);
													unhide(i);
												}
											}				
											fs = delete(fs);
											emit(funcend,yyvsp[-8].exprV,NULL,NULL,0,yylineno);
											yyval.exprV = yyvsp[-8].exprV;
			;}
    break;

  case 80:
#line 793 "parser.y"
    {printf("id->ID\n");;}
    break;

  case 81:
#line 794 "parser.y"
    {printf("NO rule id\n");
				anonymous = 1;;}
    break;

  case 82:
#line 798 "parser.y"
    {	struct expr *e = newexpr_intnum(yyvsp[0].intV);
						yyval.exprV = e;
						printf("const->intconst\n");
					;}
    break;

  case 83:
#line 802 "parser.y"
    {	struct expr *e = newexpr_floatnum(yyvsp[0].doubleV);
						yyval.exprV = e;
						printf("const->doubleconst\n");
					;}
    break;

  case 84:
#line 806 "parser.y"
    {	struct expr *e = newexpr_conststring(yyvsp[0].stringV);
						yyval.exprV = e;
						printf("const->string\n");
					;}
    break;

  case 85:
#line 810 "parser.y"
    {	struct expr *e = newexpr_nil(yyvsp[0].stringV);
						yyval.exprV = e;
						printf("const->nil\n");
					;}
    break;

  case 86:
#line 814 "parser.y"
    {	struct expr *e = newexpr_constbool(1u);
						yyval.exprV = e;
						printf("const->true\n");
					;}
    break;

  case 87:
#line 818 "parser.y"
    {	struct expr *e = newexpr_constbool(0u);
						yyval.exprV = e;
						printf("const->false\n");
					;}
    break;

  case 88:
#line 824 "parser.y"
    {printf("idlist->idlist1\n");;}
    break;

  case 89:
#line 827 "parser.y"
    {printf("idlist->ID idlist2\n");
					args[argsNum+1] = strdup(yyvsp[-1].stringV);
					if(lookup_one_level(yyvsp[-1].stringV, scope)==0)
					{
						argsNum++;
						formalArgOffset++;
						insert(scope,1,create_v(yyvsp[-1].stringV,scope,yylineno,currscopespace(),formalArgOffset),NULL, NULL);}
					else{printf("error:Redefine argument at line:%d\n",yylineno);error = 1;}
					;}
    break;

  case 90:
#line 836 "parser.y"
    {printf("NO rule\n");;}
    break;

  case 91:
#line 839 "parser.y"
    {printf("idlist2->, idlist1\n");
					
					;}
    break;

  case 92:
#line 842 "parser.y"
    {printf("NO rule idlist2\n");;}
    break;

  case 93:
#line 847 "parser.y"
    {patchlabel(yyvsp[-1].intV, nextquadlabel());printf("ifstmt->if ( expr ) stmt\n");;}
    break;

  case 94:
#line 850 "parser.y"
    {emit(if_eq, yyvsp[-1].exprV, newexpr_constbool(1u), NULL, nextquadlabel() + 2, yylineno);
							yyval.intV = nextquadlabel();
							emit(jump, NULL, NULL, NULL, 0, yylineno);
						  ;}
    break;

  case 95:
#line 856 "parser.y"
    {
									printf("ifstmt->if ( expr ) stmt elsestmt\n");
									patchlabel(yyvsp[-3].intV, yyvsp[-1].intV+1);
									patchlabel(yyvsp[-1].intV,nextquadlabel());
								;}
    break;

  case 96:
#line 866 "parser.y"
    {
					yyval.intV = nextquadlabel();
					emit(jump, NULL, NULL, NULL, 0, yylineno);
				    ;}
    break;

  case 97:
#line 873 "parser.y"
    {	
								struct funcdef_scope *tmp;
								emit(jump, NULL, NULL, NULL, yyvsp[-2].intV, yylineno);
								patchlabel(yyvsp[-1].intV, nextquadlabel());
								printf("whilestmt->while ( expr ) stmt\n");
								
								if(b!=NULL)
									patchlabel(b->fscope, nextquadlabel());
								if(c!=NULL)
									patchlabel(c->fscope, yyvsp[-2].intV);
									
								for(tmp=b;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									b=delete(b);
									patchlabel(b->fscope, nextquadlabel());
									
								}	
								for(tmp=c;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									c=delete(c);
									patchlabel(c->fscope, yyvsp[-2].intV);
									
								}
									
								b=delete(b);
								c=delete(c);
								
								breakcntblock--;
								;}
    break;

  case 98:
#line 902 "parser.y"
    {
						breakcntblock++;
						yyval.intV = nextquadlabel();;}
    break;

  case 99:
#line 907 "parser.y"
    {emit(if_eq, yyvsp[-1].exprV, newexpr_constbool(1u), NULL, nextquadlabel() + 2, yylineno);
						yyval.intV = nextquadlabel();
						emit(jump, NULL, NULL, NULL, 0, yylineno);;}
    break;

  case 100:
#line 913 "parser.y"
    {yyval.intV = nextquadlabel();emit(jump, NULL, NULL, NULL, 0, yylineno);;}
    break;

  case 101:
#line 916 "parser.y"
    {yyval.intV = nextquadlabel();;}
    break;

  case 102:
#line 920 "parser.y"
    {otinanai = 0;;}
    break;

  case 103:
#line 923 "parser.y"
    {otinanai =6;;}
    break;

  case 104:
#line 926 "parser.y"
    {yyval.exprV = yyvsp[-1].exprV;;}
    break;

  case 105:
#line 932 "parser.y"
    {
				breakcntblock++;
				yyval.forV->test = yyvsp[-2].intV;
				yyval.forV->enter = nextquadlabel();
				emit(if_eq,yyvsp[-1].exprV, newexpr_constbool(1u), NULL, 0, yylineno);
	
			;}
    break;

  case 106:
#line 941 "parser.y"
    {
								struct funcdef_scope *tmp;
								printf("forstmt->for (elist;expr;elist) stmt\n");
								patchlabel(yyvsp[-6].forV->enter, yyvsp[-2].intV + 1);
								patchlabel(yyvsp[-5].intV, nextquadlabel());
								patchlabel(yyvsp[-2].intV, yyvsp[-6].forV->test);
								patchlabel(yyvsp[0].intV, yyvsp[-5].intV+1);
								if(b!=NULL)
									patchlabel(b->fscope, nextquadlabel());
								if(c!=NULL)
									patchlabel(c->fscope, yyvsp[-5].intV+1);
									
								for(tmp=b;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									b=delete(b);
									patchlabel(b->fscope, nextquadlabel());
									
								}	
								for(tmp=c;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									c=delete(c);
									patchlabel(c->fscope, yyvsp[-5].intV+1);
									
								}
									
								b=delete(b);
								c=delete(c);
								breakcntblock--;
								;}
    break;

  case 107:
#line 970 "parser.y"
    {
	
									if(prfundef > 0){
										emit(ret, NULL, NULL, yyvsp[-1].exprV, 0, yylineno);
									}
									else{
										printf("error: return statement not in a function at line :%d\n", yylineno);
										error = 1;
									}
										
	
									printf("returnstmt->return returnstmt2 ;\n");
									yyval.exprV=yyvsp[-1].exprV	;
								;}
    break;

  case 108:
#line 986 "parser.y"
    {yyval.exprV = yyvsp[0].exprV; printf("returnstmt2->expr\n");;}
    break;

  case 109:
#line 987 "parser.y"
    {yyval.exprV=NULL;printf("NO rule returnstmt2\n");;}
    break;

  case 110:
#line 990 "parser.y"
    {printf("loc->local\n");;}
    break;

  case 111:
#line 991 "parser.y"
    {printf("loc->$\n");;}
    break;

  case 112:
#line 994 "parser.y"
    {printf("glob->global\n");;}
    break;

  case 113:
#line 995 "parser.y"
    {printf("glob->::\n");;}
    break;


    }

/* Line 1010 of yacc.c.  */
#line 2664 "parser.tab.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  const char* yyprefix;
	  char *yymsg;
	  int yyx;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 0;

	  yyprefix = ", expecting ";
	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		yysize += yystrlen (yyprefix) + yystrlen (yytname [yyx]);
		yycount += 1;
		if (yycount == 5)
		  {
		    yysize = 0;
		    break;
		  }
	      }
	  yysize += (sizeof ("syntax error, unexpected ")
		     + yystrlen (yytname[yytype]));
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yyprefix = ", expecting ";
		  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			yyp = yystpcpy (yyp, yyprefix);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yyprefix = " or ";
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* If at end of input, pop the error token,
	     then the rest of the stack, then return failure.  */
	  if (yychar == YYEOF)
	     for (;;)
	       {
		 YYPOPSTACK;
		 if (yyssp == yyss)
		   YYABORT;
		 YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
		 yydestruct (yystos[*yyssp], yyvsp);
	       }
        }
      else
	{
	  YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
	  yydestruct (yytoken, &yylval);
	  yychar = YYEMPTY;

	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

#ifdef __GNUC__
  /* Pacify GCC when the user code never invokes YYERROR and the label
     yyerrorlab therefore never appears in user code.  */
  if (0)
     goto yyerrorlab;
#endif

  yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 998 "parser.y"


/*Gia na doume an einai programvar,formalarg i metabliti mesa se function*/
enum scopespace_t currscopespace(void){

	if(scopeSpaceCounter == 1){
		return programvar;
	}
	else
	if(scopeSpaceCounter % 2 == 0){
		return formalarg;
	}
	else 
		return	functionlocal;

}



struct funcdef_scope *insertf(struct funcdef_scope *head, int x){
	struct funcdef_scope * tmp;

	if(head==NULL)/*eidiki perisptosi gia keni lista*/
	{
		head = (struct funcdef_scope *)malloc(sizeof(struct funcdef_scope));
		head->fscope = x;
		head->next = NULL;
		head->foffset= -1 ;
		return head;
	}
	
	tmp = (struct funcdef_scope *)malloc(sizeof(struct funcdef_scope));
	tmp->fscope = x;
	tmp->next = head;
	tmp->foffset= -1 ;
	
	head = tmp;
	return head;
	
}

struct funcdef_scope *delete(struct funcdef_scope *head){
	struct funcdef_scope *tmp = head;	
	if(head == NULL) {
		return NULL;
	}		

	else {
		head = head->next;
		free(tmp);
	}
	return head;
}

int main(int argc, char** argv)
{
	
	srand(time(NULL));	

	alpha = (struct alpha_token_t *)malloc(sizeof(struct alpha_token_t));
	alpha->token = (char *)malloc(sizeof(char)*1000);
	yylval1 = (void *)alpha;

	if(argc > 0)
		yyin = fopen(argv[1], "r");
	else
		yyin = stdin;

	initHash();
	yyparse();
	printf("\n\n");
	if(error==0){
		printAll();
		printf("\n\n");
		//print_quads();
		//print_quads_tofile();
		generate_all();
		print_instr_tofile();
	
		write_binary(numConsts,stringConsts,userFuncs,nameLibFuncs,instr,totalNumConsts,totalStringConsts,totalNamedLibfuncs,totalUserFuncs,num_progVars,curr_incode);
	}
	else{
		printf("Error at compile.No executable created!!!!!\n");
	}

	
	fclose(yyin);
	
	return 0;
}

int yyerror(char *yaccProvideMessage){
	fprintf(stderr,"%s at line %d ,before token:%s\n",yaccProvideMessage,yylineno,yytext);
	return 0;
}


