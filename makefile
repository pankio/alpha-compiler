parser:	parser.tab.o lex.yy.o Symbol_table.o inCode.o tarCode.o binary.o
			gcc -g  parser.tab.o lex.yy.o Symbol_table.o inCode.o tarCode.o binary.o -o parser
			cp parser ./eksetasi3			
			
parser.tab.o:	parser.tab.c
			gcc -g -c  parser.tab.c
			
binary.o:	binary.c
			gcc -g -c binary.c
			
binary.c:	binary.h

parser.tab.c:	parser.y
			bison -v -d -o parser.tab.c parser.y

parser.y:	inter.h scanner.h symboltable.h

lex.yy.o:	lex.yy.c
			gcc -g -c lex.yy.c

lex.yy.c:	scanner.l
			flex scanner.l

scanner.l:	parser.tab.h inter.h

Symbol_table.o:	Symbol_table.c
			gcc -g -c  Symbol_table.c

Symbol_table.c:	symboltable.h

inCode.o:	inCode.c
			gcc -g -c inCode.c

inCode.c:	inCode.h symboltable.h

tarCode.o:	tarCode.c
			gcc -g -c tarCode.c

tarCode.c:	tarCode.h inCode.h symboltable.h

clean:
			rm *.o
			rm parser
			rm quads.txt
			rm tcode.txt
			rm *.abc
