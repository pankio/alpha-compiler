#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "symboltable.h"
# define MULTIPLIER 32

int MAX = 990;


unsigned int hash(const char *name,unsigned int l){
	unsigned int h;
	unsigned char *p;

	h=0;
	for(p=(unsigned char *)name;*p!='\0';p++){
		h=MULTIPLIER*h+*p;
	}
	return h%l;
}

/////////////////////////////////////////////////////

void hide(int scope){
	int i=0,j;
	struct symbol_table *tmp;
	struct symbol *tmp1;
	
	i=lookup_up_dead(scope);
	if (sym_tabl[i] != NULL){
		tmp=sym_tabl[i];
		for (j=0;j<k;j++){
			tmp1=tmp->hashtable[j];
			while(tmp1!=NULL){
				//printf("scope:%d\n",i);
				if (tmp1->type==1) {
					tmp1->active=0;
				}
				tmp1=tmp1->next;
			}
		}
	}
}

void unhide(int scope){
	int i=0,j;
	struct symbol_table *tmp;
	struct symbol *tmp1;
	
	i=lookup_up_dead(scope);
	if (sym_tabl[i] != NULL){
		tmp=sym_tabl[i];
		for (j=0;j<k;j++){
			tmp1=tmp->hashtable[j];
			while(tmp1!=NULL){
				//printf("index:%d %d\n",i,j);
				if (tmp1->type==1) {
					tmp1->active=1;
				}
				tmp1=tmp1->next;
			}
		}
	}
}

/////////////////////////////////////////////////////
/*otan dead einai 0 einai nekro!!!!*/
void dead(int scope){//to scope einai to scope prin meiwthei
	int i=0;
	
	i=lookup_up_dead(scope);
	if (sym_tabl[i] != NULL){
		sym_tabl[i]->dead=0;
	}
}

/////////////////////////////////////////////////////
/*Psaxnei na brei an iparxei energo epipedo ston symblo_table me scope idio me tou orismatos*/

int lookup_up_dead(int scope){
	int i=0;

	while (sym_tabl[i]!=NULL){
		//if (sym_tabl[i]->scope > scope) return scope;
		if ((sym_tabl[i]->scope == scope) && (sym_tabl[i]->dead == 1)) {
			return i;
		}
		i++;
	}
	return i;
}

/////////////////////////////////////////////////////
/*I sinartisi create_v dimiourgei enan kombo xrisimopoiwntas enan pointer tupou varab o opoios periexei ta stoxeia mias metavlitis k ton epistrefei stin sinartisi insert opou k kaleitai*/

struct varab *create_v(char *name,  int scope, int line, enum scopespace_t space, int offset)
{
	struct varab *current;
	
	current=(struct varab *)malloc(sizeof(struct varab));
	current->name = strdup(name);
	current->scope = scope;
	current->line = line;
	current->space = space;
	current->offset = offset;
	return current;
}

//////////////////////////////////////////////////////
/*i sinartisi create_f dimiourgei enan kombo xrisimopoiwntas enan pointer tupou function o opoios periexei ta stoixeia mias sinartisis k ton epistrefei stin sinartisi insert opou k kaleitai
*N_O_A=plithos orismatwn tis sinartisis!!
*/

struct function *create_f(char *name, char **args, int scope, int line, int N_O_A)
{
	struct function *current;

	int i;

	current=(struct function *)malloc(sizeof(struct function));
	current->name = strdup(name);
	if(N_O_A > 0){
		current->args = (char **)malloc(sizeof(char *)*N_O_A);
		for (i=0;i<N_O_A;i++){
	    		current->args[i] = (char *)malloc(sizeof(char)*100);
	    		current->args[i] = strdup(args[i]);
		}
	}
	current->scope = scope;
	current->line = line;
	current->localsize = N_O_A;
	current->space = function;
	return current;
}


struct libfunction *create_lf(char *name,  int scope)
{
	struct libfunction *current;
	
	current=(struct  libfunction*)malloc(sizeof(struct libfunction));
	current->name = strdup(name);
	current->scope = scope;
	return current;
}

//////////////////////////////////////////////////////

struct symbol *insert(int scope, int type, struct varab *new, struct function *new1, struct libfunction *new2)
{
	struct symbol *tmp;
	int j,i;//hash
	int sc;//to epipedo pou douleyoume
	char *name;//Xreiazetai stin lookup
	
	if (sym_tabl == NULL){
		sym_tabl = (struct symbol_table**)malloc(sizeof(struct symbol_table*)*1000);
	}
	if (sym_tabl[MAX]!=NULL){
		MAX=MAX+1000;
		sym_tabl = (struct symbol_table**)realloc(sym_tabl,sizeof(struct symbol_table*)*(MAX+10));
	}
	if (type == 0){
		name=strdup(new2->name);
		j=hash(name,k);
	}
	else if (type == 1){
		name=strdup(new->name);
		j=hash(name,k);
	}
	else if (type == 2){	
		name=strdup(new1->name);		
		j=hash(name,k);
	}
	else return;
	sc=lookup_up_dead(scope);
	if((sc>0) && (scope-sym_tabl[sc-1]->scope>1)){
		for(i=sc;i<scope;i++){
			sym_tabl[i]=(struct symbol_table *)malloc(sizeof(struct symbol_table));
	     		sym_tabl[i]->hashtable=(struct symbol **)calloc(k, sizeof(struct symbol *));
		}
		sc=i;
	}
        if(sym_tabl[sc] == NULL){
             sym_tabl[sc]=(struct symbol_table *)malloc(sizeof(struct symbol_table));
	     sym_tabl[sc]->hashtable=(struct symbol **)calloc(k, sizeof(struct symbol *));
	}
	tmp = (struct symbol *)malloc(sizeof(struct symbol));
	if (sym_tabl[sc]->hashtable[j] == NULL)/*I lista einai keni*/
	{
		//sym_tabl[sc]->hashtable[j]=(struct symbol *)malloc(sizeof(struct symbol));
		sym_tabl[sc]->scope=scope;
		sym_tabl[sc]->dead=1;
		tmp->active=1;
		tmp->type=type;
		if(type==1)
		{
                       	tmp->u.v=new;
			tmp->next=NULL;
			sym_tabl[sc]->hashtable[j]=tmp;
		}
		else if(type==2)
		{
			tmp->u.f=new1;
			tmp->next=NULL;
			sym_tabl[sc]->hashtable[j]=tmp;
		}
		else if(type==0)
		{
			tmp->u.lf=new2;
			tmp->next=NULL;
			sym_tabl[sc]->hashtable[j]=tmp;
		}
	
	
        	return sym_tabl[sc]->hashtable[j];

	}else if (sym_tabl[sc]->hashtable[j] != NULL)//Eisagwgh kombou sto head se mi keni lista 
	{
		//if (lookup_one_level(name, j, sc)==0){return;}	
		//j=hash(name,k);
		sym_tabl[sc]->scope=scope;
		sym_tabl[sc]->dead=1;
		tmp->active=1;
		tmp->type=type;
		if(type==1)
		{
			tmp->u.v=new;
			tmp->next=sym_tabl[sc]->hashtable[j];
			sym_tabl[sc]->hashtable[j]=tmp;
		}
		else if(type==2)
		{
			tmp->u.f=new1;
			tmp->next=sym_tabl[sc]->hashtable[j];
			sym_tabl[sc]->hashtable[j]=tmp;
		}
		else if(type==0)
		{
			tmp->u.lf=new2;
			tmp->next=NULL;
			sym_tabl[sc]->hashtable[j]=tmp;
		}
		return sym_tabl[sc]->hashtable[j];
	}
}

//////////////////////////////////////////////////////

int lookup_one_level (char *name, int scope)
{
	struct symbol_table *l;
	struct symbol *tmp;
	int i = hash(name, k);
       	if (sym_tabl[scope] == NULL){
	       	//printf("\nItem not found!\n");
		return 0;
	}
       	scope=lookup_up_dead(scope);
	if(sym_tabl[scope] == NULL) {
		return 0;
	}
	l = sym_tabl[scope];
        if(l->hashtable[i]!=NULL && l->dead==1){
		tmp = l->hashtable[i];
		while(tmp!=NULL){
			if ((strcmp(tmp->u.v->name,name)==0) && (tmp->active==1))
			{
			
				return 1;
			}
			if ((strcmp(tmp->u.f->name,name)==0) /*&& (tmp->active==1)*/ && tmp->type==2)
			{
				//printf("---------------------------active=%s\n",tmp->u.f->name);
				return 1;
			}
			tmp=tmp->next;
		}
	}	
	//printf("Item not found!\n");
	return 0;
}

struct symbol *lookup_retSym (char *name, int scope)
{
	struct symbol_table *l;
	struct symbol *tmp;
	int i = hash(name, k);
       	if (sym_tabl[scope] == NULL){
	       
		return NULL;
	}
       	scope=lookup_up_dead(scope);
	if(sym_tabl[scope] == NULL) {
		return NULL;
	}
	l = sym_tabl[scope];
        if(l->hashtable[i]!=NULL && l->dead==1){
		tmp = l->hashtable[i];
		while(tmp!=NULL){
			if ((strcmp(tmp->u.v->name,name)==0) && (tmp->active==1))
			{
				//printf("-----------------------name=%s\n",tmp->u.v->name);
				return tmp;
			}
			if ((strcmp(tmp->u.f->name,name)==0) /*&& (tmp->active==1)*/)
			{
				return tmp;
			}
			tmp=tmp->next;
		}
	}	
	
	return NULL;
}

int lookup_total(char *name) {
	int i = 0;
	
	while(sym_tabl[i]!=NULL) {
		if(lookup_one_level(name, i)==1) {
			return 1;
		}
		i++;
	}
	return 0;	
}

void initHash() {
	insert(0, 0, NULL, NULL, create_lf("print", 0));
	insert(0, 0, NULL, NULL, create_lf("input", 0));
	insert(0, 0, NULL, NULL, create_lf("objectmemberkeys", 0));
	insert(0, 0, NULL, NULL, create_lf("objecttotalmembers", 0));
	insert(0, 0, NULL, NULL, create_lf("objectcopy", 0));
	insert(0, 0, NULL, NULL, create_lf("totalarguments", 0));
	insert(0, 0, NULL, NULL, create_lf("argument", 0));
	insert(0, 0, NULL, NULL, create_lf("typeof", 0));
	insert(0, 0, NULL, NULL, create_lf("strtonum", 0));
	insert(0, 0, NULL, NULL, create_lf("sqrt", 0));
	insert(0, 0, NULL, NULL, create_lf("cos", 0));
	insert(0, 0, NULL, NULL, create_lf("sin", 0));		
}

void printAll() {
	struct symbol_table *tmp;
	struct symbol *tmp1;
	int i = 0, j;
	
	while(sym_tabl[i]!=NULL) {
		tmp=sym_tabl[i];
		for (j=0;j<k;j++){
			tmp1=tmp->hashtable[j];
			while(tmp1!=NULL){
				if(tmp1->type == 1) 
					printf("Scope:%d, Name:%s, Type:V, Line:%d, ScopeSpace:%d, Offset:%d\n",tmp->scope,tmp1->u.v->name, tmp1->u.v->line,tmp1->u.v->space, tmp1->u.v->offset);
				else if(tmp1->type == 2)
					printf("Scope:%d, Name:%s, Type:F, Line:%d\n",tmp->scope,tmp1->u.f->name, tmp1->u.f->line);
				else
					printf("Scope:%d, Name:%s, Type:LF\n",tmp->scope,tmp1->u.lf->name);

						
				tmp1=tmp1->next;
			}
		}
		i++;
	}
}

void freeAll() {
}

char *create_funcName(char *name){
   char* name2;  
   int size;
  	 
	name=(char*)malloc(5*sizeof(char));
	name2=(char *)malloc(20*sizeof(char));
    
	name = strcpy(name,"#tmp");
	//name2=(char *)lltostr(rand()%1000,name2);
	sprintf(name2,"%d", rand()%1000);
	name=strcat(name,name2);
    
	size=strlen(name);
	name[7]='\0';
   	
	return name;	
}

