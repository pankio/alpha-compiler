%{
	#include<stdio.h>
	#include<string.h>
	#include<stdlib.h>
	#include<math.h>
	#include"inter.h"
	#include"parser.tab.h"
	//#define YY_DECL int alpha_yylex(yylval) void * yylval;
	
	int aa = 1;
	int slash = 0;
	int par = 0;

%}

%option yylineno
%option noyywrap
%option header-file="./scanner.h"

id		[[:alpha:]][a-zA-Z0-9_]*
string 	["\""]([^\"^\\]|"\\\""|("\\".))*["\""]
space	[[:space:]]

%x COMMENT COM

%%

if		{
			yylval.stringV = strdup(yytext);
			((att *)yylval1)->token = strdup(yytext);
			((att *)yylval1)->line = yylineno;
			((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = IF_KEYWORD;
			return IF_KEYWORD;
		}

else		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = ELSE_KEYWORD;
			return ELSE_KEYWORD;
		}

while		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = WHILE_KEYWORD;
			return WHILE_KEYWORD;
		}

for		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = FOR_KEYWORD;
			return FOR_KEYWORD;
		}

function	{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = FUNCTION_KEYWORD;
			return FUNCTION_KEYWORD;
		}

return	{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = RETURN_KEYWORD;
			return RETURN_KEYWORD;
		}

break	{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = BREAK_KEYWORD;
			return BREAK_KEYWORD;
		}

continue	{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = CONTINUE_KEYWORD;
			return CONTINUE_KEYWORD;
		}

and		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = AND_KEYWORD;
			return AND_KEYWORD;
		}

not		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = NOT_KEYWORD;
			return NOT_KEYWORD;
		}

or		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = OR_KEYWORD;
			return OR_KEYWORD;
		}

local		{

	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = LOCAL_KEYWORD;
			return LOCAL_KEYWORD;
		}

global	{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = GLOBAL_KEYWORD;
			return GLOBAL_KEYWORD;
		}

true		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = TRUE_KEYWORD;
			return TRUE_KEYWORD;
		}

false		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = FALSE_KEYWORD;
			return FALSE_KEYWORD;
		}

nil		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = NIL_KEYWORD;
			return NIL_KEYWORD;
		}

"::"	{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = D_FULLSTP;
			return  D_FULLSTP;
		}

"$"	{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
		   	((att *)yylval1)->type = DOLLAR;
			return DOLLAR;

		}

"="		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = ASSIGN_OPERATOR;
			return ASSIGN_OPERATOR;
		}

"+"		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = PLUS_OPERATOR;
			return PLUS_OPERATOR;
		}

"-"		{
			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = MINUS_OPERATOR;
			return MINUS_OPERATOR;
		}

"*"		{
			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = MUL_OPERATOR;
			return MUL_OPERATOR;
		}

"/"		{
			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type =DIV_OPERATOR;
			return DIV_OPERATOR;
		}

"%"		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = MOD_OPERATOR;
			return MOD_OPERATOR;
		}

"=="		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = EQUAL_OPERATOR;
			return EQUAL_OPERATOR;
		}

"!="		{
			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = NEQ_OPERATOR;
			return NEQ_OPERATOR;
		}

"++"		{
			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = PLUSPLUS_OPERATOR;
			return PLUSPLUS_OPERATOR;
		}

"--"		{
			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = MINUSMINUS_OPERATOR;
			return MINUSMINUS_OPERATOR;
		}

">"		{
				yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = GRE_OPERATOR;
			return GRE_OPERATOR;
		}

"<"		{
			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = SMA_OPERATOR;
			return SMA_OPERATOR;
		}

">="		{
		yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = GREQ_OPERATOR;
			return GREQ_OPERATOR;
		}

"<="		{
		yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = SMAQ_OPERATOR;
			return SMAQ_OPERATOR;
		}

"{"		{

		yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = LAG;
			return LAG;
		}

"}"		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = DAG;
			return DAG;
		}

"["		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = LBRA;
			return LBRA;
		}

"]"		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = DBRA;
			return DBRA;
		}

"."		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = FULLST;
			return FULLST;
		}

"("		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = LPAR;
			return LPAR;
		}

")"		{
	yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = DPAR;
			return DPAR;
		}

";"		{

			yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = QUESTION;
			return QUESTION;
		}

","		{
			yylval.stringV = strdup(yytext);
			((att *)yylval1)->token = strdup(yytext);
			((att *)yylval1)->line = yylineno;
			((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = COMA;
			return COMA;
		}

":"		{
			yylval.stringV = strdup(yytext);
			((att *)yylval1)->token = strdup(yytext);
			((att *)yylval1)->line = yylineno;
			((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = DFULLST;
			return DFULLST;
		}


[0-9]+	{
			yylval.intV = atoi(yytext);
			((att *)yylval1)->valueN = atoi(yytext);
			((att *)yylval1)->line = yylineno;
			((att *)yylval1)->aa = aa++;
			((att *)yylval1)->type = INTCONST;
			return INTCONST;
		}

[0-9]+"."[0-9]+	{
				yylval.doubleV = atof(yytext);
				((att *)yylval1)->valueD = atof(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
				((att *)yylval1)->type = DOUBLECONST;
				return DOUBLECONST;
			}

{id}		{
				yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
				((att *)yylval1)->type = ID;
				return ID;
			}

{string}	{
				yylval.stringV = strdup(yytext);
				((att *)yylval1)->token = strdup(yytext);
				((att *)yylval1)->line = yylineno;
				((att *)yylval1)->aa = aa++;
				((att *)yylval1)->type = STRING;
				return STRING;
			}

{space}	{
			;
		}

"#"[^\n]*		{
						printf("sxolia me #\n");
					}

"//"[^\n]*			{
						printf("sxolia me //\n");
					}

<INITIAL,COMMENT,COM>"/*"				{
											slash++;
											BEGIN(COMMENT);
										}

<COMMENT>.				{		;		}

<COMMENT>\n		{		;		}

<COMMENT,INITIAL,COM>"(*"				{
											par++;
											BEGIN(COM);
										}

<COMMENT>"*/"				{
								slash--;
								if(par!=0 && slash==0) BEGIN(COM);
								else if(par==0 && slash!=0) BEGIN(COMMENT);
								else BEGIN(INITIAL);
							}

<COM>.					{		;		}

<COM>\n				{		;		}

<COM>"*)"					{
								par--;
								if(par!=0 && slash==0) BEGIN(COM);
								else if(par==0 && slash!=0) BEGIN(COMMENT);
								else BEGIN(INITIAL);
							}

<INITIAL,COMMENT,COM><<EOF>>		{
												if(slash!=0) printf("Unfinished comments 1\n");
												if(par!=0) printf("Unfinished comments 2\n");
												return EOF;
											}

.						{
							((att *)yylval1)->token = strdup(yytext);
							((att *)yylval1)->line = yylineno;
							((att *)yylval1)->aa = aa++;
							((att *)yylval1)->type = ERRORR;
							return ERRORR;
						}
%%
