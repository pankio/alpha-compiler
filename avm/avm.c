#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "avm.h"


#define AVM_STACKENV_SIZE 4
#define avm_error printf
#define avm_warning printf
#define AVM_ENDING_PC codeSize
#define AVM_NUMACTUALS_OFFSET +4
#define AVM_SAVEDTOPSP_OFFSET +1
#define AVM_SAVEDPC_OFFSET +3
#define AVM_SAVEDTOP_OFFSET +2
#define AVM_WIPEOUT(m) memset(&(m), 0, sizeof(m))
#define AVM_MAX_INSTRUCTIONS (unsigned) getretval_v

#define MULTIPLIER 32


extern double  *numConst;
extern char ** stringConst;
extern struct userfunc ** userFunc;
extern char **nameLibFunc;
extern int totalInstr;

//struct instruction *instrs;


typedef double (*arithmetic_func_t)(double x, double y);
unsigned totalActuals = 0;


typedef char* (*tostring_func_t)(struct avm_memcell*);



struct library_map *map[13];
struct avm_memcell ax, bx, cx;
struct avm_memcell retval;
unsigned top = 4095, topsp = 4094;

extern int totalVars;
extern int totalInstr;
extern int totalUser;
extern int totalLib;
extern int totalStr;
extern int totalNum;

double consts_getnumber(unsigned index);
char * consts_getstring(unsigned index);
char * libfuncs_getused(unsigned index);


extern char* avm_tostring(struct avm_memcell*);
extern void avm_calllibfunc(char *funcName);
extern void avm_callsaveenvironment(void);


unsigned char executionFinished = 0;
unsigned pc = 0;
unsigned currLine = 0;
unsigned codeSize = 0;
struct instruction* code = (struct instruction*)0;

extern char * number_tostring (struct avm_memcell*);
extern char * string_tostring (struct avm_memcell*);
extern char * bool_tostring (struct avm_memcell*);
extern char * table_tostring (struct avm_memcell*);
extern char * userfunc_tostring (struct avm_memcell*);
extern char * libfunc_tostring (struct avm_memcell*);
extern char * nill_tostring (struct avm_memcell*);
extern char * undef_tostring (struct avm_memcell*);

double  *numConsts;
char ** strConsts;
struct userfunc ** userF;
char **libF;
struct instruction *instruct;

library_func_t libs[]={
	libraryfunc_print,
	libfunc_typeof,
	libfunc_totalarguments,
	libraryfunc_cos,
	libraryfunc_sin,
	libraryfunc_strtonum
};

tostring_func_t tostringFuncs[] = {
	number_tostring,
	string_tostring, 
	bool_tostring, 
	table_tostring, 
	userfunc_tostring, 
	libfunc_tostring, 
	nill_tostring, 
	undef_tostring 
};


unsigned int hash(const char *name,unsigned int l){
	unsigned int h;
	unsigned char *p;

	h=0;
	for(p=(unsigned char *)name;*p!='\0';p++){
		h=MULTIPLIER*h+*p;
	}
	return h%l;
}


extern char * number_tostring (struct avm_memcell* name){
	char *str = (char *)malloc(sizeof(char)*10);
	int size;
	
	sprintf(str,"%lf", name->data.numVal);
	size=strlen(str);
	str[size]='\0';

	return str;
}

extern char * string_tostring (struct avm_memcell* name){
	return name->data.strVal;
}

extern char * bool_tostring (struct avm_memcell* name){
	if(name->data.boolVal == 1u){
		return "true";
	}
	return "false";
}

extern char * table_tostring (struct avm_memcell* name){
	struct avm_table_bucket **str = name->data.tableVal->strIndexed;
	struct avm_table_bucket **num = name->data.tableVal->numIndexed;
	struct avm_table_bucket *tmp;
	
	int i;

	char *dest = (char *) malloc(sizeof(char) * 1000);
	
	
	for(i=0; i<211; i++){
		tmp = num[i];	
		while(tmp!=NULL){
			if(tmp->value->type == number_m){
				dest = strcat(dest, number_tostring(tmp->value));
				dest = strcat(dest,", ");
			}
			else{
				dest = strcat(dest,tmp->value->data.strVal);
				dest = strcat(dest,", ");
			}
			tmp=tmp->next;
		}

		tmp = str[i];	
		while(tmp!=NULL){
			if(tmp->value->type == number_m){
				dest = strcat(dest, number_tostring(tmp->value));
				dest = strcat(dest,", ");
			}
			else{
				dest = strcat(dest,tmp->value->data.strVal);
				dest = strcat(dest,", ");
			}
			tmp=tmp->next;
		}
		
	}

	return dest;
}

extern char * userfunc_tostring (struct avm_memcell* name){
	return userFunc[name->data.funcVal]->id;
}

extern char * libfunc_tostring (struct avm_memcell* name){
	return name->data.libfuncVal;
}

extern char * nill_tostring (struct avm_memcell* name){
	return "nil";
}

extern char * undef_tostring (struct avm_memcell* name){
	return "undef";
}

void avm_memcellclear(struct avm_memcell* m){
	void(*f)(struct avm_memcell *);
	if (m->type != undef_m){
		f = memclearFuncs[m->type];
	       	if (f)
			(*f)(m);
		m->type = undef_m;
	}
}


void avm_tablebucketsdestroy(struct avm_table_bucket **p){
	unsigned i;
	struct avm_table_bucket *b,*del;
	
	for(i=0;i<AVM_TABLE_HASHSIZE;i++){
		for(b = *p ; b;){
			del = b;
			b=b->next;
			avm_memcellclear(del->key);
			avm_memcellclear(del->value);
			free(del);
		}
		p[i] = (struct avm_table_bucket*)0; 
	}	
	
	
}


void avm_tabledestroy(struct avm_table *table){
	avm_tablebucketsdestroy(table->strIndexed);
	avm_tablebucketsdestroy(table->numIndexed);
	free(table);
	
}

void avm_tabledecrefcounter(struct avm_table* table){
	assert( table->refcounter > 0 );
	if ( ! --table->refcounter )
		avm_tabledestroy(table);
}

void avm_tableincrefcounter(struct avm_table* table){
	++table->refcounter;
}


void avm_tablebucketsinit(struct avm_table_bucket **p){
	unsigned i;
	
	for(i=0;i<AVM_TABLE_HASHSIZE;++i){
		p[i] = (struct avm_table_bucket*)0;
	}
}

struct avm_table* avm_tablenew(void){
	struct avm_table *t = (struct avm_table *) malloc(sizeof(struct avm_table));
	AVM_WIPEOUT(*t);
	
	t->refcounter = t->total = 0;
	avm_tablebucketsinit(t->numIndexed);
	avm_tablebucketsinit(t->strIndexed);
	
	return t;
}

void avm_initcodentables(FILE *bfile){
	read_binary();

	codeSize = totalInstr;
	numConsts = numConst;
	strConsts = stringConst;
	userF = userFunc;
	libF = nameLibFunc;
	code = instrs;
}

/*Dispatcher just for arithmetic functions*/
arithmetic_func_t arithmeticFuncs[] = {
	add_impl,
	sub_impl,
	mul_impl,
	div_impl,
	mod_impl
};

typedef unsigned char (*tobool_func_t)(struct avm_memcell *);

tobool_func_t toboolFuncs[] = {
	number_tobool,
	string_tobool,
	bool_tobool,
	table_tobool,
	userfunc_tobool,
	libfunc_tobool,
	nil_tobool,
	undef_tobool

};

/*EQUALITY OPERATIONS*/

unsigned char number_tobool (struct avm_memcell *m){return m->data.numVal != 0;}
unsigned char string_tobool (struct avm_memcell *m){return m->data.strVal[0] != 0;}
unsigned char bool_tobool (struct avm_memcell *m){return m->data.boolVal;}
unsigned char table_tobool (struct avm_memcell *m){return 1;}
unsigned char userfunc_tobool (struct avm_memcell *m){return 1;}
unsigned char libfunc_tobool (struct avm_memcell *m){return 1;}
unsigned char nil_tobool (struct avm_memcell *m){return 0;}
unsigned char undef_tobool (struct avm_memcell *m){assert(0); return(0);}


extern void execute_and(struct instruction *instruct ){}
extern void execute_or(struct instruction *instruct){}
extern void execute_not(struct instruction *instruct){}



void execute_cycle(void){
	if (executionFinished)
		return;
	else
	if (pc == AVM_ENDING_PC){
		executionFinished=1;
		return;
	}
	else{
		assert(pc<AVM_ENDING_PC);
		struct instruction* instr = code + pc;
		assert(instr->opcode >= 0);
		assert(instr->opcode <= AVM_MAX_INSTRUCTIONS);
		if (instr->srcLine)
			currLine = instr->srcLine;
		unsigned oldPC = pc;
		(*executeFuncs[instr->opcode])(instr);
		if (pc == oldPC)
			++pc;
	}
}



extern void memclear_string(struct avm_memcell* m){
	assert(m->data.strVal);
	//free(m->data.strVal);
}

extern void memclear_table(struct avm_memcell* m){
	assert(m->data.tableVal);
	avm_tabledecrefcounter(m->data.tableVal);
}

void execute_call(struct instruction* instr){

	struct avm_memcell* func = avm_translate_operand(&instr->result, &ax);
	assert(func);
	avm_callsaveenvironment();

	switch(func->type){
		
		case userfunc_m:{
			pc = func->data.funcVal;
			assert(pc < AVM_ENDING_PC);
			assert(code[pc].opcode == funcenter_v);
			break;
		}

		case string_m: avm_calllibfunc(func->data.strVal); break;
		case libfunc_m: avm_calllibfunc(func->data.libfuncVal); break;
		default:{
			char *s = avm_tostring(func);
			avm_error("%d: error: call: cannot bind '%s' to function!\n", instr->srcLine,s);
			free(s);
			executionFinished = 1;	
		}
	}
}

void execute_assign(struct instruction *instr){
	struct avm_memcell* lv = avm_translate_operand(&instr->result, (struct avm_memcell*)0);
	struct avm_memcell* rv	= avm_translate_operand(&instr->arg1, &ax);

	//assert(lv && (&stack[0] <= lv && &stack[top] > lv || lv == &retval));
	//assert(rv);

	avm_assign(lv, rv);
}

void avm_assign(struct avm_memcell* lv, struct avm_memcell* rv){
	if (lv == rv)
		return;
	
	if (lv->type == table_m &&
			rv->type == table_m &&
			lv->data.tableVal == rv->data.tableVal)
		return;
	if (rv->type == undef_m)
		avm_warning("assigning from 'undef' content!\n");

	avm_memcellclear(lv);
	memcpy(lv, rv, sizeof(struct avm_memcell));

	if (lv->type == string_m)
		lv->data.strVal == strdup(rv->data.strVal);
	else
	if (lv->type == table_m)
		avm_tableincrefcounter(lv->data.tableVal);
}


double consts_getnumber(unsigned index){
	return numConsts[index]; 
}
char * consts_getstring(unsigned index){
	return strConsts[index];
}
char * libfuncs_getused(unsigned index){
	return libF[index];
}

struct avm_memcell* avm_translate_operand(struct vmarg* arg, struct avm_memcell* reg){
	
	switch (arg->type){
		/*Variables*/
		case global_a: return stack[AVM_STACKSIZE-1-(arg->val)]; 
		case local_a: return stack[topsp-(arg->val)];
		case formal_a: return stack[topsp+AVM_STACKENV_SIZE+1+(arg->val)];
		case retval_a: return &retval;
		
		case number_a:{
			reg->type = number_m;
				reg->data.numVal = consts_getnumber(arg->val);
			return reg;
		}

		case string_a:{
			reg->type = string_m;
			reg->data.strVal = consts_getstring(arg->val);
			return reg;	
		}

		case bool_a:{
			reg->type = bool_m;
			reg->data.boolVal = arg->val;
			return reg;		    
		}

		case nil_a: reg->type = nil_m; return reg;

		case userfunc_a:{
			reg->type = userfunc_m;
			reg->data.funcVal = userF[arg->val]->address;
			return reg;		
		}

		case libfunc_a:{
			reg->type = libfunc_m;
			reg->data.libfuncVal = libfuncs_getused(arg->val);
			return reg;	       
		}

		default: assert(0);
	}
}


/*Calling Functions*/


extern struct userfunc* avm_getfuncinfo(unsigned address){
	int i;

	for(i=0;i<totalUser;i++){
		if(userF[i]->address == address){
			return userF[i];
		}
	}
	assert(0);
}

void avm_dec_top(void){
	if(!top){/*stack overflow*/
		avm_error("stack overflow\n");
		executionFinished = 1;
	}
	else
		--top;
}

void avm_push_envvalue(unsigned val){
	stack[top]->type = number_m;
	stack[top]->data.numVal = val;
	avm_dec_top();
}

void avm_callsaveenvironment(void){
	avm_push_envvalue(totalActuals);
	avm_push_envvalue(pc+1);
	avm_push_envvalue(top + totalActuals +2);
	avm_push_envvalue(topsp);
}

void execute_funcenter(struct instruction *instr){
	struct userfunc* funcinfo;
	struct avm_memcell *func = avm_translate_operand(&instr->result, &ax);
	assert(func);
	assert(pc == func->data.funcVal);/*Func address should match PC*/

	/*Callee actions here*/
	totalActuals = 0;
	funcinfo = avm_getfuncinfo(pc);
	topsp = top;
	top = top - funcinfo->localSize;

}

unsigned avm_get_envvalue (unsigned i){
	assert(stack[i]->type == number_m);
	unsigned val = (unsigned) stack[i]->data.numVal;
	assert(stack[i]->data.numVal == ((double) val) );
	return val;
}

void execute_funcexit (struct instruction* unused){
	unsigned oldTop = top;

	/*Epanafora proigoumenou periballontos kai epistrofi apo tin klisi*/
	top = avm_get_envvalue(topsp + AVM_SAVEDTOP_OFFSET);
	pc  = avm_get_envvalue(topsp + AVM_SAVEDPC_OFFSET);
	topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);

	/*ka8arismos tou activation record(garbage collection)*/
	while(oldTop++ <= top-1){/*intentionally ignoring first*/
		avm_memcellclear(stack[oldTop]);
	}
}


library_func_t avm_getlibraryfunc (char* id){
	int i;
	for(i=0;i<13;i++){
		if(strcmp(id,map[i]->id)==0){
			return map[i]->addr;
		}
	}
	assert(0);
}/*Typical hashing*/

void avm_calllibfunc (char* id){
	library_func_t f = avm_getlibraryfunc(id);
	
	if(!f){
		avm_error("%d: error: unsupported lib func '%s' called!\n", instr->srcLine,id);
		executionFinished = 1;
	}
	else{
	/*function and exit function are called manually*/

		topsp = top;
		totalActuals = 0;
		(*f)();/*call library function*/
	
		if(!executionFinished){/*an error may naturally occur inside*/
			execute_funcexit((struct instruction *) 0);/*return sequence*/
		}
	}
}

unsigned avm_totalactuals (void){
	return avm_get_envvalue(topsp + AVM_NUMACTUALS_OFFSET);
}

struct avm_memcell* avm_getactual (unsigned i){
	assert(i < avm_totalactuals());
	return stack[topsp + AVM_STACKENV_SIZE + 1 +i];
}

/*Implementation of the library function "print" 
  it displays every argument at the console
*/
void libraryfunc_print (void){
	/*unsigned n = avm_totalactuals();
	unsigned i;
	
	
	for(i=0; i<n; ++i){
		char *s = avm_tostring(avm_getactual(i));
		printf("%s",s);
		//free(s);
	}

	retval.type = number_m;
	retval.data.numVal = 0;
	
	printf("\n");*/
	unsigned i;
	char *s;
	struct avm_memcell * take;
	unsigned n = avm_totalactuals();
	
	for(i =0; i<n ; ++i){
		take=avm_getactual(i);
		if(take->type != number_m){
			s=avm_tostring(take);
			printf(s);
			//printf("\n");
		}
		else{
			printf("%lf",take->data.numVal);
			//printf("\n");
		}

		retval.type = number_m;
		retval.data.numVal = 0;
		
	//	free(s);
	}
	printf("\n");
}

void libraryfunc_cos (void){
	//unsigned p_topsp = avm_get_envvalue(topsp+AVM_SAVEDTOPSP_OFFSET);
	unsigned n = avm_totalactuals();
	struct avm_memcell *res;
	avm_memcellclear(&retval);
	
	if(n!=1){
		avm_error("Too many arguments in function cos\n");
	}
	else{
		res = avm_getactual(0);
		retval.type = number_m;
		retval.data.numVal = cos(res->data.numVal);
		//retval.data.numVal = cos(avm_get_envvalue(p_topsp+AVM_NUMACTUALS_OFFSET));
	}
}

void libraryfunc_sin (void){
	//unsigned p_topsp = avm_get_envvalue(topsp+AVM_SAVEDTOPSP_OFFSET);
	unsigned n = avm_totalactuals();
	struct avm_memcell *res;
	avm_memcellclear(&retval);
	
	if(n!=1){
		avm_error("Too many arguments in function cos\n");
	}
	else{
		res = avm_getactual(0);
		retval.type = number_m;
		retval.data.numVal = sin(res->data.numVal);
		//retval.data.numVal = cos(avm_get_envvalue(p_topsp+AVM_NUMACTUALS_OFFSET));
	}
}

void libraryfunc_strtonum(void){
	unsigned n = avm_totalactuals();
	struct avm_memcell *res;
	avm_memcellclear(&retval);
	
	if(n!=1){
		avm_error("Too many arguments in function cos\n");
	}
	else{
		res = avm_getactual(0);
		char *str = (char *)malloc(sizeof(char)*strlen(res->data.strVal)-1);
		int i;
		for(i=1; i<strlen(res->data.strVal)-1; i++){
			str[i-1]=res->data.strVal[i];
		}
		str[i-1] = '\0';
		retval.type = number_m;
		retval.data.numVal = (double)atof(str);
		//retval.data.numVal = cos(avm_get_envvalue(p_topsp+AVM_NUMACTUALS_OFFSET));
	}
}

/*with the following every library function is manually
  added in the VM library function resolution map
*/
void avm_registerlibfunc(char *id,  library_func_t addr){
	static int i=0;
	map[i]=(struct library_map *)malloc(sizeof(struct library_map));
	map[i]->id = strdup(id);
	map[i]->addr = addr;
	i++;
}

void execute_pusharg (struct instruction *instr){
	
	struct avm_memcell* arg = avm_translate_operand(&instr->arg1, &ax);
	assert(arg);

	/*This is actually stack[top] = arg but we have to use avm_assign*/
	avm_assign(stack[top], arg);
	++totalActuals;
	avm_dec_top();
}




char *avm_tostring(struct avm_memcell *m){
	assert(m->type >=0 && m->type <=undef_m);
	return(*tostringFuncs[m->type])(m);
}


/*ARITHMETIC OPERATIONS*/




double add_impl ( double x, double y ) { return x+y; }
double sub_impl ( double x, double y ) { return x-y; }
double mul_impl ( double x, double y ) { return x*y; }
double div_impl ( double x, double y ) { return x/y; /*na balo error cheack gia y!=0(runtime error)*/}
double mod_impl ( double x, double y ) { 
	return ((unsigned) x) % ((unsigned)y ); /*na balo error cheack gia y!=0(runtime error)*/
}


void execute_arithmetic (struct instruction *instr){
	
	struct avm_memcell* lv = avm_translate_operand(&instr->result, (struct avm_memcell*) 0);
	struct avm_memcell* rv1 = avm_translate_operand(&instr->arg1,  &ax);
	struct avm_memcell* rv2 = avm_translate_operand(&instr->arg2,  &bx);

	//assert(lv && (&stack[0] <= lv && stack[top] > lv || lv == retval));
	assert(rv1 && rv2);

	if(rv1->type != number_m || rv2->type != number_m){
		avm_error("%d: error: not a number in arithmetic!\n", instr->srcLine);
		executionFinished = 1;
	}	
	else{
		arithmetic_func_t op = arithmeticFuncs[instr->opcode - add_v];
		avm_memcellclear(lv);
		lv->type = number_m;
		lv->data.numVal = (*op)(rv1->data.numVal, rv2->data.numVal);
	}

}


/*EQUALITY OPERATIONS*/


unsigned char avm_tobool(struct avm_memcell* m){
	assert(m->type >= 0 && m->type < undef_m);
	return(*toboolFuncs[m->type])(m);
}

void execute_uminus (struct instruction* instr){
	return;
}

void execute_nop(struct instruction *instrs){
	return;
}

void execute_jump (struct instruction *instrs){
	pc=instrs->result.val;
}

void execute_jlt(struct instruction *instrs){
	assert(instrs->result.type == label_a);

	struct avm_memcell *rv1 = avm_translate_operand(&instrs->arg1, &ax);
	struct avm_memcell *rv2 = avm_translate_operand(&instrs->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv1->type == undef_m){
		avm_error("'undef' involved in equality\n");
	}
	else if(rv1->type == nil_m || rv1->type == nil_m){
		result = rv1->type == nil_m && rv2->type == nil_m;
	}
	else if(rv1->type == bool_m || rv2->type == bool_m){
		result = (avm_tobool(rv1) < avm_tobool(rv2));
	}
	else if(rv1->type != rv2->type){
		avm_error("%s=%s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
	}
	else{
		if(rv1->type == string_m || rv2->type == string_m){
			result = (strcmp(avm_tostring(rv1),avm_tostring(rv2)) < 0);
		}
		else if(rv1 -> type == number_m || rv2 -> type == number_m){
			result =( rv1->data.numVal < rv2->data.numVal);
		}
	}

	if(/*!executionFinished && */result){
		pc = instrs->result.val;
	}
	
}

void execute_jne(struct instruction *instrs){
	assert(instrs->result.type == label_a);

	struct avm_memcell *rv1 = avm_translate_operand(&instrs->arg1, &ax);
	struct avm_memcell *rv2 = avm_translate_operand(&instrs->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv1->type == undef_m){
		avm_error("'undef' involved in equality\n");
	}
	else if(rv1->type == nil_m || rv1->type == nil_m){
		result = rv1->type == nil_m && rv2->type == nil_m;
	}
	else if(rv1->type == bool_m || rv2->type == bool_m){
		result = (avm_tobool(rv1) != avm_tobool(rv2));
	}
	else if(rv1->type != rv2->type){
		avm_error("%s=%s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
	}
	else{
		if(rv1->type == string_m || rv2->type == string_m){
			result = (strcmp(avm_tostring(rv1),avm_tostring(rv2)) != 0);
		}
		else if(rv1 -> type == number_m || rv2 -> type == number_m){
			result =( rv1->data.numVal != rv2->data.numVal);
		}
	}

	if(/*!executionFinished && */result){
		pc = instrs->result.val;
	}
	
}

void execute_jle(struct instruction *instrs){
	assert(instrs->result.type == label_a);

	struct avm_memcell *rv1 = avm_translate_operand(&instrs->arg1, &ax);
	struct avm_memcell *rv2 = avm_translate_operand(&instrs->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv1->type == undef_m){
		avm_error("'undef' involved in equality\n");
	}
	else if(rv1->type == nil_m || rv1->type == nil_m){
		result = rv1->type == nil_m && rv2->type == nil_m;
	}
	else if(rv1->type == bool_m || rv2->type == bool_m){
		result = (avm_tobool(rv1) <= avm_tobool(rv2));
	}
	else if(rv1->type != rv2->type){
		avm_error("%s=%s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
	}
	else{
		if(rv1->type == string_m || rv2->type == string_m){
			result = (strcmp(avm_tostring(rv1),avm_tostring(rv2)) <= 0);
		}
		else if(rv1 -> type == number_m || rv2 -> type == number_m){
			result =( rv1->data.numVal <= rv2->data.numVal);
		}
	}

	if(/*!executionFinished && */result){
		pc = instrs->result.val;
	}
	
}

void execute_jge(struct instruction *instrs){
	assert(instrs->result.type == label_a);

	struct avm_memcell *rv1 = avm_translate_operand(&instrs->arg1, &ax);
	struct avm_memcell *rv2 = avm_translate_operand(&instrs->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv1->type == undef_m){
		avm_error("'undef' involved in equality\n");
	}
	else if(rv1->type == nil_m || rv1->type == nil_m){
		result = rv1->type == nil_m && rv2->type == nil_m;
	}
	else if(rv1->type == bool_m || rv2->type == bool_m){
		result = (avm_tobool(rv1) >= avm_tobool(rv2));
	}
	else if(rv1->type != rv2->type){
		avm_error("%s=%s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
	}
	else{
		if(rv1->type == string_m || rv2->type == string_m){
			result = (strcmp(avm_tostring(rv1),avm_tostring(rv2)) >= 0);
		}
		else if(rv1 -> type == number_m || rv2 -> type == number_m){
			result =( rv1->data.numVal >= rv2->data.numVal);
		}
	}

	if(/*!executionFinished && */result){
		pc = instrs->result.val;
	}
	
}

void execute_jgt(struct instruction *instrs){
	assert(instrs->result.type == label_a);

	struct avm_memcell *rv1 = avm_translate_operand(&instrs->arg1, &ax);
	struct avm_memcell *rv2 = avm_translate_operand(&instrs->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv1->type == undef_m){
		avm_error("'undef' involved in equality\n");
	}
	else if(rv1->type == nil_m || rv1->type == nil_m){
		result = rv1->type == nil_m && rv2->type == nil_m;
	}
	else if(rv1->type == bool_m || rv2->type == bool_m){
		result = (avm_tobool(rv1) > avm_tobool(rv2));
	}
	else if(rv1->type != rv2->type){
		avm_error("%s=%s is illegal!\n", typeStrings[rv1->type],typeStrings[rv2->type]);
	}
	else{
		if(rv1->type == string_m || rv2->type == string_m){
			result = (strcmp(avm_tostring(rv1),avm_tostring(rv2)) > 0);
		}
		else if(rv1 -> type == number_m || rv2 -> type == number_m){
			result =( rv1->data.numVal > rv2->data.numVal);
		}
	}

	if(/*!executionFinished && */result){
		pc = instrs->result.val;
	}
	
}

void execute_jeq(struct instruction *instrs){
	assert(instrs->result.type == label_a);

	struct avm_memcell *rv1 = avm_translate_operand(&instrs->arg1, &ax);
	struct avm_memcell *rv2 = avm_translate_operand(&instrs->arg2, &bx);

	unsigned char result = 0;

	if(rv1->type == undef_m || rv1->type == undef_m){
		avm_error("'undef' involved in equality\n");
	}
	else if(rv1->type == nil_m || rv1->type == nil_m){
		result = rv1->type == nil_m && rv2->type == nil_m;
	}
	else if(rv1->type == bool_m || rv2->type == bool_m){
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	}
	else if(rv1->type != rv2->type){
		avm_error("%s=%s is illegal!\n",typeStrings[rv1->type],typeStrings[rv2->type]);
		executionFinished = 1;
	}
	else{
		if(rv1->type == string_m || rv2->type == string_m){
			result = (strcmp(avm_tostring(rv1),avm_tostring(rv2)) == 0);
		}
		else if(rv1 -> type == number_m || rv2 -> type == number_m){
			result =( rv1->data.numVal == rv2->data.numVal);
		}
	}

	if(/*!executionFinished && */result){
		pc = instrs->result.val;
	}
	
}

void libfunc_typeof( void ){
	unsigned n = avm_totalactuals();

	if( n!= 1){
		avm_error("one (or more) argument (not %d) expected in 'typeof'!\n" ,instr->srcLine,n);
	}
	else{
		avm_memcellclear(&retval);
		retval.type = string_m;
		retval.data.strVal = strdup(typeStrings[avm_getactual(0)->type]);
	}
}

void execute_newtable(struct instruction *instr){

	struct avm_memcell* lv = avm_translate_operand(&instr->result,(struct avm_memcell*) 0);
	//assert(lv && (stack[0] <= lv && stack[top] > lv || lv==retval ));
	
	avm_memcellclear(lv);

	lv->type = table_m;
	lv->data.tableVal = avm_tablenew();
	avm_tableincrefcounter(lv->data.tableVal);
}

unsigned int my_hash(double x){
	return ((int)((int)x%AVM_TABLE_HASHSIZE));
}

struct avm_memcell *avm_tablegetelem(struct avm_table *table, struct avm_memcell *index){

	int i;
	struct avm_memcell * content;
	struct avm_table_bucket *tmp;

	if(index->type == string_m){
		
		i = hash(index->data.strVal, AVM_TABLE_HASHSIZE);
		tmp = table->strIndexed[i];
		while(tmp != NULL){
			if(strcmp(tmp->key->data.strVal,index->data.strVal) == 0){
				content = tmp->value;
				return content;
			}
			tmp=tmp->next;
		}
		return NULL;
	}
	else if(index->type == number_m){
	
		i=my_hash((int)index->data.numVal);
		//index->data.strVal = strdup(number_tostring(index));
		
		tmp = table->numIndexed[i];
		while(tmp != NULL){
			if(tmp->key->data.numVal==(int)index->data.numVal){
				content = tmp->value;
				return content;
			}
			tmp=tmp->next;
		}
		return NULL;
		
	}
	avm_error("not a number or string as a table index\n");
	
}

void avm_tablesetelem(struct avm_table *table, struct avm_memcell *index, struct avm_memcell *content){
	assert(table);
	assert(index);
	assert(content);
	
	unsigned i;
	struct avm_table_bucket *tmp1,*tmp2;
	
	struct avm_table_bucket * tmp = (struct avm_table_bucket *) malloc(sizeof(struct avm_table_bucket));
	tmp->value = (struct avm_memcell *)malloc(sizeof(struct avm_memcell)); 
	tmp->key = (struct avm_memcell *)malloc(sizeof(struct avm_memcell)); 
	
	
	if(index->type == string_m){
		i = hash(index->data.strVal, AVM_TABLE_HASHSIZE);
		
		tmp1 = table->strIndexed[i];
		if(tmp1 == NULL){
			memcpy(tmp->key, index, sizeof(struct avm_memcell));//tmp->key = index;
			
			memcpy(tmp->value, content, sizeof(struct avm_memcell));//tmp->value = content;
			tmp->next=NULL;
			table->strIndexed[i] = tmp;
		}else{
			while(tmp1!=NULL){
				if(strcmp(tmp1->key->data.strVal, index->data.strVal) == 0){
					memcpy(tmp1->value, content, sizeof(struct avm_memcell));//tmp->value = content;
					return;
				}
				tmp1 = tmp1->next;		
			}
			memcpy(tmp->key, index, sizeof(struct avm_memcell));//tmp->key = index;
			memcpy(tmp->value, content, sizeof(struct avm_memcell));//tmp->value = content;
			tmp->next=NULL;
			
			tmp1->next = tmp;
			
		}
	
	}
	else if(index->type == number_m){
		i=my_hash((int)index->data.numVal);
		//tmp->key->data.strVal = strdup(number_tostring(index));
		//index->data.strVal = strdup(number_tostring(index));
		
		
		tmp1 = table->numIndexed[i];
		if(tmp1 == NULL){
			memcpy(tmp->key, index, sizeof(struct avm_memcell));//tmp->key = index;
			
			memcpy(tmp->value, content, sizeof(struct avm_memcell));//tmp->value = content;
			tmp->next=NULL;
			table->numIndexed[i] = tmp;
		}else{
			while(tmp1!=NULL){
				if(tmp1->key->data.numVal == (int)index->data.numVal){
					memcpy(tmp1->value, content, sizeof(struct avm_memcell));//tmp->value = content;
					return;
				}
				tmp2=tmp1;
				tmp1 = tmp1->next;		
			}
			memcpy(tmp->key, index, sizeof(struct avm_memcell));//tmp->key = index;
			memcpy(tmp->value, content, sizeof(struct avm_memcell));//tmp->value = content;
			tmp->next=NULL;
			
			tmp2->next = tmp;
			
		}
	}
}

void execute_tablegetelem(struct instruction* instr){
	
	struct avm_memcell* lv =  avm_translate_operand(&instr->result,(struct avm_memcell*)0);
	struct avm_memcell* t  =  avm_translate_operand(&instr->arg1,(struct avm_memcell*)0);
	struct avm_memcell* i  =  avm_translate_operand(&instr->arg2, &ax);
	struct avm_memcell* content ;
	
	//assert(lv && (stack[0] <= lv && stack[top]>lv || lv==retval));
	//assert(t && stack[0] <= t && stack[top] > t);
	assert(i);

	avm_memcellclear(lv);
	lv->type = nil_m;

	if(t->type != table_m){
		avm_error("illegal use of type %s as table!\n",instr->srcLine,typeStrings[t->type]);
	}
	else{
		content = avm_tablegetelem(t->data.tableVal,i);
		
		if(content)
			avm_assign(lv,content);
		else{
			char *ts = avm_tostring(t);
			char *is = avm_tostring(i);
			avm_warning("table element not found!\n");
			free(ts);
			free(is);
		}
	}
}

void execute_tablesetelem(struct instruction * instr){
	struct	avm_memcell *t = avm_translate_operand(&instr->result,(struct avm_memcell *)0);
	struct  avm_memcell *i = avm_translate_operand(&instr->arg1,&ax);
	struct avm_memcell *c = avm_translate_operand(&instr->arg2, &bx);

	//assert(t && (stack[0] <= t) && (stack[top]>t) );
	assert(i && c);

	if(t->type != table_m )
		avm_error("illegal use of type %s as table!\n",instr->srcLine,typeStrings[t->type]);
	else
		avm_tablesetelem(t->data.tableVal,i,c);

}

void avm_initstack(void){
	unsigned int i;
	
	for ( i=0; i < AVM_STACKSIZE; ++i ){
		AVM_WIPEOUT(stack[i]);
		stack[i] = (struct avm_memcell *)malloc(sizeof(struct avm_memcell));
		stack[i]->type = undef_m;
	}

	for( i=AVM_STACKSIZE-1 ; i > AVM_STACKSIZE - 1 - totalVars; i--){
		top -- ;
	}
}

void avm_initialize (FILE *bfile){

	avm_initcodentables(bfile);
	
	avm_initstack();
	
	avm_registerlibfunc("print",libraryfunc_print);
	avm_registerlibfunc("typeof",libfunc_typeof);
	avm_registerlibfunc("totalarguments",libfunc_totalarguments);
	avm_registerlibfunc("cos",libraryfunc_cos);
	avm_registerlibfunc("sin",libraryfunc_sin);
	avm_registerlibfunc("strtonum", libraryfunc_strtonum);
}

void libfunc_totalarguments(void){
	unsigned p_topsp = avm_get_envvalue(topsp+AVM_SAVEDTOPSP_OFFSET);
	avm_memcellclear(&retval);

	if(!p_topsp){
		avm_error("'totalarguments' called outside a function\n", instr->srcLine);
		retval.type = nil_m;
	}
	else{
		retval.type = number_m;
		retval.data.numVal = avm_get_envvalue(p_topsp+AVM_NUMACTUALS_OFFSET);
	}
}

int main(int argc, char *argv[])
{
	FILE *bfile = fopen(argv[1], "rb");
	
	if(bfile == NULL){
		printf("avm:no input files\n");
		exit(1);
	}
	
	//ax = (struct avm_memcell *)malloc(sizeof(struct avm_memcell));
	//bx = (struct avm_memcell *)malloc(sizeof(struct avm_memcell));
	//cx = (struct avm_memcell *)malloc(sizeof(struct avm_memcell));
	//retval = (struct avm_memcell *)malloc(sizeof(struct avm_memcell));

	avm_initialize(bfile);

	while(!executionFinished){
		execute_cycle();
	}


	return 0;
}

