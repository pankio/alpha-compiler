#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"tarCode.h"
#include"binary.h"



int totalVars;
int totalInstr;
int totalUser;
int totalLib;
int totalStr;
int totalNum;


void write_binary(double *numConsts, char **stringConsts, struct userfunc **userFuncs, char **nameLibFuncs, struct instruction *instr, unsigned totalNumConsts, unsigned totalStringConsts, unsigned totalNamedLibfuncs, unsigned totalUserFuncs, int num_progVars, int curr_incode){

	FILE *bfile = fopen("exe.abc", "wb+");
	int magic = 340200702;

	fwrite(&magic, sizeof(int), 1, bfile);
	fwrite(&num_progVars, sizeof(int), 1, bfile);
	fwrite(&curr_incode, sizeof(int), 1, bfile);
	write_userfuncs(bfile, userFuncs, totalUserFuncs);
	write_libfuncs(bfile, nameLibFuncs, totalNamedLibfuncs);
	write_strings(bfile, stringConsts, totalStringConsts);
	write_numbers(bfile, numConsts, totalNumConsts);
	write_instructions(bfile, instr, curr_incode);
	fclose(bfile);
	
}


void write_userfuncs(FILE *bfile, struct userfunc **userFuncs, unsigned totalUserFuncs){
	int i, len;

	fwrite(&totalUserFuncs, sizeof(int), 1, bfile);
	for(i=0;i<totalUserFuncs;i++){
		len = strlen(userFuncs[i]->id)+1;
		fwrite(&len, sizeof(int), 1, bfile);
		fwrite(userFuncs[i]->id, sizeof(char), len, bfile);
		fwrite(&userFuncs[i]->localSize, sizeof(unsigned), 1, bfile);
		fwrite(&userFuncs[i]->address, sizeof(unsigned), 1, bfile);
	}
}

void write_libfuncs(FILE *bfile, char **nameLibFuncs, unsigned totalNamedLibfuncs){
	int i, len;

	fwrite(&totalNamedLibfuncs, sizeof(int), 1, bfile);
	for(i=0;i<totalNamedLibfuncs;i++){
		len = strlen(nameLibFuncs[i])+1;
		fwrite(&len, sizeof(int), 1, bfile);
		fwrite(nameLibFuncs[i], sizeof(char), len, bfile);
	}
}

void write_strings(FILE *bfile, char **stringConsts, unsigned totalStringConsts){
	int i, len;

	fwrite(&totalStringConsts, sizeof(int), 1, bfile);
	for(i=0;i<totalStringConsts;i++){
		len = strlen(stringConsts[i])+1;
		fwrite(&len, sizeof(int), 1, bfile);
		fwrite(stringConsts[i], sizeof(char), len, bfile);
	}
}

void write_numbers(FILE *bfile, double *numConsts, unsigned totalNumConsts){
	int i;
	
	fwrite(&totalNumConsts,sizeof(int), 1, bfile);
	for(i=0;i<totalNumConsts;i++){
		fwrite(&numConsts[i],sizeof(double), 1, bfile);
	}
}

void write_instructions(FILE *bfile, struct instruction *instr, int curr_incode){
	int i;
	
	for(i = 0; i < curr_incode; i++){
		fwrite(&instr[i], sizeof(struct instruction), 1, bfile);
	}
}



void read_binary(){
	int magic;
	FILE *bfile = fopen("exe.abc", "rb");
	
	fread(&magic, sizeof(int), 1, bfile);
	if(magic != 340200702){
		printf("Error:Not correct binary file\n");
		fclose(bfile);
		exit(1);
	}
	fread(&totalVars, sizeof(int), 1, bfile);
	fread(&totalInstr, sizeof(int), 1, bfile);
	
	read_userfuncs(bfile);
	read_libfuncs(bfile);
	read_strings(bfile);
	read_numbers(bfile);
	read_instructions(bfile);
	fclose(bfile);
}


void read_userfuncs(FILE *bfile){
	int i, len;

	fread(&totalUser, sizeof(int), 1, bfile);
	userFunc = (struct userfunc **)malloc(sizeof(struct userfunc *)*totalUser);
	for(i=0;i<totalUser;i++){
		//len = strlen(userFunc[i].id)+1;
		userFunc[i] = (struct userfunc *)malloc(sizeof(struct userfunc ));
		fread(&len, sizeof(int), 1, bfile);
		userFunc[i]->id = (char *)malloc(sizeof(char)*len);
		fread(userFunc[i]->id, sizeof(char), len, bfile);
		fread(&userFunc[i]->localSize, sizeof(unsigned), 1, bfile);
		fread(&userFunc[i]->address, sizeof(unsigned), 1, bfile);
	}	
}

void read_libfuncs(FILE *bfile){
	int i, len;

	fread(&totalLib, sizeof(int), 1, bfile);
	nameLibFunc = (char **)malloc(sizeof(char *)*totalLib);
	for(i=0;i<totalLib;i++){
		fread(&len, sizeof(int), 1, bfile);
		nameLibFunc[i] = (char *)malloc(sizeof(char)*len);
		fread(nameLibFunc[i], sizeof(char), len, bfile);
	}
}

void read_strings(FILE *bfile){
	int i, len;

	fread(&totalStr, sizeof(int), 1, bfile);
	stringConst = (char **)malloc(sizeof(char *)*totalStr);
	for(i=0;i<totalStr;i++){
		fread(&len, sizeof(int), 1, bfile);
		stringConst[i] = (char *)malloc(sizeof(char)*len);
		fread(stringConst[i], sizeof(char), len, bfile);
	}
}

void read_numbers(FILE *bfile){
	int i;
	double *nn = (double *) malloc(sizeof(double));
	fread(&totalNum, sizeof(int), 1, bfile);
	numConst = (double *)malloc(sizeof(double)*totalNum);
	
	for(i=0;i<totalNum;i++){
		fread(nn, sizeof(double), 1, bfile);
		numConst[i] = *nn;
	}

}

void read_instructions(FILE *bfile){
	int i;
	
	instrs = (struct instruction *)malloc(sizeof(struct instruction)*totalInstr);
	for(i=0;i<totalInstr;i++){
		//instrs[i] = (struct instruction *)malloc(sizeof(struct instruction ));
		fread(&instrs[i], sizeof(struct instruction), 1, bfile);
	}
}
