#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include"tarCode.h"
#include"inCode.h"
#include"symboltable.h"


int MAXEXPAND = 1000;
int INTEXPAND = 1000;

extern struct quad * quads;
extern unsigned int total;
extern int yylineno;
extern int Currquad;


unsigned totalNumConsts = 0;
unsigned totalStringConsts = 0;
unsigned totalNamedLibfuncs = 0;
unsigned totalUserFuncs = 0;

unsigned consts_newstring (char * s);
unsigned consts_newnumber (double n);
unsigned libfuncs_newused (char * s);
unsigned userfuncs_newfunc(char *, unsigned int , unsigned);


int expand_str = 0;
int expand_num = 0;
int expand_lib = 0;
int curr_incode=0;
int quadcnt=0;

struct incomplete_jump{

	unsigned instrNo; 
	unsigned iaddress;
	struct incomplete_jump * next;

};


struct return_jumps{

	struct return_jumps *next;
	struct return_jumps *innerreturn; 
	unsigned int label;
	unsigned int instr_number;

}*returnlist;






struct incomplete_jump * ij_head = (struct incomplete_jump *) 0;
unsigned ij_total = 0;


void (*generators[])(struct quad *) = {
    
	generate_ASSIGN, 
	generate_ADD ,
	generate_SUB ,
	generate_MUL ,
	generate_DIV ,
	generate_MOD ,
	generate_IF_EQ ,
	generate_IF_NOTEQ ,
	generate_IF_LESSEQ ,
	generate_IF_GREATEREQ ,
	generate_IF_LESS,
	generate_IF_GREATER ,
	generate_JUMP ,
	generate_CALL,
   	generate_PARAM,
	generate_FUNCSTART,
	generate_FUNCEND,
	generate_NEWTABLE ,	
	generate_TABLEGETELEM ,	
	generate_TABLESETELEM ,
	generate_NOP,
	generate_UMINUS,
	generate_AND ,
    	generate_OR  ,
	generate_NOT ,
	generate_GETRETVAL,
	generate_RETURN
};

char * opcode_names[] = {"assign"	 , "add" 	 	  , "sub" 	  ,
						 "mul"       , "div" 	 	  , "mod" 	  ,
						  "jeq"  	 	  , "jne" 	  ,
						 "jle"       , "jge" 	 	  , "jlt" 	  ,
						 "jgt"       , "jump"    	  , "call"	  ,
						 "pusharg"   , "funcenter",
						 "funcexit"	 , "newtable"	  ,	
						 "tablegetelem" , "tablesetelem","nop",
						 "uminus"    , "and" 	 	  , "or"  	  ,
   						 "not"       , "getretval"    
};

char * type_names[] = {  
	"label"  ,
	"global"  ,
	"formal",
	"local",
	"number" ,
	"string",
	"bool",
	"nil",
	"userfunc",
	"libfunc",
	"retval" 
};

char * booleanexpr[2] = {"false", "true"};



void expand_array ( void * a , int who ){
	
	
	
	if(who == 0){
		MAXEXPAND +=1000;
		stringConsts = (char **)realloc(stringConsts, sizeof(char*)*MAXEXPAND);
	}
	if(who == 1){
		INTEXPAND +=1000;
		numConsts = (double *)realloc(numConsts, sizeof(double)*INTEXPAND);
	}
}	


unsigned consts_newstring (char * s){
	if(totalStringConsts == 0 ){
		stringConsts = (char **) malloc(MAXEXPAND*(sizeof(char *)));
	}
	if( totalStringConsts == MAXEXPAND-2){
		expand_array((void *)stringConsts, 0);
	}
	stringConsts[totalStringConsts] = strdup(s);
	totalStringConsts++;
	
	return totalStringConsts - 1;
}

unsigned consts_newnumber (double n){
	if(totalNumConsts == 0 ){
		numConsts = (double *) malloc(INTEXPAND*(sizeof(double)));
	}
	if( totalNumConsts == INTEXPAND-2){
		expand_array((void *)numConsts, 1);
	}
	numConsts[totalNumConsts] = n;
	totalNumConsts++;

	return totalNumConsts - 1;
	
}

unsigned libfuncs_newused (char * s){
	
	int i;
	if((i=find_name(s, 0)) != -1){
		return i;	
	}
	nameLibFuncs[totalNamedLibfuncs] = strdup(s);
	totalNamedLibfuncs++;

	return totalNamedLibfuncs - 1;
	
}

unsigned userfuncs_newfunc(char *id, unsigned int locSize, unsigned addr){
	
	int i;
	struct userfunc *newf = (struct userfunc *)malloc(sizeof(struct userfunc));
	
	if((i=find_name(id, 1)) != -1){
		return i;	
	}
	newf->address = addr;
	newf->id = strdup(id);
	newf->localSize = locSize;

	userFuncs[totalUserFuncs] = newf;
	totalUserFuncs++;
	
	return totalUserFuncs - 1;	
}

void make_operand(struct expr *e, struct vmarg *arg) {
	
	if(e==NULL)
		return;
	
	switch ( e->type ){


	case var_e:
	case tableitem_e:
	case boolexpr_e:
	case arithexpr_e:
	case assignexpr_e:
	case newtable_e:{
			assert(e->sym);
			arg->val = e->sym->u.v->offset;
			arg->sym= e->sym;
			if(e->sym->u.f->space == function){
				arg->type = userfunc_a;
				arg->val = userfuncs_newfunc(e->sym->u.f->name, e->sym->u.f->localsize, quads[quadcnt].taddress);
				break;
			}
			switch (e->sym->u.v->space){

					case programvar: arg->type = global_a;
							 break;
					case functionlocal:
							 arg->type = local_a;
							 break;
					case formalarg:
							 arg->type = formal_a;
							 break;

					default:
						    break;
					
				}
			break;	
	}
	case constbool_e:{
			arg->val  = e->boolConst;
			arg->type = bool_a;
			break;
	}
	case conststring_e:{
			arg->val = consts_newstring(e->strConst);
			arg->type = string_a;
			break;
	}
					 
	case intnum_e:{
 			arg->val = consts_newnumber(e->intConst);
			arg->type = number_a;
			break;
	}
	case floatnum_e:{
 
			arg->val = consts_newnumber(e->floatConst);
			arg->type = number_a;
			break;
	}	  
	case programfunc_e:{
			
			arg->type = userfunc_a;
			//arg->val = quad_array[quadCnt].taddress;
			arg->val = userfuncs_newfunc(e->sym->u.f->name, e->sym->u.f->localsize, quads[quadcnt].taddress);
			

			break;
	}
	case libraryfunc_e:{	
			arg->type = libfunc_a;
			arg->val = libfuncs_newused( e->sym->u.f->name );
			break;
	}
	case nil_e: 	arg->type = nil_a;
			break;
	default:
			break;
	}
}

void make_numberoperand (struct  vmarg * arg , double val ){

	arg->val = consts_newnumber(val);
	arg->type = number_a;

}

void make_booloperand ( struct vmarg * arg , unsigned val ){

	arg->val = val;
	arg->type = bool_a;

}

void make_retvaloperand ( struct vmarg * arg ){

	arg->type = retval_a;
}

void generate_ADD ( struct quad *new_quad ){


	generate( add_v , new_quad );

}

void generate_SUB ( struct quad *new_quad ){


	generate( sub_v , new_quad );

}
void generate_MUL ( struct quad *new_quad ){


	generate( mul_v , new_quad );

}
void generate_DIV ( struct quad *new_quad ){


	generate( divX_v , new_quad );

}
void generate_MOD ( struct quad *new_quad ){


	generate( mod_v , new_quad );

}

void generate_NEWTABLE ( struct quad *new_quad ){


	generate( newtable_v , new_quad );

}
void generate_TABLEGETELEM ( struct quad *new_quad ){


	generate( tablegetelem_v , new_quad );

}
void generate_TABLESETELEM ( struct quad *new_quad ){


	generate( tablesetelem_v , new_quad );

}

void generate_ASSIGN ( struct quad *new_quad ){

	generate( assign_v , new_quad );
	

}

void generate_UMINUS ( struct quad *new_quad ){


	generate( uminus_v , new_quad );

}

void generate_NOP (struct quad *new_quad){

	struct instruction t;
	t.opcode = nop_v;
	new_quad->taddress = nextinstructionlabel();
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	reset_operand(&t.result);
	
	tar_emit(t);

}


void generate_JUMP ( struct quad *new_quad ){


	generate_relational( jump_v , new_quad );
	
}

void generate_IF_EQ ( struct quad *new_quad ){


	generate_relational( jeq_v , new_quad );
	

}

void generate_IF_NOTEQ ( struct quad *new_quad ){


	generate_relational( jne_v , new_quad );
}

void generate_IF_GREATER ( struct quad *new_quad ){


	generate_relational( jgt_v , new_quad );
}

void generate_IF_GREATEREQ ( struct quad *new_quad ){


	generate_relational( jge_v , new_quad );

}

void generate_IF_LESS ( struct quad *new_quad ){


	generate_relational( jlt_v , new_quad );

}

void generate_IF_LESSEQ ( struct quad *new_quad ){


	generate_relational( jle_v , new_quad );

}

void generate(enum vmopcode op , struct quad *new_quad){
	struct instruction t;
	
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	reset_operand(&t.result);
	
	t.opcode = op;
	
	make_operand(new_quad->arg1,&t.arg1);
	make_operand(new_quad->arg2,&t.arg2);
	make_operand(new_quad->result , &t.result);
	new_quad->taddress = nextinstructionlabel();
	tar_emit(t);
}

void generate_all(){
	int i;
	for(i=0; i<Currquad; ++i){
		quadcnt = i;
		(*generators[quads[i].op])(quads+i);
	}
	patch_incomplete_jump();
}

void add_incomplete_jump(unsigned instrNo , unsigned iaddress){
	
	struct incomplete_jump * new_jump,*tmp;
 

 	new_jump = (struct incomplete_jump *)malloc(sizeof(struct incomplete_jump));
    
	if(new_jump==NULL){
		printf("Memory allocation error\n");
		exit(0);
	}
	else{
    	new_jump->instrNo=instrNo;
	new_jump->iaddress=iaddress;
	new_jump->next=NULL;
		
	}
  	if(ij_head==NULL){
	       	ij_head=new_jump;
					
 	}
	else{
   		new_jump->next = ij_head;
		ij_head = new_jump;
	}
}

void patch_incomplete_jump(void){
	struct incomplete_jump *tmp,*prev;
       	
	for(tmp=ij_head; tmp!=NULL; tmp=tmp->next){
		instr[tmp->instrNo].result.val=quads[tmp->iaddress].taddress;
	}
}

unsigned int nextinstructionlabel(void){
	return curr_incode;
}

void tar_emit(struct instruction t){
	t.srcLine = quads[quadcnt].line;
	instr[curr_incode++] = t;
}

void generate_GETRETVAL(struct quad *new_quad){
	new_quad->taddress = nextinstructionlabel();
	struct instruction t;
	t.opcode = assign_v;
	make_operand(new_quad->result, &t.result);
	make_retvaloperand(&t.arg1);
	tar_emit(t);
}

void generate_FUNCSTART(struct quad *new_quad){
	struct instruction t1;
	t1.opcode = jump_v;
	reset_operand(&t1.arg1);
	reset_operand(&t1.arg2);
	t1.result.type = label_a;
	returnlist = tar_insert(returnlist);
	returnlist = create_return(returnlist, nextinstructionlabel());
	tar_emit(t1);	
	
	
	struct symbol *f = new_quad->arg1->sym;
	
	new_quad->taddress = nextinstructionlabel(); 
	
	userfuncs_newfunc(f->u.f->name, f->u.f->localsize, new_quad->taddress);
	
	
	
	funcstack = push(funcstack, f);
	
	struct instruction t;
	t.opcode = funcenter_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	
	make_operand(new_quad->arg1, &t.result);
	tar_emit(t);
	
}

void generate_RETURN(struct quad *new_quad){
	struct instruction t;
	struct symbol *f;

	new_quad->taddress = nextinstructionlabel();
	t.opcode = assign_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	make_retvaloperand(&t.result);
	make_operand(new_quad->result, &t.arg1);
	
	tar_emit(t);
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	returnlist = create_return(returnlist, nextinstructionlabel());
	
	t.opcode = jump_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	tar_emit(t);	
	
}

void generate_FUNCEND(struct quad *new_quad){
	struct instruction t;
	struct symbol *f;
	
	//struct sta f=
	//backpatch(f.return,nextinstructionlabel()); 
	
	returnlist = append(returnlist,nextinstructionlabel());
	
	backpatch(returnlist);
	returnlist = tar_delete(returnlist);
	
	new_quad->taddress = nextinstructionlabel();
	
	t.opcode = funcexit_v;
	make_operand(new_quad->arg1, &t.result);
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	tar_emit(t);
}

void backpatch(struct return_jumps *head){
	struct return_jumps *tmp = head->innerreturn;
	int flag = 1;

	while(tmp!=NULL){
		if(flag==1){/*gia na kanei jump th synarthsh*/
			instr[tmp->instr_number].result.val = tmp->label+1;
			tmp= tmp->next;
			flag=0;
			continue;
		}
		instr[tmp->instr_number].result.val = tmp->label;
		tmp= tmp->next;
	}
}


void generate_PARAM(struct quad *new_quad){
	new_quad->taddress = nextinstructionlabel();
	struct instruction t;
	t.opcode = pusharg_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	reset_operand(&t.result);
	make_operand(new_quad->result, &t.arg1);
	tar_emit(t);
	
}

void generate_CALL(struct quad *new_quad){
	new_quad->taddress = nextinstructionlabel();
	struct instruction t;
	t.opcode = call_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	make_operand(new_quad->result, &t.result);
	//t.result.val=find_name(new_quad->result->sym->u.f->name);
	tar_emit(t);
}

void generate_relational(enum vmopcode op,struct quad *new_quad ){
	struct instruction t;
	t.opcode = op;

	if (t.opcode != jump_v ){
		make_operand(new_quad->arg1,&t.arg1);
		make_operand(new_quad->arg2,&t.arg2);
	}
	else{
		reset_operand(&t.arg1);
		reset_operand(&t.arg2);
	}
	
	t.result.type = label_a;

	new_quad->taddress = nextinstructionlabel();
		
	
	if(new_quad->label <= quadcnt){
	   	t.result.val=quads[new_quad->label].taddress;
	}
	else{
		add_incomplete_jump(nextinstructionlabel(),new_quad->label);
	}   
	//new_quad->taddress = nextinstructionlabel();
    	tar_emit(t);
}

void generate_AND(struct quad *new_quad){
	
	struct instruction t;

	new_quad->taddress = nextinstructionlabel();

	t.opcode = jeq_v;
	make_operand(new_quad->arg1,&t.arg1);
	make_booloperand(&t.arg2,0);
	
	t.result.type = label_a;

	t.result.val = nextinstructionlabel()+4;

	tar_emit(t);
	
	make_operand(new_quad->arg2,&t.arg1);
	t.result.val = nextinstructionlabel()+3;

	tar_emit(t);
	
	t.opcode = assign_v;
	make_booloperand(&t.arg1,1);
	reset_operand(&t.arg2);
	make_operand(new_quad->result,&t.result);

	tar_emit(t);

	t.opcode = jump_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	t.result.val = nextinstructionlabel()+2;
	
	tar_emit(t);

	t.opcode = assign_v;
	make_booloperand(&t.arg1,0);
	reset_operand(&t.arg2);
	make_operand(new_quad->result,&t.result);

	tar_emit(t);
	patch_incomplete_jump();	
}

void generate_OR(struct quad *new_quad){
	new_quad->taddress = nextinstructionlabel();
	struct instruction t;
	t.opcode = jeq_v;
	make_operand(new_quad->arg1, &t.arg1);
	make_booloperand(&t.arg2, 1);
	t.result.type = label_a;
	t.result.val = nextinstructionlabel()+4;

	tar_emit(t);
	
	make_operand(new_quad->arg2,&t.arg1);
	t.result.val = nextinstructionlabel()+3;

	tar_emit(t);
	
	t.opcode = assign_v;
	make_booloperand(&t.arg1,0);
	reset_operand(&t.arg2);
	make_operand(new_quad->result,&t.result);

	tar_emit(t);

	t.opcode = jump_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	t.result.val = nextinstructionlabel()+2;
	
	tar_emit(t);

	t.opcode = assign_v;
	make_booloperand(&t.arg1,1);
	reset_operand(&t.arg2);
	make_operand(new_quad->result,&t.result);

	tar_emit(t);
	patch_incomplete_jump();
}

void generate_NOT(struct quad *new_quad ){
	
	struct instruction t;
	
	new_quad->taddress = nextinstructionlabel();


	t.opcode = jeq_v;
	make_operand(new_quad->arg1,&t.arg1);
	make_booloperand(&t.arg2,0);
	t.result.type = label_a;
	t.result.val = nextinstructionlabel()+3;

	tar_emit(t);

	

	t.opcode = assign_v;
	make_booloperand(&t.arg1,0);
	reset_operand(&t.arg2);
	make_operand(new_quad->result,&t.result);

	tar_emit(t);

	t.opcode = jump_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	t.result.val = nextinstructionlabel()+2;
	
	tar_emit(t);
	
	t.opcode = assign_v;
	make_booloperand(&t.arg1,1);
	reset_operand(&t.arg2);
	make_operand(new_quad->result,&t.result);

	tar_emit(t);
}

void reset_operand ( struct vmarg * arg ){
    arg->type=-1;	
}

struct stack * push(struct stack *head, struct symbol *sym){
	struct stack *tmp;
	
	if(head == NULL){
		head = (struct stack *)malloc(sizeof(struct stack));
		head->sym = sym;
		head->next = NULL;
		return head;
	}
	
	tmp = (struct stack *)malloc(sizeof(struct stack));
	tmp->sym = sym;
	tmp->next = head;
	
	head = tmp;
	
	return head;
	
}

struct symbol * pop(struct stack *head){
	struct stack *tmp = head;
	struct symbol *sym;
	
	if(head == NULL){
		return NULL;
	}
	else{
		sym = head->sym;
		head = head->next;
		free(tmp);
	}
	return sym;
}



struct return_jumps * append(struct return_jumps *head , unsigned label){

	struct return_jumps *tmp = head->innerreturn;

	if(head==NULL){
		return NULL;
	}

	while(tmp!=NULL){
		tmp->label=label;
		tmp= tmp->next;
	}
	return head;
}

struct return_jumps * create_return(struct return_jumps *head, unsigned num){
	struct return_jumps *tmp2,*tmp = head->innerreturn;
	if (tmp==NULL){
		tmp= (struct return_jumps *)malloc(sizeof(struct return_jumps));
		tmp->instr_number=num;
		head->innerreturn=tmp;
		tmp->next=NULL;
		return head;
	}
	else{
		tmp2 = (struct return_jumps *)malloc(sizeof(struct return_jumps));
		tmp2->instr_number=num;
		while(tmp->next!=NULL)
			tmp=tmp->next;
		tmp->next=tmp2;
		tmp2->next=NULL;
		return head;
	}
}

struct return_jumps *tar_insert (struct return_jumps *head){
	struct return_jumps *tmp;
	if (head==NULL){
		head = (struct return_jumps *)malloc(sizeof(struct return_jumps));
		head->innerreturn=NULL;
		head->next=NULL;
		return head;
	}
	else{
		tmp = (struct return_jumps *)malloc(sizeof(struct return_jumps));
		tmp->innerreturn=NULL;
		tmp->next=head;
		head=tmp;
		return head;
	}
}
struct return_jumps *tar_delete (struct return_jumps *head){
	struct return_jumps *tmp=head->innerreturn;
	struct return_jumps *tmp2;
	while(tmp!=NULL){
		tmp2=tmp->next;
		free(tmp);
		tmp=tmp2;
	}
	tmp=head;
	head=head->next;
	free(tmp);
	return head;

}

int find_name(char *name, int is_ufunction){
	int i=0;
	if(is_ufunction == 1){
	
		for(i=0;i<totalUserFuncs;i++){
			if(strcmp(name,userFuncs[i]->id)==0)
				return i;
		}
	}else{
		for(i=0;i<totalNamedLibfuncs;i++){
			if(strcmp(name,nameLibFuncs[i])==0)
				return i;
		}
	}
	return -1;
}


void print_instr_tofile(){
	int i;
	FILE *fquad = fopen("tcode.txt", "w+");
	char * booleanexpr[2] = {"false", "true"};
	
	for(i = 0; i < curr_incode; i++) {
			fprintf(fquad,"%i : ",i);
			fprintf(fquad," %d(%s) \t",instr[i].opcode,opcode_names[instr[i].opcode]);

			if(instr[i].result.type!=-1){
				fprintf(fquad," %d(%s), ",instr[i].result.type,type_names[instr[i].result.type]);
				if(instr[i].result.type==number_a)
            		fprintf(fquad," %d:%lf \t",instr[i].result.val,numConsts[instr[i].result.val]);
				else if(instr[i].result.type==string_a)
            		fprintf(fquad," %d:%s \t" ,instr[i].result.val,stringConsts[instr[i].result.val]);
				else if(instr[i].result.type==libfunc_a)
           			fprintf(fquad," %d:%s \t" ,instr[i].result.val,nameLibFuncs[instr[i].result.val]);
				else if(instr[i].result.type==userfunc_a)
           			fprintf(fquad," %d:%s \t" ,instr[i].result.val,userFuncs[instr[i].result.val]->id);
				else if(instr[i].result.type==bool_a){
					fprintf(fquad," %s \t" ,booleanexpr[instr[i].result.val]);
				}
				else if(instr[i].result.type==global_a || instr[i].result.type==local_a || instr[i].result.type==formal_a)
					fprintf(fquad," %d:%s \t",instr[i].result.val,instr[i].result.sym->u.v->name);
			 	else if(instr[i].result.type==label_a)
            		fprintf(fquad," %d \t" ,instr[i].result.val);
				else if(instr[i].result.type==nil_a)
            		fprintf(fquad," %d \t" ,instr[i].result.val);
				else
					fprintf(fquad,"\t" );

			}
			if(instr[i].arg1.type!=-1){
				fprintf(fquad," %d(%s), \t",instr[i].arg1.type,type_names[instr[i].arg1.type]);
				if(instr[i].arg1.type==number_a)
            		fprintf(fquad," %d:%lf \t",instr[i].arg1.val,numConsts[instr[i].arg1.val]);
				else if(instr[i].arg1.type==string_a)
            		fprintf(fquad," %d:%s \t" ,instr[i].arg1.val,stringConsts[instr[i].arg1.val]);
				else if(instr[i].arg1.type==libfunc_a)
           			fprintf(fquad," %d:%s \t" ,instr[i].arg1.val,nameLibFuncs[instr[i].arg1.val]);
				else if(instr[i].arg1.type==userfunc_a)
           			fprintf(fquad," %d:%s \t" ,instr[i].arg1.val,userFuncs[instr[i].arg1.val]->id);
				else if(instr[i].arg1.type==bool_a){
					fprintf(fquad," %s \t" ,booleanexpr[instr[i].arg1.val]);
				}
				else if(instr[i].arg1.type==global_a || instr[i].arg1.type==local_a || instr[i].arg1.type==formal_a)
					fprintf(fquad," %d:%s \t",instr[i].arg1.val,instr[i].arg1.sym->u.v->name);
			 	else if(instr[i].arg1.type==label_a)
            		fprintf(fquad," %d \t" ,instr[i].arg1.val);
				else if(instr[i].arg1.type==nil_a)
            		fprintf(fquad," %d \t" ,instr[i].arg1.val);
				else
					fprintf(fquad,"\t" );


			}
			if(instr[i].arg2.type!=-1){
				fprintf(fquad," %d(%s), \t",instr[i].arg2.type,type_names[instr[i].arg2.type]);
				if(instr[i].arg2.type==number_a)
            		fprintf(fquad," %d:%lf \t",instr[i].arg2.val,numConsts[instr[i].arg2.val]);
				else if(instr[i].arg2.type==string_a)
            		fprintf(fquad," %d:%s \t" ,instr[i].arg2.val,stringConsts[instr[i].arg2.val]);
				else if(instr[i].arg2.type==libfunc_a)
           			fprintf(fquad," %d:%s \t" ,instr[i].arg2.val,nameLibFuncs[instr[i].arg2.val]);
				else if(instr[i].arg2.type==userfunc_a)
           			fprintf(fquad," %d:%s \t" ,instr[i].arg2.val,userFuncs[instr[i].arg2.val]->id);
				else if(instr[i].arg2.type==bool_a){

            		fprintf(fquad," %s \t" ,booleanexpr[instr[i].arg2.val]);
				}
				else if(instr[i].arg2.type==global_a || instr[i].arg2.type==local_a || instr[i].arg2.type==formal_a )
					fprintf(fquad," %d:%s \t",instr[i].arg2.val,instr[i].arg2.sym->u.v->name);
			 	else if(instr[i].arg2.type==label_a)
            		fprintf(fquad," %d \t" ,instr[i].arg2.val);
				else if(instr[i].arg2.type==nil_a)
            		fprintf(fquad," %d \t" ,instr[i].arg2.val);

				else
					fprintf(fquad,"\t" );


			}
			fprintf(fquad,"\n");


		}
		fclose(fquad);
	
	}



