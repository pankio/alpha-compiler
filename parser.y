%{
	#include<stdio.h>
	#include<stdlib.h>
	#include<string.h>
	#include<time.h>
	#include<math.h>
	#include"scanner.h"
	#include"inter.h"
	#include"symboltable.h"
	#include"inCode.h"
	#include"tarCode.h"
	#include"binary.h"

	int yyerror (char *);
	int alpha_yylex (void *);

	extern int yylineno;
	extern char* yytext;
	extern FILE* yyin;
	
	extern unsigned totalNumConsts;
	extern unsigned totalStringConsts;
	extern unsigned totalNamedLibfuncs;
	extern unsigned totalUserFuncs;
	extern int curr_incode;
	
	int loopcounter = 0, breakcntblock=0, otinanai = 1;
	int scope = 0, i, Max;
	int scopeDead,num_progVars = 0;
	struct alpha_token_t *alpha;
	int argsNum = -1;
	char **args;
	int prfundef = 0;	//gia mia metavliti
	int scopeHide = 0;
	int anonymous = 0;
	char *func_name;
	struct expr *elist_l = NULL;
	struct expr *indexed_l = NULL;
	int error = 0;



	struct funcdef_scope *loopcntstack;
	struct funcdef_scope *b;
	struct funcdef_scope *c;

	struct funcdef_scope *insertf(struct funcdef_scope *, int fscope);
	struct funcdef_scope *delete(struct funcdef_scope *);	
	enum scopespace_t currscopespace(void);	
	unsigned int programVarOffset = -1;	
	unsigned int functionLocalOffset = -1;	
	unsigned int formalArgOffset = -1;	/*Gia na doume an einai programvar,formalarg i metabliti mesa se function*/
	unsigned int scopeSpaceCounter = 1;
	struct forhelp{
		int enter;
		int test;
	};
	
%}

%union Types{
	struct expr *exprV;
	char *stringV;
	int intV;
	double doubleV;
	struct forhelp *forV;
	
};

%start program

%token<intV> INTCONST
%token<doubleV> DOUBLECONST
%token<stringV> IF_KEYWORD ELSE_KEYWORD WHILE_KEYWORD FOR_KEYWORD FUNCTION_KEYWORD RETURN_KEYWORD
%token<stringV> BREAK_KEYWORD CONTINUE_KEYWORD AND_KEYWORD NOT_KEYWORD OR_KEYWORD LOCAL_KEYWORD
%token<stringV> GLOBAL_KEYWORD TRUE_KEYWORD FALSE_KEYWORD NIL_KEYWORD  
%token<stringV> MINUS_OPERATOR MUL_OPERATOR DIV_OPERATOR MOD_OPERATOR EQUAL_OPERATOR NEQ_OPERATOR ASSIGN_OPERATOR PLUS_OPERATOR
%token<stringV> PLUSPLUS_OPERATOR MINUSMINUS_OPERATOR GRE_OPERATOR SMA_OPERATOR GREQ_OPERATOR
%token<stringV> SMAQ_OPERATOR LAG DAG LBRA DBRA FULLST LPAR DPAR QUESTION COMA DFULLST D_FULLSTP DOLLAR
%token<stringV> ID STRING ERRORR


%type<forV> forprefix
%type<intV> ifprefix whilestart whilecontinue elseprefix  N M
%type<exprV>  stmts ifstmt whilestmt forstmt returnstmt exprs exprs2 loopstmt
%type<exprV> block funcdef assignexpr expr term idlist1 idlist2 loc glob
%type<exprV> lvalue primary call objectdef member objectdef2 const id
%type<exprV> elist indexed indexedelem idlist stmt indexed1 indexed2 returnstmt2

%left ASSIGN_OPERATOR
%left OR_KEYWORD
%left AND_KEYWORD
%nonassoc EQUAL_OPERATOR NEQ_OPERATOR
%nonassoc GRE_OPERATOR GREQ_OPERATOR SMA_OPERATOR SMAQ_OPERATOR
%left PLUS_OPERATOR MINUS_OPERATOR
%left MUL_OPERATOR DIV_OPERATOR MOD_OPERATOR
%right NOT_KEYWORD PLUSPLUS_OPERATOR MINUSMINUS_OPERATOR UMINUS
%left FULLST
%left LBRA DBRA
%left LPAR DPAR

%%
	program:	stmts	{printf("programm->stmts\n");emit(nop, NULL, NULL, NULL, 0, yylineno);}
			;

	stmts:		stmt stmts	{printf("stmts->stmt stmts\n");}
			|		{printf("NO rule stmts\n");}
			;

	


	stmt:		error stmt				{yyerrok;error = 1;}
			|ERRORR					{yyerror("parse error invalid character ");error = 1;}
			|expr QUESTION			{resettemp();printf("stmt->expr ;\n");}
			|ifstmt				{resettemp();printf("stmt->ifstmt\n");}
			|whilestmt			{resettemp();printf("stmt->whilestmt\n");}
			|forstmt			{resettemp();printf("stmt->forstmt\n");}
			|returnstmt			{printf("stmt->returnstmt\n");}
			|BREAK_KEYWORD QUESTION		{	
								if(loopcounter >0 && otinanai !=0){
									printf("error: illegal break statement (not in a loop) at line %d\n", yylineno);
									error = 1;}
								else{
									b=insertf(b, nextquadlabel()); emit(jump, NULL,NULL,NULL, 0, yylineno);
									b->foffset = breakcntblock;
								}
								printf("stmt->break ;\n");
							}
			|CONTINUE_KEYWORD QUESTION	{
								if(loopcounter >0 && otinanai !=0){
									printf("error: illegal continue statement (not in a loop) at line %d\n", yylineno);
									error = 1;}
								else{
									c=insertf(c, nextquadlabel()); emit(jump, NULL,NULL,NULL, 0, yylineno);
									c->foffset = breakcntblock;
								}
								printf("stmt->continue ;\n");
							}
			|block				{resettemp();printf("stmt->block\n");}
			|funcdef			{resettemp();printf("stmt->funcdef\n");}
			|QUESTION			{printf("stmt->;\n");}
			;
			

	expr:		assignexpr			{if($1->sym != NULL) {
							 	$$ = $1;
							 }
							 else $$->sym = NULL;
								printf("expr->assirnexpr\n");
							}
			|expr PLUS_OPERATOR expr	{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = operations_expr(add, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr + expr\n");
							}
			|expr MINUS_OPERATOR expr	{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = operations_expr(sub, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr - expr\n");
							}
			|expr MUL_OPERATOR expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = operations_expr(mul, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr * expr\n");
							}
			|expr DIV_OPERATOR expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = operations_expr(divX, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr / expr\n");
							}
			|expr MOD_OPERATOR expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = operations_expr(mod, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr % expr\n");
							}
			|expr GRE_OPERATOR expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = bool_op_expr(if_greater, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr > expr\n");
							}
			|expr GREQ_OPERATOR expr	{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = bool_op_expr(if_greatereq, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr >= expr\n");
							}
			|expr SMA_OPERATOR expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = bool_op_expr(if_less, $1, $3);
							 }
							 else $$->sym = NULL;	
								printf("expr->expr < expr\n");
							}
			|expr SMAQ_OPERATOR expr	{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = bool_op_expr(if_lesseq, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr <= expr\n");
							}
			|expr EQUAL_OPERATOR expr	{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = bool_op_expr(if_eq, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr == expr\n");
							}
			|expr NEQ_OPERATOR expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = bool_op_expr(if_noteq, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr != expr\n");
							}
			|expr OR_KEYWORD expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = andor_expr(or, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr or expr\n");
							}
			|expr AND_KEYWORD expr		{if($1->sym != NULL && $3->sym != NULL) {
							 	$$ = andor_expr(and, $1, $3);
							 }
							 else $$->sym = NULL;
								printf("expr->expr and expr\n");
							}
			|term				{	$$ = $1;
								printf("expr->term\n");	
							}
			;

	term:		LPAR expr DPAR				{$$ = $2; printf("term->( expr )\n");}
			|MINUS_OPERATOR expr %prec UMINUS	{
									if($2->sym != NULL){
										$$ = expruminus(uminus, $2);
									}
									printf("term->- expr\n");
								}
			|NOT_KEYWORD expr			{if ( $2->sym != NULL){
	   								$$ = not_expr(not,$2);
								 }
								 else $$->sym = NULL;
									printf("term->not expr\n");
								}
			|PLUSPLUS_OPERATOR lvalue		{	struct expr *e = newexpr_intnum(1);
									if($2->sym != NULL){
										$$ = sspp_lvalue(add, $2, e);
										free(e);
								 	}
									else $$->sym = NULL;
									printf("term->++ lvalue\n");
								}
			|lvalue PLUSPLUS_OPERATOR		{	struct expr *e;
									$$ = newexpr(var_e);
									$$->sym = newtemp(scope);

									if($1->type == tableitem_e) {
										e = emit_iftableitem($1);
										emit(assign, e, $1, NULL, 0, yylineno);
										$$ = sspp_lvalue(add, $1, newexpr_intnum(1));
										emit(tablesetelem,$1, $1->index, e, 0, yylineno);
									}
									else {
										emit(assign, newexpr_intnum(1), NULL, $$, 0, yylineno);
										$$ = sspp_lvalue(add, $1, $$);
									}
									printf("term->lvalue ++\n");
								}
			|MINUSMINUS_OPERATOR lvalue		{	struct expr *e = newexpr_intnum(1);
									if($2->sym != NULL){
										$$ = sspp_lvalue(sub, $2, e);
										free(e);
								 	}
									else $$->sym = NULL;
									printf("term->--lvalue\n");
								}
			|lvalue MINUSMINUS_OPERATOR		{	struct expr *e;
									$$ = newexpr(var_e);
									$$->sym = newtemp(scope);

									if($1->type == tableitem_e) {
										e = emit_iftableitem($1);
										emit(assign, e, $1, NULL, 0, yylineno);
										$$ = sspp_lvalue(add, $1, newexpr_intnum(1));
										emit(tablesetelem, $1,$1->index, e, 0, yylineno);
									}
									else {		
										emit(assign, newexpr_intnum(1), NULL, $$, 0, yylineno);
										$$ = sspp_lvalue(sub, $1, $$);
									}
									printf("term->lvalue --\n");
								}
			|primary				{	$$ = $1;
									printf("term->primary\n");
								}
			;

	assignexpr:  	lvalue ASSIGN_OPERATOR expr	{if($1->sym != NULL && $3->sym != NULL) {	
								if($1->type == tableitem_e) {
									emit(tablesetelem, $1->index, $3, $1, 0, yylineno);
									$$ = emit_iftableitem($1);
									$$->type = assignexpr_e;
								}
								else {
									emit(assign, $3, NULL, $1, 0, yylineno);
									$$ = newexpr(assignexpr_e);
									$$->sym = newtemp(scope);
									emit(assign, $1, NULL, $$, 0, yylineno);
									$$ = $1;
								}
								
							 }
							 else
							 	$$->sym = NULL;

							printf("assignexpr->lvalue = expr\n");
							}
			;

	primary:  	lvalue			{	$$ = emit_iftableitem($1);
							
							printf("primary->lvalue\n");
						}
			|call			{	$$ = $1;
							printf("primary->call\n");
						}
			|objectdef		{	$$ = $1;
							printf("primary->objectdef\n");
						}
			|LPAR funcdef DPAR	{	
							$$ = $2;
							printf("primary->( funcdef )\n");
						}
			|const			{	$$ = const_expr(assign, $1);
							printf("primary->const\n");
						}	
			;

	lvalue: 	ID		{	struct expr *e;
						struct symbol *sym;
						int i;	
						printf("lvalue->ID\n");
						
						
						if((lookup_total($1) == 0) /*&& (lookup_one_level($1, scope) == 0)*/){
							if(currscopespace() == 0){
								programVarOffset++;
								insert(scope,1,create_v($1,scope,yylineno,currscopespace(),programVarOffset),NULL, NULL);	
								num_progVars++;
								sym = lookup_retSym($1, scope);
								e = newexpr_var(sym);
								$$ = e;	
							}else if(currscopespace() == 1){//na balo kati
								fs->foffset++;
								functionLocalOffset = fs->foffset;
								insert(scope,1,create_v($1,scope,yylineno,currscopespace(),fs->foffset),NULL, NULL);}
								sym = lookup_retSym($1, scope);
								e = newexpr_var(sym);
								$$ = e;	
						}
						else{
						
							if(scope-1>=0 && prfundef>=1){
								for(i=fs->fscope;i<=scope;i++){
									sym = lookup_retSym($1, i);
									if(sym!=NULL){
										break;
									}
								}
							}else{
								for(i=0;i<=scope;i++){
									sym = lookup_retSym($1, i);
									if(sym!=NULL){
										break;
									}
								}
							}
							if(sym == NULL) {
								printf("error:Redefine variable at line:%d\n",yylineno);
								error = 1;
								$$ = NULL;
							}
							else {
								e = newexpr_var(sym);
								$$ = e;	
							}
						}
					
					}
			|loc ID		{struct expr *e;
						struct symbol *sym;	
					printf("lvalue->loc ID\n");
					if(scope == 0){
						
						printf("parse error:illegal statement local at line %d\n",yylineno);
						error = 1;
					}
					else{
						if(lookup_one_level($2, scope) == 0){
							if(currscopespace() == 0){
								programVarOffset++;
								insert(scope,1,create_v($2,scope,yylineno,currscopespace(),programVarOffset),NULL, NULL);
								num_progVars++;
								sym = lookup_retSym($2, scope);
							}else if(currscopespace() == 1){//na balo kati
								fs->foffset++;
								functionLocalOffset = fs->foffset;
								insert(scope,1,create_v($2,scope,yylineno,currscopespace(),fs->foffset),NULL, NULL);}
								sym = lookup_retSym($2, scope);
						}
						sym = lookup_retSym($2, scope);
						if(sym == NULL) {
							printf("error:Redefine variable at line:%d\n",yylineno);
							error = 1;
							$$ = NULL;
						}
						else {
							e = newexpr_var(sym);
							$$ = e;	
						}
					}
					}
			|glob ID	{struct expr *e;
						struct symbol *sym;						
						printf("lvalue->glob ID\n");
						if(lookup_total($2) == 0){
							if(scope == 0){
								programVarOffset++;
								insert(scope,1,create_v($2,scope,yylineno,currscopespace(),programVarOffset),NULL, NULL);
								num_progVars++;
								sym = lookup_retSym($2, 0);
							}
							else{
								printf("parse error:illegal statement global at line %d\n", yylineno);
								error = 1;
							}
								
						}
						sym = lookup_retSym($2, 0);
						if(scope == 0 && sym == NULL){
							printf("error:Redefine variable at line:%d\n",yylineno);
							error = 1;
							$$ = NULL;
						}
						else{
							e = newexpr_var(sym);
							$$ = e;
						}
						
					}
			|member		{	$$=$1;
						printf("lvalue->member\n");
					}
			;

	member:  	lvalue FULLST ID	{	
							if($1->sym!=NULL){
							
							/*	struct symbol * sym = newtemp(scope);
								struct expr * expres= newexpr_var(sym);
							
								struct expr* table = newexpr(tableitem_e);
								table->sym = $1->sym;
								//if($1->type==tableitem_e)
								//emit(tablegetelem, table, newexpr_conststring($3), expres, 0, yylineno);
								//$1 = emit_iftableitem($1);
								$$ = expres;
								$$->type = tableitem_e;
								$$-> sym = $1->sym;
								$$ -> index = newexpr_conststring($3); */
								
								$1 = emit_iftableitem($1);
								$$ = newexpr(tableitem_e);
								$$ -> sym = $1->sym;
								
								char *str = (char *) malloc(sizeof(char) * strlen($3)+3);
								int i;
								
								str[0] = '\"';
								for(i=1;i<strlen($3)+1; i++){
									str[i] = $3[i-1];
								}
								int size = strlen(str);
								str[size] = '\"';
								str[size+1] = '\0';
								
								$$->index = newexpr_conststring(str);
							}
							
							printf("member->lvalue . ID\n");
						}
			|lvalue LBRA expr DBRA	{
							if($1->sym !=NULL && $3->sym !=NULL){
							
								$1 = emit_iftableitem($1);
								$$ = newexpr(tableitem_e);
								$$-> sym = $1->sym;
								$$->index = $3;
							}
							printf("member->lvalue [ expr ]\n");
						}
			|call FULLST ID		{	
							if($1->sym!=NULL){
							

								
								$1 = emit_iftableitem($1);
								$$ = newexpr(tableitem_e);
								$$ -> sym = $1->sym;
								$$->index = newexpr_conststring($3);
							}
							printf("member->call . ID\n");
						}
			|call LBRA expr DBRA	{	
							if($1->sym !=NULL && $3->sym !=NULL){
							
								$1 = emit_iftableitem($1);
								$$ = newexpr(tableitem_e);
								$$-> sym = $1->sym;
								$$->index = $3;
							}
							printf("member->call [ expr ]\n");
						}
			;

	call:  		call LPAR elist DPAR			{	
									int i, find = 0;
									struct expr *ex;
									struct symbol *sym;
									
									if($1->sym!=NULL){
										for(i=0; i<=Max; i++) {
											if(lookup_one_level($1->sym->u.v->name, i)){
												sym = lookup_retSym($1->sym->u.v->name, i);
												ex = newexpr_var(sym);
												$$ = newexpr_call(ex, elist_l);
												elist_l = NULL;
												find = 1;
												break;
											} 
										}
									}
									if(find == 0) {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call->call ( elist )\n");
								}
								
			|ID LPAR elist DPAR			{
									int i, find = 0;
									struct symbol *sym;
									struct expr *ex;
									
									for(i=0; i<=Max; i++) {
										if(lookup_one_level($1, i)){
											sym = lookup_retSym($1, i);
											ex = expr_func(sym);
											$$ = newexpr_call(ex, elist_l);
											elist_l = NULL;
											find = 1;
											break;
										} 
									}
									if(find == 0) {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call->ID ( elist )\n");
								}	

			|glob ID LPAR elist DPAR		{
									int i, find = 0;
									struct symbol *sym;
									struct expr *ex;
									
									for(i=0; i<=Max; i++) {
										if(lookup_one_level($2, i)){
											sym = lookup_retSym($2, i);
											ex = expr_func(sym);
											$$ = newexpr_call(ex, elist_l);
											elist_l = NULL;
											find = 1;
											break;
										} 
									}
									if(find == 0) {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call-> glob ID ( elist )\n");
								}
							
			|loc ID LPAR elist DPAR			{
									struct symbol *sym;
									struct expr *ex;
									
									if(lookup_one_level($2, scope)){
										sym = lookup_retSym($2, i);
										ex = expr_func(sym);
										$$ = newexpr_call(ex, elist_l);
										elist_l = NULL;								
									}
									else {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}	
									printf("call->loc ID ( elist )\n");		
								}
			
			|member LPAR elist DPAR			{
									struct symbol *sym;
									if($1->sym != NULL) {
										$$ = newexpr_call($1, elist_l);
										elist_l = NULL;
									}	
									else {
										printf("Undefined call of function at line:%d\n",yylineno);
										error = 1;
									}
									printf("call->member ( elist )\n");
								}

			|LPAR funcdef DPAR LPAR elist DPAR	{	
									
									$$ = newexpr_call($2, elist_l);
									elist_l = NULL;
									printf("call->( funcdef ) ( elist )\n");
								}
			;

	elist: 		exprs	{	/*if(elist_l == NULL){
						elist_l = exprcpy(elist_l, $1);
					}
					else{	
						$1->next = elist_l;
						elist_l = exprcpy(elist_l, $1);
					}*/
					printf("elist->exprs\n");}
			|	{printf("NO rule elist\n");}
			;

	exprs:		expr exprs2	{	if(elist_l == NULL){
							elist_l = exprcpy(elist_l, $1);
						}
						else{     
							$1->next = elist_l;
							elist_l = exprcpy(elist_l, $1);
						}
						printf("exprs->expr exprs2\n");}
			;

	exprs2:		COMA exprs	{$$ = $2; printf("exprs2->, exprs\n");}
			|		{elist_l = NULL; printf("NO rule exprs2\n");}
			;

	objectdef: 	LBRA objectdef2 DBRA	{$$ = $2; printf("objectdef->[ objectdef2 ]\n");}
			;

	objectdef2:	indexed		{
	
						struct expr *t = newexpr(newtable_e);
						struct expr *tmp;
						int i = 0;
						
						t->sym = newtemp(scope);
						emit(tablecreate, NULL, NULL, t, 0, yylineno);
						for(tmp = indexed_l; tmp != NULL; tmp = tmp->next)
							emit(tablesetelem, tmp, tmp->index, t, 0, yylineno);
						$$ = t;
						printf("objectdef2->indexed\n");
					}
			|elist		{	
			
						struct expr *t = newexpr(newtable_e);
						struct expr *tmp;
						int i = 0;
						
						t->sym = newtemp(scope);
						emit(tablecreate, NULL, NULL, t, 0, yylineno);
						for(tmp = elist_l; tmp != NULL; tmp = tmp->next)
							emit(tablesetelem, newexpr_intnum(i++), tmp, t, 0, yylineno);
						$$ = t;
						printf("objectdef2->elist\n");
					}
			;

	indexed:  	indexed1	{	/*if(indexed_l == NULL){
							indexed_l = exprcpy(indexed_l, $1);
						}
						else{
							$1->next = indexed_l;
							//indexed_l = $1;							
							indexed_l = exprcpy(indexed_l, $1);
							//indexed_l->next = NULL;
						}*/
						printf("indexed->indexed1\n");}		
			;

	indexed1:	indexedelem indexed2	{	if(indexed_l == NULL){
								indexed_l = exprcpy(indexed_l, $1);
							}
							else{
								$1->next = indexed_l;
								indexed_l = exprcpy(indexed_l, $1);
						}
							printf("indexed1->indexedelem indexed2\n");}
			;

	indexed2:	COMA indexed1	{$$ = $2; printf("indexed2->, indexed1\n");}	
			|		{indexed_l = NULL; printf("NO rule indexed2\n");}
			;
	
	indexedelem:    LAG expr DFULLST expr DAG	{$2 ->index = exprcpy($2->index,$4);$$=$2;printf("indexedelem->{ expr : expr }\n");}
			;

	block:  	LAG{scope++;Max=scope;} stmts DAG	{printf("block->{ stmt }\n");
							scope--;hide(scope+1);}
			;

	funcdef:  	FUNCTION_KEYWORD id{
				loopcounter ++;
				otinanai = 1;	
				fs = insertf(fs,scope);
				if(fs->next!=NULL){
					for(i=fs->fscope;i>fs->next->fscope;i--) {
						//printf("hide scope:%d\n",i);
						hide(i);
					}
				}
			}
			LPAR {
				
			scopeSpaceCounter++;
			
			argsNum=-1;args = (char**)malloc(sizeof(char*));scope++;Max=scope;} idlist {
			if(anonymous==0) {
				if(lookup_total(((char *)$2)) == 0){
				insert(scope-1,2,NULL,create_f(((char *)$2),args,scope,yylineno,argsNum+1), NULL);}
				else {printf("error:Redefine function at line:%d\n",yylineno);error = 1;}
			} 
			else{while(lookup_total(func_name = create_funcName(func_name))){/*printf("error:Redefine function at line:%d\n",yylineno);*/}
				insert(scope-1,2,NULL,create_f(func_name,args,scope,yylineno,argsNum+1), NULL);
				
			}
			struct expr *e;
			struct symbol *sym;
			if(anonymous == 0)//an den einai  anonimi
				sym = lookup_retSym((char *)$2, scope-1);
			else
				sym = lookup_retSym(func_name, scope-1);
				
			anonymous = 0;
				
			e = expr_func(sym);
			$2 = e;
			emit(funcstart,$2,NULL,NULL,0,yylineno);
			//$$ = $2;
			}
			DPAR {prfundef++;scopeSpaceCounter++;formalArgOffset = -1;
				/*loopcntstack = insertf(loopcntstack, loopcounter);loopcounter = 0;*/
			} block {		
											loopcounter --;
											/*loopcounter = loopcntstack->fscope;
											loopcntstack = delete(loopcntstack);*/
											scopeSpaceCounter = scopeSpaceCounter - 2;
											printf("funcdef->function id ( idlist ) block\n");
											scopeDead = scope+1;
											printf("dead scope:%d\n",scopeDead);
											dead(scopeDead);
											scopeDead--;
											if(scopeDead != 0) {
												//printf("dead scope:%d\n",scopeDead);
												dead(scopeDead);
											}
											scope--;
											prfundef--;
											if(fs->next != NULL){
												for(i=fs->fscope;i>fs->next->fscope;i--){	
													//printf("unhide scope:%d\n",i);
													unhide(i);
												}
											}				
											fs = delete(fs);
											emit(funcend,$2,NULL,NULL,0,yylineno);
											$$ = $2;
			}
			;

	id:		ID	{printf("id->ID\n");}
			|	{printf("NO rule id\n");
				anonymous = 1;}
			;

	const: 		INTCONST 	{	struct expr *e = newexpr_intnum($1);
						$$ = e;
						printf("const->intconst\n");
					}
			|DOUBLECONST	{	struct expr *e = newexpr_floatnum($1);
						$$ = e;
						printf("const->doubleconst\n");
					}
			|STRING		{	struct expr *e = newexpr_conststring($1);
						$$ = e;
						printf("const->string\n");
					}
			|NIL_KEYWORD	{	struct expr *e = newexpr_nil($1);
						$$ = e;
						printf("const->nil\n");
					}
			|TRUE_KEYWORD	{	struct expr *e = newexpr_constbool(1u);
						$$ = e;
						printf("const->true\n");
					}
			|FALSE_KEYWORD	{	struct expr *e = newexpr_constbool(0u);
						$$ = e;
						printf("const->false\n");
					}	
			;

	idlist: 	idlist1		{printf("idlist->idlist1\n");}
			;

	idlist1:	ID idlist2	{printf("idlist->ID idlist2\n");
					args[argsNum+1] = strdup($1);
					if(lookup_one_level($1, scope)==0)
					{
						argsNum++;
						formalArgOffset++;
						insert(scope,1,create_v($1,scope,yylineno,currscopespace(),formalArgOffset),NULL, NULL);}
					else{printf("error:Redefine argument at line:%d\n",yylineno);error = 1;}
					}
				|				{printf("NO rule\n");}
			;

	idlist2:	COMA idlist1	{printf("idlist2->, idlist1\n");
					
					}
			|		{printf("NO rule idlist2\n");}
			;



	ifstmt:  	ifprefix stmt {patchlabel($1, nextquadlabel());printf("ifstmt->if ( expr ) stmt\n");}
			;
			
	ifprefix:	IF_KEYWORD LPAR expr DPAR {emit(if_eq, $3, newexpr_constbool(1u), NULL, nextquadlabel() + 2, yylineno);
							$$ = nextquadlabel();
							emit(jump, NULL, NULL, NULL, 0, yylineno);
						  }
			;
			
	ifstmt:  	ifprefix stmt elseprefix stmt		{
									printf("ifstmt->if ( expr ) stmt elsestmt\n");
									patchlabel($1, $3+1);
									patchlabel($3,nextquadlabel());
								}
			
			;			
	
	
			
	elseprefix:	ELSE_KEYWORD{
					$$ = nextquadlabel();
					emit(jump, NULL, NULL, NULL, 0, yylineno);
				    }



	whilestmt:  	whilestart  whilecontinue loopstmt	{	
								struct funcdef_scope *tmp;
								emit(jump, NULL, NULL, NULL, $1, yylineno);
								patchlabel($2, nextquadlabel());
								printf("whilestmt->while ( expr ) stmt\n");
								
								if(b!=NULL)
									patchlabel(b->fscope, nextquadlabel());
								if(c!=NULL)
									patchlabel(c->fscope, $1);
									
								for(tmp=b;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									b=delete(b);
									patchlabel(b->fscope, nextquadlabel());
									
								}	
								for(tmp=c;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									c=delete(c);
									patchlabel(c->fscope, $1);
									
								}
									
								b=delete(b);
								c=delete(c);
								
								breakcntblock--;
								}
			;

	whilestart:	WHILE_KEYWORD {
						breakcntblock++;
						$$ = nextquadlabel();}
			;

	whilecontinue:	LPAR expr DPAR {emit(if_eq, $2, newexpr_constbool(1u), NULL, nextquadlabel() + 2, yylineno);
						$$ = nextquadlabel();
						emit(jump, NULL, NULL, NULL, 0, yylineno);}
			;


	N:		{$$ = nextquadlabel();emit(jump, NULL, NULL, NULL, 0, yylineno);}
			;
	
	M:		{$$ = nextquadlabel();}
			;
	
	
	loopstart:	{otinanai = 0;}
			;
			
	loopend:	{otinanai =6;}
	;
	
	loopstmt:	loopstart stmt loopend{$$ = $2;}
			;
			
	
	
	
	forprefix:	FOR_KEYWORD LPAR elist QUESTION M expr QUESTION{
				breakcntblock++;
				$$->test = $5;
				$$->enter = nextquadlabel();
				emit(if_eq,$6, newexpr_constbool(1u), NULL, 0, yylineno);
	
			}


	forstmt:  	forprefix N elist DPAR N loopstmt N	{
								struct funcdef_scope *tmp;
								printf("forstmt->for (elist;expr;elist) stmt\n");
								patchlabel($1->enter, $5 + 1);
								patchlabel($2, nextquadlabel());
								patchlabel($5, $1->test);
								patchlabel($7, $2+1);
								if(b!=NULL)
									patchlabel(b->fscope, nextquadlabel());
								if(c!=NULL)
									patchlabel(c->fscope, $2+1);
									
								for(tmp=b;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									b=delete(b);
									patchlabel(b->fscope, nextquadlabel());
									
								}	
								for(tmp=c;(tmp !=NULL) && (tmp->next != NULL)&&(tmp->foffset == tmp->next->foffset); tmp = tmp->next){
									c=delete(c);
									patchlabel(c->fscope, $2+1);
									
								}
									
								b=delete(b);
								c=delete(c);
								breakcntblock--;
								}
			;

	returnstmt:  	RETURN_KEYWORD returnstmt2 QUESTION	{
	
									if(prfundef > 0){
										emit(ret, NULL, NULL, $2, 0, yylineno);
									}
									else{
										printf("error: return statement not in a function at line :%d\n", yylineno);
										error = 1;
									}
										
	
									printf("returnstmt->return returnstmt2 ;\n");
									$$=$2	;
								}
			;

	returnstmt2:	expr	{$$ = $1; printf("returnstmt2->expr\n");}
			|	{$$=NULL;printf("NO rule returnstmt2\n");}
			;

	loc:		LOCAL_KEYWORD		{printf("loc->local\n");}
			|DOLLAR			{printf("loc->$\n");}
			;

	glob:		GLOBAL_KEYWORD		{printf("glob->global\n");}
			|D_FULLSTP		{printf("glob->::\n");}
			;
			
%%

/*Gia na doume an einai programvar,formalarg i metabliti mesa se function*/
enum scopespace_t currscopespace(void){

	if(scopeSpaceCounter == 1){
		return programvar;
	}
	else
	if(scopeSpaceCounter % 2 == 0){
		return formalarg;
	}
	else 
		return	functionlocal;

}



struct funcdef_scope *insertf(struct funcdef_scope *head, int x){
	struct funcdef_scope * tmp;

	if(head==NULL)/*eidiki perisptosi gia keni lista*/
	{
		head = (struct funcdef_scope *)malloc(sizeof(struct funcdef_scope));
		head->fscope = x;
		head->next = NULL;
		head->foffset= -1 ;
		return head;
	}
	
	tmp = (struct funcdef_scope *)malloc(sizeof(struct funcdef_scope));
	tmp->fscope = x;
	tmp->next = head;
	tmp->foffset= -1 ;
	
	head = tmp;
	return head;
	
}

struct funcdef_scope *delete(struct funcdef_scope *head){
	struct funcdef_scope *tmp = head;	
	if(head == NULL) {
		return NULL;
	}		

	else {
		head = head->next;
		free(tmp);
	}
	return head;
}

int main(int argc, char** argv)
{
	
	srand(time(NULL));	

	alpha = (struct alpha_token_t *)malloc(sizeof(struct alpha_token_t));
	alpha->token = (char *)malloc(sizeof(char)*1000);
	yylval1 = (void *)alpha;

	if(argc > 0)
		yyin = fopen(argv[1], "r");
	else
		yyin = stdin;

	initHash();
	yyparse();
	printf("\n\n");
	if(error==0){
		printAll();
		printf("\n\n");
		//print_quads();
		//print_quads_tofile();
		generate_all();
		print_instr_tofile();
	
		write_binary(numConsts,stringConsts,userFuncs,nameLibFuncs,instr,totalNumConsts,totalStringConsts,totalNamedLibfuncs,totalUserFuncs,num_progVars,curr_incode);
	}
	else{
		printf("Error at compile.No executable created!!!!!\n");
	}

	
	fclose(yyin);
	
	return 0;
}

int yyerror(char *yaccProvideMessage){
	fprintf(stderr,"%s at line %d ,before token:%s\n",yaccProvideMessage,yylineno,yytext);
	return 0;
}

