#ifndef _SYMBOLTABLE_H_
#define _SYMBOLTABLE_H_
#define k 100

#include<stdio.h>
#include<string.h>
#include<stdlib.h>



enum scopespace_t {
	programvar,
	functionlocal,
	formalarg,
	function	//gia allh fash
};

enum symbol_t {
	 libraryfunc_s, var_s, programfunc_s
};

struct symbol_table{
	int dead;
	int scope;
	struct symbol{
		int active;
		enum symbol_t type;
		
		union u_{
			struct varab *v;
			struct function *f;
			struct libfunction *lf;
		}u;
		
		struct symbol *next;
	}**hashtable;
}**sym_tabl;

struct varab{
	char *name;
	int scope;
	int line;
	enum scopespace_t space;
	unsigned int offset;
};	

struct function{
	char *name;
	char **args;
	int scope;
	int line;
	unsigned int localsize;
	enum scopespace_t space;
};

struct libfunction{
	char *name;
	int scope;
};


struct symbol *insert(int, int, struct varab*, struct function*, struct libfunction*);

int lookup_one_level(char *, int);

struct symbol *lookup_retSym (char *name, int scope);

int lookup_total(char *);

void dead(int);

void hide(int);

void unhide(int);

struct varab *create_v(char *,  int, int, enum scopespace_t, int );

struct function *create_f(char *, char **, int, int, int);

struct libfunction *create_lf(char *, int);

unsigned int hash(const char *, unsigned int);

int lookup_up_dead(int);

void initHash();

void printAll();

void freeAll();

char *create_funcName(char *);



#endif

