#ifndef _INCODE_H_
#define _INCODE_H_


#include"symboltable.h"


enum iopcode {
		assign=0, add, sub, mul, divX, mod, if_eq, if_noteq,
		if_lesseq, if_greatereq, if_less, if_greater, jump, 
		call, param, funcstart, funcend, tablecreate, tablegetelem, 
		tablesetelem, nop, uminus, and, or, not, getretval, ret 
};

	struct funcdef_scope{/*gia emfoleumena pramata*/
		int fscope;
		struct funcdef_scope *next;
		int foffset;
	}*fs;

struct quad {
	enum iopcode op;
	struct expr * arg1;
	struct expr * arg2;
	struct expr * result;
	unsigned label;
	unsigned line;
	unsigned int taddress;
};

enum expr_t {
	var_e,
	tableitem_e,
	
	programfunc_e,
	libraryfunc_e,
	
	arithexpr_e,
	boolexpr_e,
	assignexpr_e,
	newtable_e,
	
	intnum_e,
	floatnum_e,
	constbool_e,
	conststring_e,
	
	nil_e		
};


struct expr {
	enum expr_t 	type;
	struct symbol 	*sym;
	struct expr 	*index;
	int 	 intConst;
	double	 floatConst;
	char	*strConst;
	unsigned char boolConst;
	struct expr*	next;
};



void expand ( void );
void emit(enum iopcode , struct expr *, struct expr *, struct expr *, unsigned , unsigned );


unsigned nextquadlabel(void);

void patchlabel( unsigned quadNo, unsigned label );

struct expr * lvalue_expr(struct symbol *sym);

struct expr* newexpr (enum expr_t t);

struct expr * newexpr_conststring(char *s);

struct expr * newexpr_intnum(int s);

struct expr * newexpr_floatnum(float s);

struct expr * newexpr_constbool(const char s);

struct expr * newexpr_nil(char *s);

struct expr * newexpr_var(struct symbol * sym);

struct expr * newexpr_call(struct expr *, struct expr *);

struct expr* emit_iftableitem(struct expr *e);

void checkuminus(struct expr *e);

void comperror(char* format,int line);

void resettemp();

struct symbol * newtemp(int);

char *newtempname( void);

struct expr * operations_expr(enum iopcode op, struct expr* arg1, struct expr *arg2);

void print_quads();

void print_quads_tofile();

struct expr *bool_op_expr(enum iopcode op, struct expr *arg1, struct expr *arg2);

struct expr *eq_neq_expr(enum iopcode op, struct expr *arg1, struct expr *arg2);

struct expr *andor_expr(enum iopcode op, struct expr *arg1, struct expr *arg2);

struct expr *not_expr(enum iopcode op, struct expr *arg1);

struct expr *sspp_lvalue(enum iopcode op, struct expr *arg1, struct expr *arg2);

struct expr *exprcpy(struct expr *e1, struct expr *e2);

struct expr *expr_func(struct symbol *);

struct expr *const_expr(enum iopcode, struct expr *);

struct expr *expruminus(enum iopcode, struct expr *);

#endif
