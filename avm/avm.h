#ifndef _AVM_H_
#define _AVM_H_

#include "../tarCode.h"
#include "../binary.h"

#define AVM_STACKSIZE	4096

#define AVM_TABLE_HASHSIZE 211


#define execute_add execute_arithmetic
#define execute_sub execute_arithmetic
#define execute_mul execute_arithmetic
#define execute_div execute_arithmetic
#define execute_mod execute_arithmetic



enum avm_memcell_t{
	number_m = 0,
	string_m = 1,
	bool_m = 2,
	table_m = 3,
	userfunc_m = 4,
	libfunc_m = 5,
	nil_m = 6,
	undef_m = 7
};

struct avm_memcell{
	enum avm_memcell_t type;
	
	union d{
		double			numVal;
		char * 			strVal;
		unsigned char		boolVal;
		struct 	avm_table*	tableVal;
		unsigned 		funcVal;
		char * 			libfuncVal;
	
	}data;
	
};


struct avm_table_bucket{
	struct avm_memcell	*key;
	struct avm_memcell	*value;
	struct avm_table_bucket	*next;
};

struct avm_table{
	unsigned	refcounter;
	struct avm_table_bucket*	strIndexed[AVM_TABLE_HASHSIZE];
	struct avm_table_bucket*	numIndexed[AVM_TABLE_HASHSIZE];
	unsigned total;
};




struct avm_memcell *stack[AVM_STACKSIZE];

void avm_initstack(void);

/*tables*/
struct avm_table* avm_tablenew(void);
void 	avm_tabledestroy(struct avm_table*);
struct avm_memcell *avm_tablegetelem(struct avm_table *table, struct avm_memcell *index);
void avm_tablesetelem(struct avm_table *, struct avm_memcell *, struct avm_memcell *);





extern void execute_assign(struct instruction *);

void execute_add(struct instruction *);
extern void execute_sub(struct instruction *);
extern void execute_mul(struct instruction *);
extern void execute_div(struct instruction *);
extern void execute_mod(struct instruction *);
extern void execute_uminus(struct instruction *);

extern void execute_and(struct instruction *);
extern void execute_or(struct instruction *);
extern void execute_not(struct instruction *);

extern void execute_jeq(struct instruction *);
extern void execute_jne(struct instruction *);
extern void execute_jle(struct instruction *);
extern void execute_jge(struct instruction *);
extern void execute_jlt(struct instruction *);
extern void execute_jgt(struct instruction *);

extern void execute_jump(struct instruction *);

extern void execute_call(struct instruction *);
extern void execute_pusharg(struct instruction *);
extern void execute_funcenter(struct instruction *);
extern void execute_funcexit(struct instruction *);

extern void execute_newtable(struct instruction *);
extern void execute_tablegetelem(struct instruction *);
extern void execute_tablesetelem(struct instruction *);

extern void execute_nop(struct instruction *);

typedef void (*execute_func_t)(struct instruction*);

execute_func_t executeFuncs[]={
	execute_assign,
	execute_add,
	execute_sub,
	execute_mul,
	execute_div,
	execute_mod,
	//execute_uminus,
	//execute_and,
	//execute_or,
	//execute_not,
	execute_jeq,
	execute_jne,
	execute_jle,
	execute_jge,
	execute_jlt,
	execute_jgt,
	execute_jump,
	execute_call,
	execute_pusharg,
	execute_funcenter,
	execute_funcexit,
	execute_newtable,
	execute_tablegetelem,
	execute_tablesetelem,
	execute_nop
};

typedef void (*library_func_t) (void);

struct library_map{
	char *id;
	library_func_t addr;
};

extern void memclear_string(struct avm_memcell* m);
extern void memclear_table(struct avm_memcell* m);

typedef void (*memclear_func_t)(struct avm_memcell*);

memclear_func_t memclearFuncs[]={
	0,
	memclear_string,
	0,
	memclear_table,
	0,
	0,
	0,
	0
};

double add_impl ( double x, double y );
double sub_impl ( double x, double y );
double mul_impl ( double x, double y );
double div_impl ( double x, double y );
double mod_impl ( double x, double y );

unsigned char number_tobool (struct avm_memcell *m);
unsigned char string_tobool (struct avm_memcell *m);
unsigned char bool_tobool (struct avm_memcell *m);
unsigned char table_tobool (struct avm_memcell *m);
unsigned char userfunc_tobool (struct avm_memcell *m);
unsigned char libfunc_tobool (struct avm_memcell *m);
unsigned char nil_tobool (struct avm_memcell *m);
unsigned char undef_tobool (struct avm_memcell *m);

struct avm_memcell* avm_translate_operand(struct vmarg* , struct avm_memcell*);
void avm_assign(struct avm_memcell* , struct avm_memcell* );

char * typeStrings[] = {
	"number",
	"string",
	"bool",
	"table",
	"userfunc",
	"libfunc",
	"nil",
	"undef"
};

void avm_tabledecrefcounter(struct avm_table*);
void avm_tableincrefcounter(struct avm_table*);

void avm_initcodentables(FILE *);

void avm_tabledestroy(struct avm_table *);


void libraryfunc_print (void);
void libfunc_typeof( void );
void libfunc_totalarguments(void);
void libraryfunc_cos (void);
void libraryfunc_sin (void);
void libraryfunc_strtonum(void);



#endif
