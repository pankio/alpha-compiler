#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include"inCode.h"
#include"symboltable.h"


#define EXPAND_SIZE 30000
#define CURR_SIZE   (total*sizeof(Quad))
#define NEW_SIZE    (EXPAND_SIZE*sizeof(Quad)+CURR_SIZE)

extern unsigned int programVarOffset;	
extern unsigned int functionLocalOffset;	
extern unsigned int formalArgOffset;	/*Gia na doume an einai programvar,formalarg i metabliti mesa se function*/
extern int scope;
extern int yylineno;
int tempcounter = 0;//gia kryfes metablites
extern int scopeSpaceCounter;
extern int num_progVars;/*gia synolo metablito n*/


typedef struct quad Quad;

Quad *quads = (Quad *) 0;
static unsigned int total = 0;
int Currquad  = 0;


char *instructions[] = {
		"assign", "add", "sub", "mul", "divX", "mod","if_eq", "if_noteq",
		"if_lesseq", "if_greatereq", "if_less", "if_greater", "jump","call","param","funcstart","funcend","tablecreate",
		"tablegetelem","tablesetelem","nop","uminus","and","or","not","getretval","ret"
		};

void expand ( void ){

	Quad *p;
	assert( total == Currquad );

	p = (Quad *) malloc(NEW_SIZE);

	if(p == NULL){
		fprintf(stderr, "Allocation error\n");
		exit(1);
	}
	if ( quads ){
		memcpy(p,quads,CURR_SIZE);
		free(quads);
	}
	quads = p;
	total += EXPAND_SIZE;

}

void emit(enum iopcode op, struct expr *arg1, struct expr *arg2, struct expr *result, unsigned label, unsigned line){
	Quad *p;
	
	if ( Currquad == total ){
		expand();
	}
	
	p = quads + Currquad++;
	p->op = op;
	p->arg1 = exprcpy(p->arg1, arg1);
	p->arg2 = exprcpy(p->arg2, arg2);
	p->result = exprcpy(p->result, result);
	p->label = label;
	p->line = line;
	
}

struct expr *exprcpy(struct expr *e1, struct expr *e2) {
	e1 = (struct expr *)malloc(sizeof(struct expr));
	
	if(e2 == NULL){
		return NULL;
	}
	e1->type = e2->type;
	e1->sym = e2->sym;
	e1->index = e2->index;
	e1->intConst = e2->intConst;
	e1->floatConst = e2->floatConst;
	if(e2->strConst != NULL){
		e1->strConst = NULL;
		e1->strConst = (char *)strdup(e2->strConst);
	}
	else {
		e1->strConst = NULL;
	}
	e1->boolConst = e2->boolConst;
	e1->next = e2->next;

	return e1;
}

char *newtempname( void){
	char *name = (char*)malloc(5*sizeof(char));
   	char* name2 = (char*)malloc(20*sizeof(char));  
   	int size;
  	 
	name = strcpy(name,"_tmp");
	sprintf(name2,"%d", tempcounter);
	name=strcat(name,name2);
    
	size=strlen(name);
	name[size]='\0';
   	tempcounter++;
   	
	return name;	
}


struct symbol * newtemp(int scope){
	struct symbol *tmp;
	char* tempname = newtempname();
	int scopespace;
	
	if(lookup_one_level(tempname,scope)){
		return lookup_retSym(tempname, scope);	
	}
	
	if(scopeSpaceCounter == 1){
		scopespace=0;
	}
	else
	if(scopeSpaceCounter % 2 == 0){
		scopespace=2;
	}
	else 
			scopespace=1;
	
	if(scopespace == 0){
		tmp = insert(scope, var_s,create_v(tempname, scope, yylineno, scopespace, ++programVarOffset), NULL, NULL);
		num_progVars++;
	}
	else if(scopespace == 1){
		fs->foffset = functionLocalOffset + 1;
		tmp = insert(scope, var_s,create_v(tempname, scope, yylineno, scopespace, ++functionLocalOffset), NULL, NULL);
	}
	else if(scopespace == 2){
		tmp = insert(scope, var_s,create_v(tempname, scope, yylineno, scopespace, ++formalArgOffset), NULL, NULL);
	}
	
	
	
	return tmp;
}

void resettemp(){
	tempcounter =0;
}


unsigned nextquadlabel(void){
	return Currquad;
}

void patchlabel( unsigned quadNo, unsigned label ){
	assert(quadNo < Currquad);
	quads[quadNo].label = label;
}

struct expr * lvalue_expr(struct symbol *sym){
	assert(sym);
	struct expr* e = (struct expr*) malloc(sizeof(struct expr));
	memset(e, 0, sizeof(struct expr));
	
	e->next = (struct expr *)0;
	e->sym = sym;
	
	switch (sym->type){
		case var_s		:e->type = var_e; break;
		case programfunc_s	:e->type = programfunc_e; break;
		case libraryfunc_s	:e->type = libraryfunc_e; break;
	}
	return e;
}

struct expr* newexpr (enum expr_t t){
	struct expr* e = (struct expr*)malloc(sizeof(struct expr));
	
	if(e == NULL){
		printf("Allocation error\n");
		exit(1);
	}
	
	memset(e,0,sizeof(struct expr));
	e->type = t;
	e->next = NULL;
	return e;
}

struct expr * newexpr_conststring(char *s){
	struct expr *e = newexpr(conststring_e);
	
	e->strConst = strdup(s);
	return e;
}

struct expr * newexpr_intnum(int s){
	struct expr *e = newexpr(intnum_e);
	

	e->intConst = s;
	return e;
}

struct expr * newexpr_floatnum(float s){
	struct expr *e = newexpr(floatnum_e);
	

	e->floatConst = s;
	return e;
}

struct expr * newexpr_constbool(const char s){
	struct expr *e = newexpr(constbool_e);
	
	e->boolConst = s;
	return e;
}

struct expr * newexpr_nil(char *s){
	struct expr *e = newexpr(nil_e);
	e->strConst = strdup(s);
	return e;
}

struct expr * newexpr_var(struct symbol * sym){
	struct expr *e = newexpr(var_e);
	e->sym = sym;
	return e;

}

struct expr *const_expr(enum iopcode op, struct expr *expr) {
	char *tmpName;
	struct expr *e = (struct expr *)malloc(sizeof(struct expr));
	struct symbol *sym;
	
	if  ( expr->type == intnum_e  || expr->type == floatnum_e || expr->type == conststring_e 
		  || expr->type==constbool_e  ){
		
		sym = newtemp(scope);
	       	e->sym = sym;
		emit(op, expr, NULL, e, 0, yylineno);
	}
	else
		printf("Error : Wrong type argument at line:%dn", yylineno);
	
	return e;	
}

struct expr *expr_func(struct symbol *sym) {
	struct expr *e;
	if(sym->type == 0) {
		e = newexpr(libraryfunc_e);
		e->sym = sym;
	}
	else if(sym->type == 2){
		e = newexpr(programfunc_e);
		e->sym = sym;
	}
	else if(sym->type == 1){
		e = newexpr(var_e);
		e->sym = sym;
	}
	else return NULL;

	return e;
}

struct expr* emit_iftableitem(struct expr *e){
	if(e->type != tableitem_e)
		return e;
	else{
		struct expr *result = newexpr(var_e);
		result->sym = newtemp(scope);
		emit(
			tablegetelem,
			e,
			e->index,
			result,
			0,
			yylineno
		);
		return result;
	}
}





void checkuminus(struct expr *e){
	if(	e->type == conststring_e ||
		e->type == constbool_e ||
		e->type == nil_e ||
		e->type == newtable_e ||
		e->type == programfunc_e ||
		e->type == libraryfunc_e ||
		e->type == boolexpr_e ){
		
			comperror("illegal expr to unary -",yylineno);
		
		}
}

void comperror(char* format,int line){
	printf("%s at line %d\n", format, line);
}

struct expr * operations_expr(enum iopcode op, struct expr* arg1, struct expr *arg2){
	struct symbol *sym;
	struct expr *temp;
	
	if((arg1->type == intnum_e || arg1->type == floatnum_e || arg1->type ==var_e )
		&&(arg2->type == intnum_e || arg2->type == floatnum_e || arg2->type ==var_e)){
		
		sym = newtemp(scope);
	       	temp = newexpr_var(sym);
		emit(op, arg1, arg2, temp, 0, yylineno);
	}
	else
		printf("error:Incompatible types in operation at line %d\n",yylineno);

	return temp;
}

struct expr *bool_op_expr(enum iopcode op, struct expr *arg1, struct expr *arg2) {
	struct symbol *sym;
	struct expr *temp;
	
	if((arg1->type == intnum_e || arg1->type == floatnum_e || arg1->type ==var_e )
		&&(arg2->type == intnum_e || arg2->type == floatnum_e || arg2->type ==var_e)){
		
		sym = newtemp(scope);
	       	temp = newexpr_var(sym);
		emit(op, arg1, arg2, NULL, Currquad + 3, yylineno);
		emit(assign, newexpr_constbool(0u),NULL,temp, 0, yylineno);
		emit(jump ,NULL, NULL, NULL, Currquad + 2, yylineno);
		emit(assign, newexpr_constbool(1u), NULL, temp, 0, yylineno);
	}
	else
		printf("error:Incompatible types in operation at line %d\n",yylineno);
	
	return temp;
}



struct expr *andor_expr(enum iopcode op, struct expr *arg1, struct expr *arg2) {
	struct symbol *sym;
	struct expr *temp;
	
		if((arg1->type == intnum_e || arg1->type == floatnum_e || arg1->type ==var_e )
		&&(arg2->type == intnum_e || arg2->type == floatnum_e || arg2->type ==var_e)){
		
		sym = newtemp(scope);
	       	temp = newexpr_var(sym);
		emit(op, arg1, arg2, temp, 0, yylineno);
	}
	else
		printf("error:Incompatible types in operation at line %d\n",yylineno);
	
	return temp;
}

struct expr *not_expr(enum iopcode op, struct expr *arg1) {
	struct symbol *sym;
	struct expr *temp;
	
		if((arg1->type == intnum_e || arg1->type == floatnum_e || arg1->type ==var_e )){
		
		sym = newtemp(scope);
	       	temp = newexpr_var(sym);
		emit(op, arg1, NULL, temp, 0, yylineno);
	}
	else
		printf("error:Incompatible types in operation at line %d\n",yylineno);
	
	return temp;
}

struct expr *sspp_lvalue(enum iopcode op, struct expr *arg1, struct expr *arg2) {
	if(arg1->type == var_e) 
		emit(op, arg1, arg2, arg1, 0, yylineno);
	else
		printf("error:Incompatible types in Operation at line:%d\n", yylineno);

	return arg1;
}



struct expr * newexpr_call(struct expr * name, struct expr *elist){

	struct expr *tmp;
	struct expr * ex;
	struct symbol *sym;
	


	for(tmp = elist; tmp!=NULL; tmp=tmp->next)
		emit(param,NULL,NULL,tmp, 0, yylineno);
		
	emit(call, NULL, NULL, name, 0, yylineno);
	
	sym = newtemp(scope);
	
	ex = newexpr_var(sym);
	
	emit(getretval, NULL,NULL, ex, 0, yylineno);
	
	return ex;

}

struct expr *expruminus(enum iopcode op, struct expr *ex) {
	struct symbol *sym;
	struct expr *tmp;
	
	if  ( ex->type == intnum_e  || ex->type == floatnum_e || ex->type == var_e){
		//sym = newtemp(scope);
		//tmp = newexpr_var(sym);
		emit(mul, ex, newexpr_intnum(-1), ex, 0, yylineno);
	}
	else {
		printf("Error : Wrong type argument to Unary Operators at line:%d\n", yylineno);
	}

	return tmp;
}




void print_quads() {
	int i;
	
	char * booleanexpr[2] = {"false", "true"};
	
	printf("#Quads:%d\n", Currquad);
	for(i = 0; i < Currquad; i++) {
	
		if(quads[i].op == funcstart || quads[i].op == funcend){/*functions*/
			printf("%d) <%s> %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name);
		}else
		if(quads[i].op == assign){/*assignement*/
			if(quads[i].arg1->sym != NULL ){
				printf("%d) <%s> %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].result->sym->u.v->name);
			}
			else if(quads[i].arg1->type == constbool_e )
				printf("%d) <%s> %s %s;\n",i,instructions[quads[i].op], booleanexpr[(int)quads[i].arg1->boolConst],quads[i].result->sym->u.v->name);
			else if(quads[i].arg1->type == conststring_e){
				printf("%d) <%s> %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->strConst,quads[i].result->sym->u.v->name);
			}
			else if(quads[i].arg1->type == floatnum_e){
				printf("%d) <%s> %lf %s;\n",i,instructions[quads[i].op], quads[i].arg1->floatConst,quads[i].result->sym->u.v->name);
			}
			
			
			else
				printf("%d) <%s> %d %s;\n",i,instructions[quads[i].op], quads[i].arg1->intConst,quads[i].result->sym->u.v->name);
		}else
		if(quads[i].op == sub || quads[i].op == add || quads[i].op == mul || quads[i].op == divX || quads[i].op == mod)/*add-sub-mul-div*/{
			if(quads[i].arg2->type == intnum_e){/*gia ++i*/
				printf("%d) <%s> %s %d %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->intConst, quads[i].result->sym->u.v->name);
			}
			else
			printf("%d) <%s> %s %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
		}
			
		else
		if(quads[i].op == jump){
			printf("%d) <%s> %d;\n", i,instructions[quads[i].op], quads[i].label);
		}
		else
		if(quads[i].op == if_eq || quads[i].op == if_noteq ||
		quads[i].op == if_lesseq || quads[i].op == if_greatereq ||  
		quads[i].op == if_less ||quads[i].op == if_greater){
			if(quads[i].arg2->sym != NULL ){
				printf("%d) <%s> %s %s %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].label);
			}
			else if(quads[i].arg2->type == constbool_e )
				printf("%d) <%s> %s %s %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,booleanexpr[(int)quads[i].arg2->boolConst],quads[i].label);
			else if(quads[i].arg2->type == conststring_e){
				printf("%d) <%s> %s %s %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->strConst,quads[i].label);
			}
			else if(quads[i].arg2->type == floatnum_e){
				printf("%d) <%s> %s %lf %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->floatConst,quads[i].label);
			}
			
			
			else
				printf("%d) <%s> %s %d %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->intConst,quads[i].label);
		
		}
		else
		if(quads[i].op == ret){
			if(quads[i].result == NULL){
				printf("%d) <%s>\n", i, instructions[quads[i].op]);
			}
			else if(quads[i].result->sym != NULL ){
				printf("%d) <%s> %s;\n", i, instructions[quads[i].op], quads[i].result->sym->u.v->name);
			}
			else if(quads[i].result->type == constbool_e ){
				printf("%d) <%s> %c;\n", i, instructions[quads[i].op], quads[i].result->boolConst);
			}
			else if(quads[i].result->type == conststring_e){
				printf("%d) <%s> %s;\n", i, instructions[quads[i].op], quads[i].result->strConst);
			}
			else if(quads[i].result->type == floatnum_e){
				printf("%d) <%s> %lf;\n", i, instructions[quads[i].op], quads[i].result->floatConst);
			}
			else if(quads[i].result->type == nil_e){
				printf("%d) <%s> nil;\n", i, instructions[quads[i].op]);
			}
			else if(quads[i].result->type == intnum_e ){
				printf("%d) <%s> %d;\n", i, instructions[quads[i].op], quads[i].result->intConst);
			}
		}
		else
		if(quads[i].op == tablecreate){
			printf("%d) <%s> %s;\n", i, instructions[quads[i].op], quads[i].result->sym->u.v->name);	
		}
		else
		if(quads[i].op == tablesetelem){
			if(quads[i].arg1->type == intnum_e)
				printf("%d) <%s> %d %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->intConst,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
			else
			if(quads[i].arg2->type == conststring_e){
				printf("%d) <%s> %s \"%s\" %s\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name , quads[i].arg2->strConst ,quads[i].result->sym->u.v->name);
			}else
				printf("%d) <%s> %s %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
			
		}
		else
		if(quads[i].op == uminus){
			printf("%d) <%s> %s %s\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name , quads[i].result->sym->u.v->name);
		}
		else
		if(quads[i].op == tablegetelem){
			if(quads[i].arg2->type == conststring_e)
				printf("%d) <%s> %s \"%s\" %s\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name , quads[i].arg2->strConst ,quads[i].result->sym->u.v->name);
			else
				printf("%d) <%s> %s %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
		}
		else 
		if(quads[i].op == call || quads[i].op == getretval || quads[i].op == param){
			printf("%d) <%s> %s ;\n",i,instructions[quads[i].op], quads[i].result->sym->u.v->name);
		}
			else if(quads[i].op == and || quads[i].op == or){
			printf("%d) <%s> %s %s %s;\n", i, instructions[quads[i].op],quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
		}else if(quads[i].op == and || quads[i].op == or){
			printf("%d) <%s> %s %s %s;\n", i, instructions[quads[i].op],quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
		}
		else if(quads[i].op == nop){
			printf("%d) <%s>\n", i, instructions[quads[i].op]);
		}	
	}
}

void print_quads_tofile() {
	int i;
	FILE *fquad = fopen("quads.txt", "w+");
	char * booleanexpr[2] = {"false", "true"};
	
	for(i = 0; i < Currquad; i++) {
	
		if(quads[i].op == funcstart || quads[i].op == funcend){/*functions*/
			fprintf(fquad, "%d) <%s> %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name);
		}else
		if(quads[i].op == assign){/*assignement*/
			if(quads[i].arg1->sym != NULL ){
				fprintf(fquad, "%d) <%s> %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].result->sym->u.v->name);
			}
			else if(quads[i].arg1->type == constbool_e )
				fprintf(fquad, "%d) <%s> %s %s;\n",i,instructions[quads[i].op], booleanexpr[(int)quads[i].arg1->boolConst],quads[i].result->sym->u.v->name);
			else if(quads[i].arg1->type == conststring_e){
				fprintf(fquad, "%d) <%s> %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->strConst,quads[i].result->sym->u.v->name);
			}
			else if(quads[i].arg1->type == floatnum_e){
				fprintf(fquad, "%d) <%s> %lf %s;\n",i,instructions[quads[i].op], quads[i].arg1->floatConst,quads[i].result->sym->u.v->name);
			}
			
			
			else
				fprintf(fquad, "%d) <%s> %d %s;\n",i,instructions[quads[i].op], quads[i].arg1->intConst,quads[i].result->sym->u.v->name);
		}else
		if(quads[i].op == sub || quads[i].op == add || quads[i].op == mul || quads[i].op == divX || quads[i].op == mod)/*add-sub-mul-div*/{
			if(quads[i].arg2->type == intnum_e){/*gia ++i*/
				fprintf(fquad, "%d) <%s> %s %d %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->intConst, quads[i].result->sym->u.v->name);
			}
			else
			fprintf(fquad, "%d) <%s> %s %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
		}
			
		else
		if(quads[i].op == jump){
			fprintf(fquad, "%d) <%s> %d;\n", i,instructions[quads[i].op], quads[i].label);
		}
		else
		if(quads[i].op == if_eq || quads[i].op == if_noteq ||
		quads[i].op == if_lesseq || quads[i].op == if_greatereq ||  
		quads[i].op == if_less ||quads[i].op == if_greater){
			if(quads[i].arg2->sym != NULL ){
				fprintf(fquad, "%d) <%s> %s %s %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].label);
			}
			else if(quads[i].arg2->type == constbool_e )
				fprintf(fquad, "%d) <%s> %s %s %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,booleanexpr[(int)quads[i].arg2->boolConst],quads[i].label);
			else if(quads[i].arg2->type == conststring_e){
				fprintf(fquad, "%d) <%s> %s %s %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->strConst,quads[i].label);
			}
			else if(quads[i].arg2->type == floatnum_e){
				fprintf(fquad, "%d) <%s> %s %lf %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->floatConst,quads[i].label);
			}
			
			
			else
				fprintf(fquad, "%d) <%s> %s %d %d;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->intConst,quads[i].label);
		
		}
		else
		if(quads[i].op == ret){
			if(quads[i].result == NULL){
				fprintf(fquad, "%d) <%s>\n", i, instructions[quads[i].op]);
			}
			else if(quads[i].result->sym != NULL ){
				fprintf(fquad, "%d) <%s> %s;\n", i, instructions[quads[i].op], quads[i].result->sym->u.v->name);
			}
			else if(quads[i].result->type == constbool_e ){
				fprintf(fquad, "%d) <%s> %c;\n", i, instructions[quads[i].op], quads[i].result->boolConst);
			}
			else if(quads[i].result->type == conststring_e){
				fprintf(fquad, "%d) <%s> %s;\n", i, instructions[quads[i].op], quads[i].result->strConst);
			}
			else if(quads[i].result->type == floatnum_e){
				fprintf(fquad, "%d) <%s> %lf;\n", i, instructions[quads[i].op], quads[i].result->floatConst);
			}
			else if(quads[i].result->type == nil_e){
				fprintf(fquad, "%d) <%s> nil;\n", i, instructions[quads[i].op]);
			}
			else if(quads[i].result->type == intnum_e ){
				fprintf(fquad, "%d) <%s> %d;\n", i, instructions[quads[i].op], quads[i].result->intConst);
			}
		}
		else
		if(quads[i].op == tablecreate){
			fprintf(fquad, "%d) <%s> %s;\n", i, instructions[quads[i].op], quads[i].result->sym->u.v->name);	
		}
		else
		if(quads[i].op == tablesetelem){
			if(quads[i].arg1->type == intnum_e)
				fprintf(fquad, "%d) <%s> %d %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->intConst,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
			else
			if(quads[i].arg2->type == conststring_e){
				fprintf(fquad, "%d) <%s> %s \"%s\" %s\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name , quads[i].arg2->strConst ,quads[i].result->sym->u.v->name);
			}else
				fprintf(fquad, "%d) <%s> %s %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
			
		}
		else
		if(quads[i].op == uminus){
			fprintf(fquad, "%d) <%s> %s %s\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name , quads[i].result->sym->u.v->name);
		}
		else
		if(quads[i].op == tablegetelem){
			if(quads[i].arg2->type == conststring_e)
				fprintf(fquad, "%d) <%s> %s \"%s\" %s\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name , quads[i].arg2->strConst ,quads[i].result->sym->u.v->name);
			else
				fprintf(fquad, "%d) <%s> %s %s %s;\n",i,instructions[quads[i].op], quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
		}
		else 
		if(quads[i].op == call || quads[i].op == getretval || quads[i].op == param){
			fprintf(fquad, "%d) <%s> %s ;\n",i,instructions[quads[i].op], quads[i].result->sym->u.v->name);
		}
		else if(quads[i].op == and || quads[i].op == or){
			fprintf(fquad, "%d) <%s> %s %s %s;\n", i, instructions[quads[i].op],quads[i].arg1->sym->u.v->name,quads[i].arg2->sym->u.v->name, quads[i].result->sym->u.v->name);
		}
		else if(quads[i].op == nop){
			fprintf(fquad, "%d) <%s>\n", i, instructions[quads[i].op]);
		}	
	}
	fclose(fquad);
}

